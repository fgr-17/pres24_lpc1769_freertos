################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/lwIP/lwIP_Apps/lwIP_Apps.c 

OBJS += \
./Source/lwIP/lwIP_Apps/lwIP_Apps.o 

C_DEPS += \
./Source/lwIP/lwIP_Apps/lwIP_Apps.d 


# Each subdirectory must supply rules for building sources it contributes
Source/lwIP/lwIP_Apps/%.o: ../Source/lwIP/lwIP_Apps/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Products/FreeRTOS-Plus-IO/Include" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Products/FreeRTOS/include" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Products/FreeRTOS-Plus-IO/Device/LPC17xx/SupportedBoards" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Products/FreeRTOS/portable/GCC/ARM_CM3" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Products/FreeRTOS-Plus-CLI" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/CMSISv2p00_LPC17xx/inc" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Plus-Demo-2/Source/FatFS" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Plus-Demo-2/Source/lwIP/include/ipv4" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Plus-Demo-2/Source/lwIP/netif/include" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Plus-Demo-2/Source/lwIP/lwIP_Apps" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Plus-Demo-2/Source/lwIP/include" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Plus-Demo-2/Source/Examples/Include" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/lpc17xx.cmsis.driver.library/Include" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/FreeRTOS-Plus-Demo-2/Source" -Os -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -Wextra -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


