/*
 * ADS1292.h
 *
 *  Created on: 02/10/2013
 *      Author: Lautaro
 */

#ifndef ADS1292_H_
#define ADS1292_H_

/**************************************************************
 ** Códigos de operación del ADS1292. Ver SBAS502B pág. 35
 **************************************************************/

// System Commands :

	#define 	ADS_WAKEUP  	0x02								// Wake-up from standby mode
	#define	 	ADS_STANDBY		0x04								// Enter standby mode
	#define	 	ADS_RESET 		0x06								// Reset the device
	#define 	ADS_START 		0x08								// Start or restart (synchronize) conversions
	#define 	ADS_STOP 		0x0A								// Stop conversion
	#define 	ADS_OFFSETCAL 	0x1A								// Channel offset calibracion

// Data Read Commands :

	#define		ADS_RDATAC		0x10								// Enable Read Data Continuous mode. This mode is the default mode at power-up
	#define		ADS_SDATAC		0x11								// Stop Read Data Continuously mode
	#define 	ADS_RDATA		0x12								// Read data by command; supports multiple read back

// Register Read Commands :

	#define		ADS_RREG		0x20								// Read n nnnn registers starting at address r rrrr (001r rrrr - 000n nnnn)
	#define 	ADS_WREG 		0x40								// Write n nnnn registers starting at address r rrrr (010r rrrr - 000n nnnn)

/**************************************************************
 ** Mapa de registros del ADS1292. Ver SBAS502B pág. 39 Tabla 14
 **************************************************************/

// Device Settings (Read-Only Registers) :

#define		ADS_ID			0x00

// Global Settings Across Channels :

#define		ADS_CONFIG1   	0x01
#define		ADS_CONFIG2   	0x02
#define		ADS_LOFF 	  	0x03

// Channel-Specific Settings :

#define		ADS_CH1SET 	  	0x04
#define		ADS_CH2SET 	  	0x05
#define		ADS_RLD_SENS  	0x06
#define		ADS_LOFF_SENS 	0x07
#define		ADS_LOFF_STAT 	0x08

// GPIO and Other Registers :

#define		ADS_RESP1 	  	0x09
#define		ADS_RESP2     	0x0A
#define		ADS_GPIO 	  	0x0B

#define 	DR_LECTURA			0x00
/**************************************************************
 ** Descripción de los registros :
 **************************************************************/

// Registro ID : ID Control Register (Factory-Programmed, Read-Only) SBAS502B Pág. 39
// Direccion = 00h
// Bit 4 siempre en 1
// Bit [3:2] siempre en 0

#define ADS_ID_REV_ID_A_MAS						(0x07 << 5)
	#define ADS_ID_REV_ID_A_ADS1x9x				(0x02 << 5)
	#define ADS_ID_REV_ID_A_ADS1292R			(0x03 << 5)

#define ADS_ID_REV_ID_B_MAS						(0x03 << 0)
	#define ADS_ID_REV_ID_B_ADS1191				(0x00 << 0)
	#define ADS_ID_REV_ID_B_ADS1192				(0x01 << 0)
	#define ADS_ID_REV_ID_B_ADS1291				(0x02 << 0)
	#define ADS_ID_REV_ID_B_ADS1292x			(0x03 << 0)

// Registro CONFIG1 : Configuration Register 1 SBAS502B Pág. 40
// Direccion = 01h
// Bit [6:3] Debe setearse a 0 siempre

#define ADS_CONFIG1_SINGLE_SHOT					(0x01 << 7)					// Modo Single-Shot
#define ADS_CONFIG1_CONTINUOUS				  	(~(0x01 << 7))				// Modo contínuo de conversión

#define	ADS_CONFIG_1_OS_RATIO_MAS				(0x07 << 0)					// These bits determine the oversampling ratio of both channel 1 - 2
	#define	ADS_CONFIG_1_OS_RATIO_125SPS		(0x00 << 0)					// fMOD / 1024 - 125SPS
	#define	ADS_CONFIG_1_OS_RATIO_250SPS		(0x01 << 0)					// fMOD / 512 - 250SPS
	#define	ADS_CONFIG_1_OS_RATIO_500SPS		(0x02 << 0)					// fMOD / 256 - 500SPS
	#define	ADS_CONFIG_1_OS_RATIO_1kSPS			(0x03 << 0)					// fMOD / 128 - 1kSPS
	#define	ADS_CONFIG_1_OS_RATIO_2kSPS			(0x04 << 0)					// fMOD / 64 - 2kSPS
	#define	ADS_CONFIG_1_OS_RATIO_4kSPS			(0x05 << 0)					// fMOD / 32 - 4kSPS
	#define	ADS_CONFIG_1_OS_RATIO_8kSPS			(0x06 << 0)					// fMOD / 16 - 8kSPS

// Registro CONFIG2 : Configuration Register 2 SBAS502B Pág. 41
// Direccion = 02h
// Bit 7 siempre en 1
// Bit 2 siempre en 0

#define ADS_CONFIG2_PDB_LOFF_COMP				(0x01 << 6)					// Lead-off comparator enabled
#define ADS_CONFIG2_PDB_REFBUF					(0x01 << 5)					// Reference buffer is enabled
#define ADS_CONFIG2_VREF_4V						(0x01 << 4)					// 1 - 4.033V reference // 0 - 2.42V reference
#define ADS_CONFIG2_CLK_EN						(0x01 << 3)					// Oscillator clock output enabled (salida del CLK pin)
#define ADS_CONFIG2_INT_TEST					(0x01 << 1)					// Test signal selection : 0 :off // 1 = On; amplitude = +- (VREFP - VREFN)/2400
#define ADS_CONFIG2_TEST_FREQ					(0x01 << 0)					// Test signal freq. 0 : DC // 1 : Square wave 1 Hz


// Registro CH1SET : Channel 1 Settings. SBAS502B Pág. 43
// Dirección = 04h

#define ADS_CH1SET_PD 							(0x01 << 7)					// 1 : Channel 1 power-down (setear mux a corto) // 0 : Normal operation
#define ADS_CH1SET_GAIN_MAS						(0x07 << 4)					// Channel 1 PGA gain setting
	#define ADS_CH1SET_GAIN_6					(0x00 << 4)					// Ganancia = 6
	#define ADS_CH1SET_GAIN_1					(0x01 << 4)					// Ganancia = 1
	#define ADS_CH1SET_GAIN_2					(0x02 << 4)					// Ganancia = 2
	#define ADS_CH1SET_GAIN_3					(0x03 << 4)					// Ganancia = 3
	#define ADS_CH1SET_GAIN_4					(0x04 << 4)					// Ganancia = 4
	#define ADS_CH1SET_GAIN_8					(0x05 << 4)					// Ganancia = 8
	#define ADS_CH1SET_GAIN_12					(0x06 << 4)					// Ganancia = 12

#define ADS_CH1SET_MUX_MAS						(0x0F << 0)					// Channel 1 input selection
	#define ADS_CH1SET_MUX_NORMAL				(0x00 << 0)					// Normal electrode input (default)
	#define ADS_CH1SET_MUX_SHORTED				(0x01 << 0)					// Input shorted (for offset measurements)
	#define ADS_CH1SET_MUX_RDL_MEAS				(0x02 << 0)					// RLD_MEASURE
	#define ADS_CH1SET_MUX_MVDD					(0x03 << 0)					// MVDD for supply measurements = (AVDD + AVSS)/2 (setear PGA1 para no saturar)
	#define ADS_CH1SET_MUX_TEMP_SENS			(0x04 << 0)					// Temperature sensor
	#define ADS_CH1SET_MUX_TEST_SIGNAL			(0x05 << 0)					// Test signal
	#define ADS_CH1SET_MUX_RLD_DRP				(0x06 << 0)					// RLD_DRP (positive input is connected to RLDIN)
	#define ADS_CH1SET_MUX_RLD_DRM				(0x07 << 0)					// RLD_DRM (negative input is conected to RLDIN)
	#define ADS_CH1SET_MUX_RLD_DRPM				(0x08 << 0)					// RLD_DRPM (both positive and negative inputs are connected to RLDIN)
	#define ADS_CH1SET_MUX_IN3					(0x09 << 0)					// Route IN3P and IN3N to channel 1 inputs

// Registro CH2SET : Channel 2 Settings. SBAS502B Pág. 44
// Dirección = 05h

#define ADS_CH2SET_PD 							(0x01 << 7)					// 1 : Channel 1 power-down (setear mux a corto) // 0 : Normal operation
#define ADS_CH2SET_GAIN_MAS						(0x07 << 4)					// Channel 1 PGA gain setting
	#define ADS_CH2SET_GAIN_6					(0x00 << 4)					// Ganancia = 6
	#define ADS_CH2SET_GAIN_1					(0x01 << 4)					// Ganancia = 1
	#define ADS_CH2SET_GAIN_2					(0x02 << 4)					// Ganancia = 2
	#define ADS_CH2SET_GAIN_3					(0x03 << 4)					// Ganancia = 3
	#define ADS_CH2SET_GAIN_4					(0x04 << 4)					// Ganancia = 4
	#define ADS_CH2SET_GAIN_8					(0x05 << 4)					// Ganancia = 8
	#define ADS_CH2SET_GAIN_12					(0x06 << 4)					// Ganancia = 12

#define ADS_CH2SET_MUX_MAS						(0x0F << 0)					// Channel 1 input selection
	#define ADS_CH2SET_MUX_NORMAL				(0x00 << 0)					// Normal electrode input (default)
	#define ADS_CH2SET_MUX_SHORTED				(0x01 << 0)					// Input shorted (for offset measurements)
	#define ADS_CH2SET_MUX_RDL_MEAS				(0x02 << 0)					// RLD_MEASURE
	#define ADS_CH2SET_MUX_VDD_2				(0x03 << 0)					// VDD/2 for supply measurements
	#define ADS_CH2SET_MUX_TEMP_SENS			(0x04 << 0)					// Temperature sensor
	#define ADS_CH2SET_MUX_TEST_SIGNAL			(0x05 << 0)					// Test signal
	#define ADS_CH2SET_MUX_RLD_DRP				(0x06 << 0)					// RLD_DRP (positive input is connected to RLDIN)
	#define ADS_CH2SET_MUX_RLD_DRM				(0x07 << 0)					// RLD_DRM (negative input is conected to RLDIN)
	#define ADS_CH2SET_MUX_RLD_DRPM				(0x08 << 0)					// RLD_DRPM (both positive and negative inputs are connected to RLDIN)
	#define ADS_CH2SET_MUX_IN3					(0x09 << 0)					// Route IN3P and IN3N to channel 1 inputs


/**************************************************************
 ** Frecuencias y tiempos de espera tras comandos :
 **************************************************************/

#define ADS_FMOD					128000							// Frecuencia de muestreo al inicializar con clock interno
#define ADS_TPOR					4096							// Tiempo de espera después de encender en tMODs. SBAS502B Pág. 62 Tabla 17
#define ADS_TRST					1								// Ancho del pulso de reset en tiempo de tMODs. SBAS502B Pág. 62 Tabla 17
#define ADS_TDRST					18								// Tiempo de espera después del pulso de reset en ciclos de clock tCLK
#define ADS_CS_SOSTENER				4								// Tiempo de mantener CS en bajo después de mandar un comando en tCLK
#define ADS_CMD_RESET_TESP			9								// Tiempo de espera luego de enviar el Reset en tMOD


/*************************************************
 ** Bits del registro IOnIntEnF - Usado en pin DRDY
 *************************************************/

#define GPIOINT_IONINTENF_P00EF						(0x01 << 0)			// Bit para habilitar interrupcion en flanco desc. del P0.0

/*************************************************
 ** Bits del registro IntStat - Usado en pin DRDY
 *************************************************/

#define GPIOINT_INTSTAT_P0INT				(0x01 << 0)						// Interrupcion pendiente en P0
#define GPIOINT_INTSTAT_P2INT				(0x01 << 2)						// Interrupcion pendiente en P0
#define GPIOINT_INTSTAT_P00FEI				(0x01 << 0)						// Interrupcion pendiente en flanco desc. del P0.0
#define GPIOINT_INTCLR_P00CI				(0x01 << 0)						// Bit para limpiar la irq. pendiente

/*************************************************
 ** Macros para el manejo de CS.
 *************************************************/

#define ADS_SPI_CS_INACTIVO()		LPC_GPIO0->FIOSET0 |= 0x01UL << 6
#define ADS_SPI_CS_ACTIVO()			LPC_GPIO0->FIOCLR0 |= 0x01UL << 6

/****************************************************************************
 ** 						FUNCIONES EXTERNAS								*
 ****************************************************************************/

extern void SSP1_Recibir_Palabra_24bits (uint8_t*buf);
uint8_t ADS_Config(uint8_t reg, uint8_t rw, uint8_t data );

extern void ADS_Command(uint8_t command );
extern void ADS_Init(uint8_t channels, uint8_t sampling);

extern void ADS_Prueba(void);
extern void ADS1292_HW_Inic (void);
extern void ADS1292_Inic (void);
extern uint8_t SSP1_Recibir_Byte (void);

#endif /* ADS1292_H_ */
