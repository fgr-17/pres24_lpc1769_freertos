
void SetPINSEL ( uint8_t  , uint8_t  ,uint8_t );
void SetPINMODE( uint8_t  , uint8_t  ,uint8_t );
void SetDIR( uint8_t  , uint8_t  , uint8_t  );
void SetPIN( uint8_t  , uint8_t  , uint8_t  );
uint8_t GetPIN( uint8_t  , uint8_t  , uint8_t );

void Inicializar_Teclado( void );
void Inicializar_Relay( void );
void Inicializar_RGB( void );
void Inicializar_Display( void );
void Inicializar( void );

#define		ACTIVO_BAJO		0
#define		ACTIVO_ALTO		1

//Identificación de los puertos de expansion:

#define EXPANSION0		PORT2,7
#define EXPANSION1		PORT1,29
#define EXPANSION2		PORT4,28
#define EXPANSION3		PORT1,23
#define EXPANSION4		PORT1,20
#define EXPANSION5		PORT0,19
#define EXPANSION6		PORT3,26
#define EXPANSION7		PORT1,25
#define EXPANSION8		PORT1,22
#define EXPANSION9		PORT1,19
#define EXPANSION10		PORT0,20
#define EXPANSION11		PORT3,25
#define EXPANSION12		PORT1,27
#define EXPANSION13		PORT1,24
#define EXPANSION14		PORT1,21
#define EXPANSION15		PORT1,18
#define EXPANSION16		PORT1,31
#define EXPANSION17		PORT0,24
#define EXPANSION18		PORT0,25
#define EXPANSION19		PORT0,17
#define EXPANSION20		PORT1,31
#define EXPANSION21		PORT0,22
#define EXPANSION22		PORT0,15
#define EXPANSION23		PORT0,16
#define EXPANSION24		PORT2,8
#define EXPANSION25		PORT2,12
#define EXPANSION26		PORT1,31
#define EXPANSION27		PORT1,31

//Leds (Reles)
#define		RELAY1		PORT2,0
#define		RELAY2		PORT0,23
#define		RELAY3		PORT0,21
#define		RELAY4		PORT0,27
#define		BUZZ		PORT0,28

//Led RGB:
#define		RGBR		PORT2,1
#define		RGBG		PORT2,2
#define		RGBB		PORT2,3

//Teclas (teclado 4x1)
#define		KEY0		PORT2,10
#define		KEY1		PORT0,18
#define		KEY2		PORT0,11
#define		KEY3		PORT2,13

// DAC

#define AOUT PORT0,26
#define		O4		PORT1,28

