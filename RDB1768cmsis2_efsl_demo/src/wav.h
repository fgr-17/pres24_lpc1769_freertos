#ifndef WAV_H_
#define WAV_H_

#include "RegsLPC1769.h"
#include "infotronic.h"


typedef struct {
	// Chunk Descriptor
	uint8_t  ChunkID[4]; // RIFF
	uint32_t ChunkSize;
	uint8_t  Format[4];  // WAVE
	// fmt Subchunk
	uint8_t  Subchunk1ID[4]; // fmt[]
	uint32_t Subchunk1Size;
	uint16_t AudioFormat;
	uint16_t NumChannels;
	uint32_t SampleRate;
	uint32_t ByteRate;
	uint16_t BlockAlign;
	uint16_t BitPerSample;
	// data Subchunk
	uint8_t  Subchunk2ID[4];  // data
	uint32_t Subchunk2Size;
	void* data;
} wav;
#endif /* WAV_H_ */
