#ifndef REGS_H_
#define REGS_H_

#define		PORT0		0
#define		PORT1		1
#define		PORT2		2
#define		PORT3		3
#define		PORT4		4


#define		FUNCION_GPIO	0
#define		FUNCION_1		1
#define		FUNCION_2		2
#define		FUNCION_3		3

#define		MODO_0		0
#define		MODO_1		1
#define		MODO_2		2
#define		MODO_3		3

#define		ENTRADA		0
#define		SALIDA		1

	#define		__R					volatile const
	#define		__W					volatile
	#define		__RW				volatile

	typedef 	unsigned int 		uint32_t;
	typedef 	unsigned short 		uint16_t;
	typedef 	unsigned char 		uint8_t;

	//!< GPIO - PORT0
	/*	*						*
		*************************
		*		FIODIR			*	0x2009C000
		*************************
		*		RESERVED		*	0x2009C004
		*************************
		*		RESERVED		*	0x2009C008
		*************************
		*		RESERVED		*	0x2009C00C
		*************************
		*		FIOMASK			*	0x2009C010
		*************************
		*		FIOPIN			*	0x2009C014
		*************************
		*		FIOSET			*	0x2009C018
		*************************
		*		FIOCLR			*	0x2009C01C
		*************************
		*						*
		*						*
	*/
#define		GPIO		( ( __RW uint32_t * ) 0x2009C000UL )

	//!< ////////////////Registros PINSEL//////////////////////////////
	//!< 00	GPIO (reset value)		01	funcion 1
	//!< 11	funcion 3				10	funcion 2
#define		PINSEL		( ( __RW uint32_t * ) 0x4002C000UL )

	//!< //////////////////Registros PINMODE ///////////////////////////
#define		PINMODE		( ( __RW uint32_t * ) 0x4002C040UL )
#define		PINMODE_OD  ( ( __RW uint32_t * ) 0x4002C068UL )

	//!< ----------- Estados de PINMODE
	//!< 00	Pull Up resistor enable (reset value)		01	repeated mode enable
	//!< 11	Pull Down resistor enable					10	ni Pull Up ni Pull Down
	#define		PINMODE_PULLUP 		0
	#define		PINMODE_REPEAT 		1
	#define		PINMODE_NONE 		2
	#define		PINMODE_PULLDOWN 	3
	#define 	PINMODE_OD_DISABLED 0
	#define 	PINMODE_OD_ENABLED  1
	//!< ///////////////////   PCONP   //////////////////////////
	//!<  Power Control for Peripherals register (PCONP - 0x400F C0C4) [pag. 62 user manual LPC1769]
	//!< 0x400FC0C4UL : Direccion de inicio del registro de habilitación de dispositivos:
	#define 	PCONP	(* ( ( __RW uint32_t  * ) 0x400FC0C4UL ))


	//!< ///////////////////   PCLKSEL   //////////////////////////
	//!< Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 -0x400F C1A8 and PCLKSEL1 - 0x400F C1AC) [pag. 56 user manual]
	//!< 0x400FC1A8UL : Direccion de inicio de los registros de seleccion de los CLKs de los dispositivos:
	#define		PCLKSEL		( ( __RW uint32_t  * ) 0x400FC1A8UL )
	//!< Registros PCLKSEL
	#define		PCLKSEL0	PCLKSEL[0]
	#define		PCLKSEL1	PCLKSEL[1]


	//0xE000E100UL : Direccion de inicio de los registros de habilitación (set) de interrupciones en el NVIC:
	#define		ISER		( ( uint32_t * ) 0xE000E100UL )
	//0xE000E180UL : Direccion de inicio de los registros de deshabilitacion (clear) de interrupciones en el NVIC:
	#define		ICER		( (  uint32_t * ) 0xE000E180UL )


	//Registros ISER: Para habilitar las Interupciones Se activan con 1 Escribiendo un 0 no hace nada
	#define		ISER0		ISER[0]
	#define		ISER1		ISER[1]
	#define		ISE_UART1	ISER[0] |= (0x00000001 << 06)
	#define		ISE_EINT3	ISER[0] |= (0x00000001 << 21)  //ISER0->bit21 pongo un 1 en el bit 21 para habilitar la INT EINT3
    #define     ISE_EINT2	ISER[0] |= (0x00000001 << 20)  //ISER0->bit20 pongo un 1 en el bit 20 para habilitar la INT EINT2
    #define     ISE_EINT1	ISER[0] |= (0x00000001 << 19)  //ISER0->bit19 pongo un 1 en el bit 19 para habilitar la INT EINT1
    #define     ISE_EINT0	ISER[0] |= (0x00000001 << 18)  //ISER0->bit18 pongo un 1 en el bit 18 para habilitar la INT EINT1

	// Registro EXTMODE : Para seleccionar si la ISR Externa activa por flanco ó nivel
	#define		EXTMODE 		( (uint32_t  * ) 0x400FC148 )
	#define		EXTMODE3_F		EXTMODE[0] |= 0x00000001 << 3  // EINT3 por flanco
    #define		EXTMODE2_F		EXTMODE[0] |= 0x00000001 << 2  // EINT2 por flanco
    #define		EXTMODE1_F		EXTMODE[0] |= 0x00000001 << 1  // EINT1 por flanco
    #define		EXTMODE0_F		EXTMODE[0] |= 0x00000001       // EINT0 por flanco

	// Registro EXTPOLAR : selecciona Polaridad del EXTMODE
	#define    EXTPOLAR        ( (uint32_t  * ) 0x400FC14C )
    #define    EXTPOLAR3_P      EXTPOLAR[0] |= 0X00000001 << 3 // Flanco ó Nivel Positivo
    #define    EXTPOLAR2_P      EXTPOLAR[0] |= 0X00000001 << 2 // Flanco ó Nivel Positivo
    #define    EXTPOLAR1_P      EXTPOLAR[0] |= 0X00000001 << 1 // Flanco ó Nivel Positivo
    #define    EXTPOLAR0_P      EXTPOLAR[0] |= 0X00000001      // Flanco ó Nivel Positivo


	//Registros ICER: Para deshabilitar las Interupciones Se desactivan con 1 Escribiendo un 0 no hace nada
	//Registros ICER:

	#define		ICER0		ICER[0]
	#define		ICER1		ICER[1]
	#define		ICE_EINT3	ICER0 |= (0x00000001 << 21) // deshabilito a EINT3
    #define		ICE_EINT2	ICER0 |= (0x00000001 << 20) // deshabilito a EINT2
    #define		ICE_EINT1	ICER0 |= (0x00000001 << 19) // deshabilito a EINT1
    #define		ICE_EINT0	ICER0 |= (0x00000001 << 18) // deshabilito a EINT0


	#define		EXTINT 		( (uint32_t  * ) 0x400FC140UL ) // Reg de Flags para limpiar la ISR

	#define		CLR_EINT3		EXTINT[0] |= 0x00000001 << 3 // bajo el flag de EINT3
    #define		CLR_EINT2		EXTINT[0] |= 0x00000001 << 2 // bajo el flag de EINT2
    #define		CLR_EINT1		EXTINT[0] |= 0x00000001 << 1 // bajo el flag de EINT1
    #define		CLR_EINT0		EXTINT[0] |= 0x00000001      // bajo el flag de EINT0



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Registros de GPIO para usarse como ISR
    // Configuración
    #define      IO0IntEnR  ( (uint32_t  * ) 0x40028090UL ) // Puerto 0 como flanco ascendente
    #define      IO2IntEnR  ( (uint32_t  * ) 0x400280B0UL )//  Puerto 2 como flanco ascendente
    #define      IO0IntEnF  ( (uint32_t  * ) 0x40028094UL )// Puerto 0 como flanco descendente
    #define      IO2IntEnF  ( (uint32_t  * ) 0x400280B4UL )// Puerto 0 como flanco ascendente

   // Estado
    #define     IO0IntStatR  ( (uint32_t  * ) 0x40028084UL ) //Estado de los flags de interr flanco ascendente bits Puerto 0
    #define     IO2IntStatR  ( (uint32_t  * ) 0x400280A4UL ) //Estado de los flags de interr flanco ascendente bits Puerto 2
    #define     IO0IntStatF  ( (uint32_t  * ) 0x40028088UL ) //Estado de los flags de interr flanco descendente bits Puerto 0
    #define     IO2IntStatF  ( (uint32_t  * ) 0x400280A8UL ) //Estado de los flags de interr flanco descendente bits Puerto 2
    #define     IOIntStatus  ( (uint32_t  * ) 0x40028080UL ) //Estado de los flags de interr de bits Puerto 2 y Puerto 0

  //Bajo flags de Interr por GPIO
    #define     IO0IntClr  ( (uint32_t  * ) 0x4002808CUL ) //Bajo flags de Interr Puerto 0
	#define     IO2IntClr  ( (uint32_t  * ) 0x400280ACUL ) //Bajo flags de Interr Puerto 2

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


	//!< /////////////		SYSTICK		///////////////////////////
	//!< Tipo de dato específico para manejar el SYSTICK
	typedef struct
	{
		union{
			__RW uint32_t _STCTRL;
			struct{
				__RW uint32_t _ENABLE:1;
				__RW uint32_t _TICKINT:1;
				__RW uint32_t _CLKSOURCE:1;
				__RW uint32_t _RESERVED0:14;
				__RW uint32_t _COUNTFLAG:1;
				__RW uint32_t _RESERVED1:14;
			}bits;
		};
		__RW uint32_t _STRELOAD;
		__RW uint32_t _STCURR;
		__R uint32_t  _STCALIB;
	}systick_t;

	//!< 0xE000E010UL: Registro de control del SysTick:
	#define 	SYSTICK		( (systick_t *) 0xE000E010UL )

	#define		STCTRL		SYSTICK->_STCTRL

		#define	Systick_ENABLE			SYSTICK->bits._ENABLE
		#define	Systick_TICKINT			SYSTICK->bits._TICKINT
		#define	Systick_CLKSOURCE		SYSTICK->bits._CLKSOURCE
		#define	Systick_COUNTFLAG		SYSTICK->bits._COUNTFLAG

	#define		Systick_STRELOAD	SYSTICK->_STRELOAD
	#define		Systick_STCURR		SYSTICK->_STCURR
	#define		Systick_STCALIB		SYSTICK->_STCALIB

	/* Pulse Width Modulator (PWM) */
	#define PWM1_BASE_ADDR 0x40018000
	#define PWM1IR         (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x00))
	#define PWM1TCR        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x04))
	#define PWM1TC         (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x08))
	#define PWM1PR         (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x0C))
	#define PWM1PC         (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x10))
	#define PWM1MCR        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x14))
	#define PWM1MR0        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x18))
	#define PWM1MR1        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x1C))
	#define PWM1MR2        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x20))
	#define PWM1MR3        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x24))
		#define PWM1CCR        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x28))
	#define PWM1CR0        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x2C))
	#define PWM1CR1        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x30))
	#define PWM1CR2        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x34))
	#define PWM1CR3        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x38))
	#define PWM1MR4        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x40))
	#define PWM1MR5        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x44))
	#define PWM1MR6        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x48))
	#define PWM1PCR        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x4C))
	#define PWM1LER        (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x50))
	#define PWM1CTCR       (*(volatile unsigned long *)(PWM1_BASE_ADDR + 0x70))

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			//Estructura para manejar los Timers:
			typedef struct
			{
				uint32_t	IR;
				uint32_t	TCR;
				uint32_t 	TC;
				uint32_t 	PR;
				uint32_t 	PC;
				uint32_t 	MCR;
				uint32_t	MR0;
				uint32_t	MR1;
				uint32_t	MR2;
				uint32_t	MR3;
				uint32_t	CCR;
				uint32_t	CR0;
				uint32_t	CR1;        // 0x40004030 para el T0
				uint32_t    RESERVE[2]; // 0x40004034 para el T0
				                        // 0x40004038 para el T0
				uint32_t    EMR;        // 0x4000403C para el T0

			} timer_t;

			//Timers:
			//0x40004000UL : Direccion de inicio de los registros del Timer0
			#define		DIR_TIMER0		( (__RW timer_t  * ) 0x40004000UL )
			//0x40008000UL : Direccion de inicio de los registros del Timer1
			#define		DIR_TIMER1		( (__RW timer_t  * ) 0x40008000UL )
			//0x40090000UL : Direccion de inicio de los registros del Timer2
			#define		DIR_TIMER2		( ( __RW timer_t  * ) 0x40090000UL )
			//0x40094000UL : Direccion de inicio de los registros del Timer3
			#define		DIR_TIMER3		( (__RW timer_t  * ) 0x40094000UL )

			//Registros de timers:
			#define		TIMER0		DIR_TIMER0[0]
			#define		TIMER1		DIR_TIMER1[0]
			#define		TIMER2		DIR_TIMER2[0]
			#define		TIMER3		DIR_TIMER3[0]
	 // Timer 0
		#define		T0IR		TIMER0.IR
	    #define     T0_IR_MR0   T0IR & 0X01       // Bit 0 del registro IR corresponde al flag de la Int por Match 0
	    #define     T0_IR_MR1   (T0IR & (0X01 <<1))>>1 // Bit 1 del registro IR corresponde al flag de la Int por Match 1
	    #define     T0_IR_MR2   (T0IR & (0X01 <<2))>>2 // Bit 2 del registro IR corresponde al flag de la Int por Match 2
	    #define     T0_IR_MR3   (T0IR & (0X01 <<3))>>3 // Bit 3 del registro IR corresponde al flag de la Int por Match 3
	    #define     T0_IR_CR0   (T0IR & (0X01 <<1))>>4 // Bit 4 del registro IR corresponde al flag de la Int por CAPTURE 0
	    #define     T0_IR_CR1   (T0IR & (0X01 <<1))>>5 // Bit 5 del registro IR corresponde al flag de la Int por CAPTURE 1

		#define 	T0CTCR  	(* ( __RW uint32_t*) 0x40004070UL )
		//Timer 1
		#define		T1IR		TIMER1.IR
	    #define     T1_IR_MR0   T1IR & 0X01       // Bit 0 del registro IR corresponde al flag de la Int por Match 0
	    #define     T1_IR_MR1   (T1IR & (0X01 <<1))>>1 // Bit 1 del registro IR corresponde al flag de la Int por Match 1
	    #define     T1_IR_MR2   (T1IR & (0X01 <<2))>>2 // Bit 2 del registro IR corresponde al flag de la Int por Match 2
	    #define     T1_IR_MR3   (T1IR & (0X01 <<3))>>3 // Bit 3 del registro IR corresponde al flag de la Int por Match 3
	    #define     T1_IR_CR0   (T1IR & (0X01 <<1))>>4 // Bit 4 del registro IR corresponde al flag de la Int por CAPTURE 0
	    #define     T1_IR_CR1   (T1IR & (0X01 <<1))>>5 // Bit 5 del registro IR corresponde al flag de la Int por CAPTURE 1



	    #define		T2IR		TIMER2.IR
		#define		T3IR		TIMER3.IR

		#define		T0TCR		TIMER0.TCR
		#define		T1TCR		TIMER1.TCR
		#define		T2TCR		TIMER2.TCR
		#define		T3TCR		TIMER3.TCR

		#define		T0TC		TIMER0.TC
		#define		T1TC		TIMER1.TC
		#define		T2TC		TIMER2.TC
		#define		T3TC		TIMER3.TC

		#define		T0PC		TIMER0.PC
		#define		T1PC		TIMER1.PC
		#define		T2PC		TIMER2.PC
		#define		T3PC		TIMER3.PC

		#define		T0MCR		TIMER0.MCR
		#define		T1MCR		TIMER1.MCR
		#define		T2MCR		TIMER2.MCR
		#define		T3MCR		TIMER3.MCR

		#define		T0MR0		TIMER0.MR0
		#define		T1MR0		TIMER1.MR0
		#define		T2MR0		TIMER2.MR0
		#define		T3MR0		TIMER3.MR0

		#define		T0MR1		TIMER0.MR1
		#define		T1MR1		TIMER1.MR1
		#define		T2MR1		TIMER2.MR1
		#define		T3MR1		TIMER3.MR1

		#define		T0MR2		TIMER0.MR2
		#define		T1MR2		TIMER1.MR2
		#define		T2MR2		TIMER2.MR2
		#define		T3MR2		TIMER3.MR2

		#define		T0MR3		TIMER0.MR3
		#define		T1MR3		TIMER1.MR3
		#define		T2MR3		TIMER2.MR3
		#define		T3MR3		TIMER3.MR3

		#define		T0CCR		TIMER0.CCR
		#define		T1CCR		TIMER1.CCR
		#define		T2CCR		TIMER2.CCR
		#define		T3CCR		TIMER3.CCR

		#define		T0CR0		TIMER0.CR0
		#define		T1CR0		TIMER1.CR0
		#define		T2CR0		TIMER2.CR0
		#define		T3CR0		TIMER3.CR0

		#define		T0CR1		TIMER0.CR1
		#define		T1CR1		TIMER1.CR1
		#define		T2CR1		TIMER2.CR1
		#define		T3CR1		TIMER3.CR1

	    #define		T0EMR		TIMER0.EMR
		#define		T1EMR		TIMER1.EMR
		#define		T2EMR		TIMER2.EMR
		#define		T3EMR		TIMER3.EMR
	    #define     EMCO_TOGGLE  T1EMR|=(0x3 << 4) // Configurado como TOGGLE el pin MAT1.0
	    #define     EMC1_TOGGLE  T1EMR|=(0x3 << 6) //  Configurado como TOGGLE el pin MAT1.1
       #define     T1PR         TIMER1.PR
       #define     T0PR         TIMER0.PR



	/* D/A Converter (DAC) */
	#define DAC_BASE_ADDR  0x4008C000
	#define DACR           (*(volatile unsigned long *)(DAC_BASE_ADDR + 0x00))
	#define DACCTRL        (*(volatile unsigned long *)(DAC_BASE_ADDR + 0x04))
	#define DACCNTVAL      (*(volatile unsigned long *)(DAC_BASE_ADDR + 0x08))


	/* Motor Control PWM */
	#define MCPWM_BASE_ADDR 0x400B8000
	#define MCCON          (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x00))
	#define MCCON_SET      (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x04))
	#define MCCON_CLR      (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x08))
	#define MCCAPCON       (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x0C))
	#define MCCAPCON_SET   (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x10))
	#define MCCAPCON_CLR   (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x14))
	#define MCTIM0         (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x18))
	#define MCTIM1         (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x1C))
	#define MCTIM2         (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x20))
	#define MCPER0         (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x24))
	#define MCPER1         (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x28))
	#define MCPER2         (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x2C))
	#define MCPW0          (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x30))
	#define MCPW1          (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x34))
	#define MCPW2          (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x38))
	#define MCDEADTIME     (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x3C))
	#define MCCCP          (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x40))
	#define MCCR0          (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x44))
	#define MCCR1          (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x48))
	#define MCCR2          (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x4C))
	#define MCINTEN        (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x50))
	#define MCINTEN_SET    (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x54))
	#define MCINTEN_CLR    (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x58))
	#define MCCNTCON       (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x5C))
	#define MCCNTCON_SET   (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x60))
	#define MCCNTCON_CLR   (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x64))
	#define MCINTFLAG      (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x68))
	#define MCINTFLAG_SET  (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x6C))
	#define MCINTFLAG_CLR  (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x70))
	#define MCCAP_CLR      (*(volatile unsigned long *)(MCPWM_BASE_ADDR + 0x74))


#endif /* REGS_H_ */
