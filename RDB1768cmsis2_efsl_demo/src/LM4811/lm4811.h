/*
 * lm4811.h
 *
 *  Created on: 29/01/2015
 *      Author: froux
 */

#ifndef LM4811_H_
#define LM4811_H_

/******************************************************************************************
 ** DEFINICION DE CONSTANTES :
 ******************************************************************************************/

// PINES LM4811 Y REGISTROS :

// PIN SD : P0.4
#define LM4811_SD_PINSEL_REG 				(LPC_PINCON->PINSEL0)
#define LM4811_SD_PINSEL_MAS				((0x03 << 10))
#define LM4811_SD_FIODIR_REG				(LPC_GPIO0->FIODIR)
#define LM4811_SD_FIOSET_REG				(LPC_GPIO0->FIOSET)
#define LM4811_SD_FIOCLR_REG				(LPC_GPIO0->FIOCLR)
#define LM4811_SD_FIODIR_MAS				((0x01 << 5))

#define ACT_PIN_LM4811_SD()					(LM4811_SD_FIOSET_REG |= LM4811_SD_FIODIR_MAS)
#define PAS_PIN_LM4811_SD()					(LM4811_SD_FIOCLR_REG |= LM4811_SD_FIODIR_MAS)

// PIN UP/DN : P0.5
#define LM4811_UPDN_PINSEL_REG				(LPC_PINCON->PINSEL0)
#define LM4811_UPDN_PINSEL_MAS				((0x03 << 8))
#define LM4811_UPDN_FIODIR_REG				(LPC_GPIO0->FIODIR)
#define LM4811_UPDN_FIOSET_REG				(LPC_GPIO0->FIOSET)
#define LM4811_UPDN_FIOCLR_REG				(LPC_GPIO0->FIOCLR)
#define LM4811_UPDN_FIODIR_MAS				((0x01 << 4))

#define ACT_PIN_LM4811_UPDN()				(LM4811_UPDN_FIOSET_REG |= LM4811_UPDN_FIODIR_MAS)
#define PAS_PIN_LM4811_UPDN()				(LM4811_UPDN_FIOCLR_REG |= LM4811_UPDN_FIODIR_MAS)

// PIN CLOCK : P2.0
#define LM4811_CLOCK_PINSEL_REG 			(LPC_PINCON->PINSEL4)
#define LM4811_CLOCK_PINSEL_MAS				((0x03 << 00))
#define LM4811_CLOCK_FIODIR_REG				(LPC_GPIO2->FIODIR)
#define LM4811_CLOCK_FIOSET_REG				(LPC_GPIO2->FIOSET)
#define LM4811_CLOCK_FIOCLR_REG				(LPC_GPIO2->FIOCLR)
#define LM4811_CLOCK_FIODIR_MAS				((0x01 << 0))

#define ACT_PIN_LM4811_CLOCK()				(LM4811_CLOCK_FIOSET_REG |= LM4811_CLOCK_FIODIR_MAS)
#define PAS_PIN_LM4811_CLOCK()				(LM4811_CLOCK_FIOCLR_REG |= LM4811_CLOCK_FIODIR_MAS)


/******************************************************************************************
 ** FUNCIONES EXTERNAS :
 ******************************************************************************************/

extern void Inic_LM4811 (void);
extern void Reiniciar_LM4811 (void);
extern int Incrementar_Ganancia_3dB_LM4811 (void);
extern int Decrementar_Ganancia_3dB_LM4811 (void);

/******************************************************************************************
 ** VARIABLES PÚBLICAS :
 ******************************************************************************************/

extern int8_t ganancia_lm4811;

#endif /* LM4811_H_ */
