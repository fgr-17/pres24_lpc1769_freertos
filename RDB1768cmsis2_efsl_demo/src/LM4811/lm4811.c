/*
 * lm4811.c
 *
 *  Created on: 29/01/2015
 *      Author: froux
 */

/******************************************************************************************
 ** INCLUSIÓN DE ARCHIVOS :
 ******************************************************************************************/

#include <LPC17xx.h>
#include <stdint.h>

#include <LM4811/lm4811.h>

/******************************************************************************************
 ** PROTOTIPOS DE FUNCIONES :
 ******************************************************************************************/

void Inic_LM4811 (void);
void Reiniciar_LM4811 (void);
int Incrementar_Ganancia_3dB_LM4811 (void);
int Decrementar_Ganancia_3dB_LM4811 (void);

/******************************************************************************************
 ** VARIABLES GLOBALES :
 ******************************************************************************************/

int8_t ganancia_lm4811 = 0;

/*
 ========================================================================================
 =	 		IMPLEMENTACION DE FUNCIONES													=
 ========================================================================================
 */

/******************************************************************************************
 *
 * \function		void Inic_LM4811 (void)
 *
 * \brief			Inicializo pines del LM4811 y le aplico el shutdown
 * \brief 	 		Hardware del LM4811 :
 * \brief 	 		- VIN1 y VIN2 (pines 2 y 8): conectados a la salida del DAC a través de un cap. c/u (MONO)
 * \brief 	 		- SD (shutdown pin 7) : P0.5 - J39
 * \brief 	 		- UP/DN (pin 6) : P0.4 - J38
 * \brief 	 		- CLOCK (pin4) : P2.0 - J42
 *
 ******************************************************************************************/

void Inic_LM4811 (void)
{

	// Inicializo todos los pines del LM4811 como salida y en cero :

	LM4811_CLOCK_PINSEL_REG &= ~LM4811_CLOCK_PINSEL_MAS;		// Pongo en cero los pines del PINSEL : GPIO
	LM4811_CLOCK_FIODIR_REG |= LM4811_CLOCK_FIODIR_MAS;			// Selecciono pin como salida
	PAS_PIN_LM4811_CLOCK();

	LM4811_UPDN_PINSEL_REG &= ~LM4811_UPDN_PINSEL_MAS;			// Pongo en cero los pines del PINSEL : GPIO
	LM4811_UPDN_FIODIR_REG |= LM4811_UPDN_FIODIR_MAS;			// Selecciono pin como salida
	PAS_PIN_LM4811_UPDN();

	LM4811_SD_PINSEL_REG &= ~LM4811_SD_PINSEL_MAS;				// Pongo en cero los pines del PINSEL : GPIO
	LM4811_SD_FIODIR_REG |= LM4811_SD_FIODIR_MAS;				// Selecciono pin como salida
	ACT_PIN_LM4811_SD();										// Pongo en alto el pin de shutdown
	PAS_PIN_LM4811_SD();										// Lo vuelvo a bajar

	ganancia_lm4811 = 0;										// Pongo la variable de ganancia en cero

	return;
}

/******************************************************************************************
 *
 * \function		void Reiniciar_LM4811 (void)
 *
 * \brief			Aplico el shutdown y reinicio valor de ganancia
 *
 ******************************************************************************************/

void Reiniciar_LM4811 (void)
{
	ACT_PIN_LM4811_SD();										// Pongo en alto el pin de shutdown
	PAS_PIN_LM4811_SD();										// Lo vuelvo a bajar
	ganancia_lm4811 = 0;
	return;
}

/******************************************************************************************
 *
 * \function		int Incrementar_Ganancia_3dB_LM4811 (void)
 *
 * \brief			Aumento la ganancia en +3dB con flanco ascendente del pin CLOCK
 * \brief			Ver SNAS119D pág. 12
 *
 * \return 			0 : éxito
 * \return			1 : ya había llegado al máximo +12dB. no se aumentó la ganancia
 *
 ******************************************************************************************/

int Incrementar_Ganancia_3dB_LM4811 (void)
{
	if (ganancia_lm4811 >= 12) return 1;						// Ya estoy en el máximo

	PAS_PIN_LM4811_CLOCK();										// Pasivo el pin por si había quedado arriba
	ACT_PIN_LM4811_UPDN();										// Activo el pin para subir el volumen
	ACT_PIN_LM4811_CLOCK();										// Flanco asc. del clock
	PAS_PIN_LM4811_CLOCK();										// Vuelvo a bajar el pin
	ganancia_lm4811 += 3;										// Incremento la ganancia en +3dB
	return 0;
}

/******************************************************************************************
 *
 * \function		int Decrementar_Ganancia_3dB_LM4811 (void)
 *
 * \brief			Decremento la ganancia en -3dB con flanco ascendente del pin CLOCK
 * \brief			Ver SNAS119D pág. 12
 *
 * \return 			0 : éxito
 * \return			1 : ya había llegado al mínimo -33dB. no se aumentó la ganancia
 *
 ******************************************************************************************/

int Decrementar_Ganancia_3dB_LM4811 (void)
{
	if (ganancia_lm4811 <= -33) return 1;						// Ya estoy en el mínimo

	PAS_PIN_LM4811_CLOCK();										// Pasivo el pin por si había quedado arriba
	PAS_PIN_LM4811_UPDN();										// Pasivo el pin para bajar el volumen
	ACT_PIN_LM4811_CLOCK();										// Flanco asc. del clock
	PAS_PIN_LM4811_CLOCK();										// Vuelvo a bajar el pin clock
	ganancia_lm4811 -= 3;										// Incremento la ganancia en -3dB
	return 0;
}

