
#ifndef __QRS_DET__H_
#define __QRS_DET__H_

// Prototypes.
signed int hpfilt( signed int datum, int init ) ;
signed int lpfilt( signed int datum ,int init) ;
signed int deriv1( signed int x0, int init ) ;
int mvwint(signed int datum, int init) ;
signed int QRSDetector(signed int, int init) ;
signed int Peak( signed int datum, int init ) ;
void UpdateQ(int newQ) ;
void UpdateRR(int newRR) ;
void UpdateN(int newN) ;


// Time interval constants. Asume 200 Hz de f muestreo

#define MS80	16
#define MS95	19
#define MS150	30
#define MS200	40
#define MS360	72
#define MS450	90
#define MS1000	200
#define MS1500	300

#define WINDOW_WIDTH	MS80
#define FILTER_DELAY	21 + MS200






#define BPM_MEAN_SAMPLES		10
#define BPM_FACTOR 				12000







#endif
