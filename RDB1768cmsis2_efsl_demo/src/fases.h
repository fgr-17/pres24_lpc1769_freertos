#ifndef FASES_H_
#define FASES_H_

#define MESSAGE printf
#define SIZE_BLOQUE_GUARDADO 512
#define RGRAF 1
#define SIZEBUF4 2048

#include "QRS_Det.h"
#include "efs.h"
#include "estados.h"
#include "pulsador.h"
#include "LPC17xx.h"


extern EmbeddedFile filew;
volatile unsigned int msTicks;


extern unsigned char buf4[];
unsigned char buf4Aux[5]; //GLOBITO ELIMINAR

extern unsigned short bloque;
extern int CantDatosGenerados;
extern int CantEnviados;
extern unsigned int datoECG2;

extern int vectorQRS[1600];
extern int vectorPulsador[10];
extern int cantQRS;
extern int cantPulsador;

extern int IGraf;
extern int posicTag;
extern char nuevodato;
extern int estado;

unsigned int BPM_Buff[BPM_MEAN_SAMPLES];
unsigned int BPM_cursor;
signed int QRS_stat;
extern int index;
extern int cambiarVol;
EmbeddedFile fileM;


//Detector Nuestro
extern   int nactual;
extern  int umbral;
extern   int nignorar;
extern   int max;
extern   int promedio;
extern  long long suma;
extern   int inicializo;


//extern int antes[400]; //GLOBITO ELIMINAR
//extern int desp[400]; //GLOBITO ELIMINAR
extern int xxx;//GLOBITO ELIMINAR

int detectar( int  muestra);



extern int AnteriorQRS;

__inline void AccionFaseMotora();

__inline void AccionFaseIntero1();
__inline void AccionFaseRetro();
__inline void AccionFaseIntero2();
//__inline void AccionFaseInteroVolVariable();


void SetearVolumen(int);

//extern void delayms (uint32_t delayTicks);
/*__inline static void delayms (unsigned int  delayTicks) {
	unsigned int  currentTicks;
  currentTicks = msTicks;	// read current tick counter
  while ((msTicks - currentTicks) < delayTicks); // Now loop until required number of ticks passes.
}*/

#endif /* FASES_H_ */
