#include "fases.h"

int detectar( int muestra){
	muestra=muestra*256;
	if(!inicializo){
		if(nactual==0)
			max=muestra;
		suma=suma+muestra;
		if(muestra>max)
			max=muestra;
		if(nactual++==500){
			promedio=suma/500;
			suma=muestra;
			umbral=(max-promedio)*0.9+promedio;
			max=-2147483648;
			nactual=0;
			inicializo=1;
		}
		return 0;
	}

	if(++nactual==500){
		promedio=suma/500;
		suma=0;
		umbral=(max-promedio)*0.8+promedio;
		max=-2147483648;
		nactual=0;
	}
	suma=suma+muestra;
	if(muestra>max)
		max=muestra;
	//if(muestra==0)
	//	MESSAGE("llegue");

	if(nignorar<=0){
		if(muestra>umbral){
			nignorar=167;
			return 1;
		}
	}else
		nignorar--;
	return 0;
}


__inline void AccionFaseMotora(){
	int cantw;
	cantPulsador=0;
	cantQRS=0;

	int deltaGuardado=0;

	while(estado==EstadoPA1 || estado==EstadoPB5)
	{
		Antirreb_ME();
		if(boton_estado == PRES){
			boton_estado = SUELTO;
			Enviar3('f','t','p'); //Pulsador ->Envio a PC
			vectorPulsador[cantPulsador++]=CantDatosGenerados;
		}

		if(nuevodato=='s'){
			nuevodato='n';
			if(!(CantDatosGenerados%500)){   //GLOBITO vector
				vectorQRS[cantQRS++]=CantDatosGenerados;
				Enviar3('f','t','q');

			}

			if(((CantDatosGenerados-128)%(SIZE_BLOQUE_GUARDADO/4))==0 &&(CantDatosGenerados!=128)){
				deltaGuardado=CantDatosGenerados;
				//MESSAGE("\n%d dat gen",CantDatosGenerados);
				cantw=file_write(&filew,SIZE_BLOQUE_GUARDADO,buf4+bloque*SIZE_BLOQUE_GUARDADO);

				deltaGuardado=CantDatosGenerados-deltaGuardado;
				bloque++;
				if(bloque>1) bloque=0;
			}
		}
	}
}

__inline void AccionFaseIntero(){
	int cantw;
	int a; //GLOBITO ELIMINAR
	Antirreb_ME();
	if(boton_estado == PRES){
		boton_estado = SUELTO;
		Enviar3('f','t','p'); //Pulsador ->Envio a PC
		vectorPulsador[cantPulsador++]=CantDatosGenerados;
	}


	if(nuevodato=='s'){
		if(IGraf==RGRAF){ //envio dato a Pc para graficar
			CantEnviados++;
			EnviarBuf4();
			IGraf=1;
		}
		else
			IGraf++;

		nuevodato='n';
		if(((CantDatosGenerados-128)%(SIZE_BLOQUE_GUARDADO/4))==0 &&(CantDatosGenerados!=128)){

		//	for(a=0;a<SIZE_BLOQUE_GUARDADO;a++)
		//		buf4Aux[a]=buf4[bloque*SIZE_BLOQUE_GUARDADO+a];

			//MESSAGE("\n%d dat gen",CantDatosGenerados);
		//	cantw=file_write(&filew,SIZE_BLOQUE_GUARDADO,buf4Aux);
			cantw=file_write(&filew,SIZE_BLOQUE_GUARDADO,buf4+bloque*SIZE_BLOQUE_GUARDADO);
			//index=0;
			bloque++;
			if(bloque>3) bloque=0;

		}
		//datoECG2=0;


			/*if(index==0)
				cantw=file_write(&filew,4,buf4+2048-4);
			else
				cantw=file_write(&filew,4,buf4+index-4);
			 */




		if(index)
		    datoECG2=buf4[index-1]+buf4[index-2]*256+buf4[index-3]*256*256;
		else
		    datoECG2=buf4[2048-1]+buf4[2048-2]*256+buf4[2048-3]*256*256;
		    //QRS_stat = QRSDetector(datoECG2,0);

		QRS_stat=detectar(datoECG2);
		if(QRS_stat!=0){
			//enable_timer(1); //Dispara sonido
			if((CantDatosGenerados-AnteriorQRS)>0){
			BPM_Buff[BPM_cursor]=posicTag;//CantDatosGenerados;

			LPC_GPIO2->FIOSET |= (0x01 << 13);
			enable_timer(0);
			Enviar3('f','t','q');
			vectorQRS[cantQRS++]=CantDatosGenerados;//CantDatosGenerados;

			if (BPM_cursor>=(BPM_MEAN_SAMPLES-1))
					BPM_cursor = 0;
			else
					BPM_cursor++;
			posicTag=0;

			AnteriorQRS=CantDatosGenerados;
			}
		}

	}

}






 void SetearVolumen(int volu){

	 cambiarVol=0;
}

