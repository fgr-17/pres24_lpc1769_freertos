/***************************************************************
 * pulsador.c
 *
 *  Created on: 19/12/2014
 *      Author: froux
 ***************************************************************/

/***************************************************************
 **	Inclusión de archivos
 ***************************************************************/

#include <LPC17xx.h>
#include <stdint.h>
#include <pulsador.h>

/***************************************************************
 **	Prototipos de Funciones
 ***************************************************************/

void Antirreb_Inic (void);
void Antirreb_ME (void);
void Inicializar_Pin_Boton (void);

/***************************************************************
 **	Variables globales
 ***************************************************************/

t_antirreb_estado antirreb_estado;
t_boton_estado boton_estado;

volatile uint32_t ms_contador_antirreb = 0;				// Contador para la rutina antirrebote
volatile uint8_t antirreb_activo = 0;

/***************************************************************
 **	Implementación de Funciones
 ***************************************************************/

/***************************************************************
 *
 * \function 	void Antirreb_Inic (void)
 *
 * \brief 		Inicialización de la máquina de estados del botón
 *
 * \paramin 	none
 *
 * \return 		none
 *
 * \author 		Roux, Federico G. (froux@favaloro.edu.ar)
 *
 ***************************************************************/

void Antirreb_Inic (void)
{
	// Tiene que estar activado el systick para que funcione
	ms_contador_antirreb = 0;
	antirreb_activo = 0;
	antirreb_estado = INACTIVO;
	return;
}

/***************************************************************
 *
 * \function 	void Antirreb_ME (void)
 *
 * \brief 		Máquina de estados Antirrebote
 *
 * \paramin 	none
 *
 * \return 		none
 *
 * \author 		Roux, Federico G. (froux@favaloro.edu.ar)
 *
 ***************************************************************/

void Antirreb_ME (void)
{
	switch(antirreb_estado)
	{
	case INACTIVO:
		return;

	case ANTIRREB_INI:

		if(ms_contador_antirreb >= MS_CONTADOR_ANTIRREB_VENTANA_FLANCO)
		{
			if(BOTON_PRESIONADO)
			{
				ms_contador_antirreb = 0;
				antirreb_activo = 1;
				antirreb_estado = PRESIONADO;
				boton_estado = PRES;
				return;
			}
			else
			{
				ms_contador_antirreb = 0;
				antirreb_activo = 0;
				antirreb_estado = INACTIVO;
				boton_estado = RUIDO;
				return;
			}
		}
		else
		{
			antirreb_estado = ANTIRREB_INI;
			return;
		}

	case PRESIONADO:

		if(ms_contador_antirreb >= MS_CONTADOR_ANTIRREB_VENTANA_PRESIONADO)
		{
			if(BOTON_PRESIONADO)
			{
				ms_contador_antirreb = 0;
				antirreb_activo = 1;
				antirreb_estado = MANTENIDO;
				boton_estado = MANT;
				return;
			}
			else
			{
				ms_contador_antirreb = 0;
				antirreb_activo = 1;
				antirreb_estado = ANTIRREB_FIN;
				// boton_estado = PRES;
				return;
			}
		}
		else
		{
			antirreb_estado = PRESIONADO;
			return;
		}

	case MANTENIDO :

		if(ms_contador_antirreb >= MS_CONTADOR_ANTIRREB_VENTANA_MANTENIDO)
		{
			if(BOTON_PRESIONADO)
			{
				ms_contador_antirreb = 0;
				antirreb_activo = 1;
				antirreb_estado = MANTENIDO;
				boton_estado = MANT;
				return;
			}
			else
			{
				ms_contador_antirreb = 0;
				antirreb_activo = 1;
				antirreb_estado = ANTIRREB_FIN;
				// boton_estado = PRES;
				return;
			}
		}
		else
		{
			antirreb_estado = MANTENIDO;
			return;
		}
		break;

	case MANTENIDO_FIN :

		if(ms_contador_antirreb >= MS_CONTADOR_ANTIRREB_VENTANA_PRESIONADO)
		{
			if(BOTON_PRESIONADO)
			{
				ms_contador_antirreb = 0;
				antirreb_activo = 1;
				antirreb_estado = MANTENIDO;
				boton_estado = MANT;
				return;
			}
			else
			{
				ms_contador_antirreb = 0;
				antirreb_activo = 1;
				antirreb_estado = ANTIRREB_FIN;
				// boton_estado = PRES;
				return;
			}
		}
		else
		{
			antirreb_estado = MANTENIDO_FIN;
			return;
		}
		break;

	case ANTIRREB_FIN:

 		if(ms_contador_antirreb >= MS_CONTADOR_ANTIRREB_VENTANA_FLANCO)
		{
			if(BOTON_PRESIONADO)
			{
				// Se mantuvo apretado se solto y se volvio a apretar en poco tiempo...
				ms_contador_antirreb = 0;
				antirreb_activo = 0;
				antirreb_estado = INACTIVO;
				boton_estado = RUIDO;
				return;
			}
			else
			{
				// Se mantuvo apretado se solto y se volvio a apretar en poco tiempo...
				ms_contador_antirreb = 0;
				antirreb_activo = 0;
				antirreb_estado = INACTIVO;
				//boton_estado = PRES;
				// Antirreb_Inic();
				Inicializar_Pin_Boton();
				// LPC_GPIOINT->IO2IntEnR |= GPIOINT_IO2INTENR_P211;				// Habilito interrupción en el flanco de bajada
				return;
			}
		}
		else
		{
			antirreb_estado = ANTIRREB_FIN;
			return;
		}
		break;
	default:
		Antirreb_Inic();
		return;
	}
	return;
}

/************************************************************************
 *
 *	\function	void Inicializar_Pin_Boton (void)
 *
 * 	\brief 		Inicializacion del pulsador
 *
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *
 ************************************************************************/

void Inicializar_Pin_Boton (void)
{
	NVIC_DisableIRQ(EINT3_IRQn);
	LPC_PINCON->PINSEL4 &= ~(0x03UL << 22);							// Entrada de DRDY
	LPC_GPIO2->FIODIR |= (0x00 << 11);								// P2.11 seteado como entrada

	LPC_GPIOINT->IO2IntEnR |= GPIOINT_IO2INTENR_P211;				// Habilito interrupción en el flanco de bajada
	NVIC_EnableIRQ(EINT3_IRQn);

	return;
}

/***************************************************************
 **	Fin del archivo
 ***************************************************************/
