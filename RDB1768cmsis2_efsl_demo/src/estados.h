/*
 * estados.h
 *
 *  Created on: 30/10/2014
 *      Author: MarianoR - RominaF
 */

#ifndef ESTADOS_H_
#define ESTADOS_H_

#define EstadoPre_bin 	-1

#define EstadoInicio 	0

#define EstadoEsperaPA1 1
#define EstadoPA1 		2			//Protocolo A Fase 1
#define EstadoPAEG1 	3		//Espera Guardar de Protocolo A Fase 1
#define EstadoPAB1		4		//Borrar Protocolo A Fase 1

#define EstadoEsperaPA2 5		//Espera al Play de PA fase 2
#define EstadoPA2 		6
#define EstadoPAEG2 	7
#define EstadoPAB2		8

#define EstadoEsperaPA3 9
#define EstadoPA3 		10
#define EstadoPAEG3 	11
#define EstadoPAB3		12

#define EstadoEsperaPA4 13
#define EstadoPA4 		14
#define EstadoPAEG4 	15
#define EstadoPAB4		16

#define EstadoFinEG		17
#define EstadoEnviarTodo 18

#define EstadoSalirTodo 19

#define EstadoCancel1	20   	//Cancelar
#define EstadoCancel2	21
#define EstadoCancel3	22
#define EstadoCancel4	23
#define EstadoCancel5	24
#define EstadoCancel6	25

#define EstadoEsperaPB5 26
#define EstadoPB5 		27
#define EstadoPBEG5 	28
#define EstadoPBB5		29

#define EstadoEsperaPB6 30
#define EstadoPB6		31
#define EstadoPBEG6 	32
#define EstadoPBB6		33

#define EstadoGPQPA1	34		//Guardar Ps y Qs
#define EstadoGPQPA2	35
#define EstadoGPQPA3	36
#define EstadoGPQPA4	37
#define EstadoGPQPB5	38
#define EstadoGPQPB6	39



#endif /* ESTADOS_H_ */
