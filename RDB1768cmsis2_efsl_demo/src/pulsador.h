/*
 * pulsador.h
 *
 *  Created on: 19/12/2014
 *      Author: froux
 */

#ifndef PULSADOR_H_
#define PULSADOR_H_

/***************************************************************
 **	Definición de constantes
 ***************************************************************/

// Bits de los registros asociados a la interrupción por flanco ascendente del botón :
#define GPIOINT_INTSTAT_P0INT							(0x01 << 0)						// Interrupcion pendiente en P0
#define GPIOINT_INTSTAT_P2INT							(0x01 << 2)						// Interrupcion pendiente en P2
#define GPIOINT_INTSTAT_P211REI							(0x01 << 11)					// Interrupcion pendiente en flanco desc. del P0.0
#define GPIOINT_INTCLR_P211CI							(0x01 << 11)					// Bit para limpiar la irq. pendiente
#define GPIOINT_IO2INTENR_P211							(0x01 << 11)

// Tiempos de espera de la rutina antirrebote [ms] :
#define MS_CONTADOR_ANTIRREB_VENTANA_FLANCO				100	 							// Tiempo de espera
#define MS_CONTADOR_ANTIRREB_VENTANA_PRESIONADO			100	 							// Tiempo de espera
#define MS_CONTADOR_ANTIRREB_VENTANA_MANTENIDO			50								// Tiempo de espera mantenido


/***************************************************************
 **	Declaración de macros :
 ***************************************************************/

#define BOTON_PRESIONADO								((LPC_GPIO2->FIOPIN & (0x01 << 11)))

/***************************************************************
 **	Definición de tipos de dato
 ***************************************************************/

typedef enum {INACTIVO, ANTIRREB_INI, PRESIONADO, MANTENIDO, MANTENIDO_FIN, ANTIRREB_FIN} t_antirreb_estado;
typedef enum {RUIDO, PRES, MANT, SUELTO} t_boton_estado;

/***************************************************************
 **	Prototipos de funciones :
 ***************************************************************/

extern void Antirreb_Inic (void);
extern void Antirreb_ME (void);
extern void Inicializar_Pin_Boton (void);

/***************************************************************
 **	Variables públicas a todos los módulos :
 ***************************************************************/

extern volatile unsigned int ms_contador_antirreb;				// Contador para la rutina antirrebote
extern volatile unsigned char antirreb_activo;

extern t_antirreb_estado antirreb_estado;
extern t_boton_estado boton_estado;

#endif /* PULSADOR_H_ */
