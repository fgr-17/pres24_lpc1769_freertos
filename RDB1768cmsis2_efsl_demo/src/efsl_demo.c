//*****************************************************************************
// Interocepcion Cardiaca
//			Romina Farbairn y Mariano Ratto
//					Universidad Favaloro, 2014
//
//*****************************************************************************
#include "efsl_demo.h"



__inline int Es(char a, char b){
	if(bufr[1]==a && bufr[2]==b)
		return 1;
	else
		return 0;
}

__inline void EnviarBuf4(){
	index = 4;
	while( (LPC_UART0->LSR & LSR_THRE) == 0 );	// Block until tx empty
	LPC_UART0->THR = buf4[index-4];
	LPC_UART0->THR = buf4[index-3];
	LPC_UART0->THR = buf4[index-2];
	LPC_UART0->THR = buf4[index-1];
	LPC_UART0->THR = buf4[index-2]^buf4[index-3]^buf4[index-1]^buf4[index-4];

}
// -- Handlers -- //
void SysTick_Handler(void) {
  msTicks++;

  if(antirreb_activo)
	  ms_contador_antirreb++;
}

void TIMER0_IRQHandler (void)
{
  LPC_TIM0->IR = 1;			/* clear interrupt flag */
  LPC_GPIO2->FIOCLR |= (0x01 << 13);
  disable_timer(0);
 /*

  int cantidad;
  int i=0;
  euint8 datoECG;

  do{
  cantidad=file_read(&fileECG,sizeof(euint8),&datoECG);
  if(cantidad != 1){
	  MESSAGE("Cantidad distinto de 1. Es igual a %d \n",cantidad);
	  disable_timer(0); //Ya no tiene sentido
	  return;
  }
  else
	  dato[i]=datoECG;
  }while(dato[i++] !='\n');

  resul=atoi(&dato);
  datoECG2=resul;
  resul+=8000000;
  resul*=100;

  B[3]=resul & 0xFF;
  B[2]=(resul >> 8) & 0xFF;
  B[1]=(resul >> 16) & 0xFF;
  B[0]='a';

/*
  B[0]='a';

  B[3]++;
  B[1]++; //un poco mas de nasta

  if(B[3]==0){
	  B[2]++;
	  if(B[2]==0){
	 	  B[1]++;
	   }
  }
*/
  /*
  //LLENADO DEL BUFFER CON EL DATO
    for(i=0;i<4;i++){
  	  buf4[index]=B[i];
  	  index++;
    }

	if(index>2047) index=0;
	nuevodato='s';
	CantDatosGenerados++;
	return;
	*/

}


// -- Funciones -- //
// systick_delay - creates a delay of the appropriate number of Systicks (happens every 1 ms)
__inline static void delayms (uint32_t delayTicks) {
  uint32_t currentTicks;
  currentTicks = msTicks;	// read current tick counter
  while ((msTicks - currentTicks) < delayTicks); // Now loop until required number of ticks passes.
}


// --------------------- MAIN --------------------- //

int main(void)
 {
     /*-------------CONFIGURACIONES -------------*/
	//MESSAGE("%d,%d,%d,%d",sizeof(int),sizeof(long int), sizeof(long long int),sizeof(float));
	led2_init();
	SysTick_Config(SystemCoreClock / 1000);
	//init_timer( 0, SystemCoreClock/4/fm ); //fm en Hz (ver define)
	init_timer( 0, SystemCoreClock/4/10 );
	init_timer( 1, SystemCoreClock/4/fmDAC ); //fmDAC en Hz (ver define)
   // init_timer( 3, SystemCoreClock/4/1000);   //1ms para contar pulsador en motora
	Dac_Init();
	SSP1Init();																// Inicializo el bus SSP1

	ADS1292_HW_Inic();
	ADS1292_Desactivar_Int_DRDY();
	ADS_SPI_CS_INACTIVO();
	ADS1292_Inic();



	Antirreb_Inic ();
	Inicializar_Pin_Boton();
	Inicializar_Pin_Buzzer ();

	UART0_Init(BAUDRATE); //ver define arriba
	B[0]=B[1]=B[2]=B[3]=0;

	estado = EstadoInicio;

	 if(IniciarSD()!=0){
		 Enviar3('e','i','n');
		 MESSAGE("ERROR DE INICIALIZACION DE SD");
		 estado=666;
		 while(1);
	 }
	FaseActual=0;
	//CantFases=0;
	//ABRIR ARCHIVO AUDIO//
	/*
	if(AbrirWAV()!=0){
		 estado=666;
		 while(1);
	}
*/
	int error,cantr;
	//ABRIR ARCHIVO ECG GRABADO//
	//if((error=file_fopen(&fileECG,&efs.myFs,"UnCanal.txt",'r')))
	//		Error_file_fopen(error,"UnCanal.txt");	//ERROR!

	//ABRIR ARCHIVO PARA ESCRITURA//
	char NombreArchivo[12]="archivoX.txt";
	disable_timer(0);
	int i=0;



	EnviarBuf4();

	//-------------ARRANCA-----------------------//
	while(1){
		switch(estado){
			case -1:
				break;
			case EstadoInicio:
				BPM_cursor=0;
				MESSAGE("Fase Pre\n");
				//enable_timer(0);
				bloque=0;
				index=0;
				ADS1292_Comenzar_Adquisicion();
				while(estado==EstadoInicio)
					if(nuevodato=='s'){
						 EnviarBuf4();
						// nuevodato='n';
					}
				break;
			case EstadoEsperaPA1: //Fase roja pre para ver si esta conectado bien
				ADS1292_Finalizar_Adquisicion();
				//disable_timer(0);

				CantFases=4;
				MESSAGE("Espera confirmacion para fase 1\n");
				while(estado==EstadoEsperaPA1);
				break;
			case EstadoPA1: //Comienza fase motora
				FaseActual='1';
				NombreArchivo[7]=FaseActual;  //Primer archivo es archivo2
				InicioDeFase(NombreArchivo);
				MESSAGE("Fase 1: \n");
				Enviar3('f','a','1');
				AccionFaseMotora();
				ADS1292_Finalizar_Adquisicion();
				file_fclose(&filew);
				break;
			case EstadoPAEG1:
				MESSAGE("Esperanto.\n");
				while(estado==EstadoPAEG1){}
				break;
			case EstadoGPQPA1:
				cantDatosQRS[0]=cantQRS; //Globito todo
				cantDatosPulsador[0]=cantPulsador;
				GuardarPulsador(FaseActual);
				GuardarQRS(FaseActual);
				estado=EstadoEsperaPA2;
				//no break
			case EstadoEsperaPA2:
				Enviar3('f','f','g');
			 	MESSAGE("Espero confirmacion fase 2\n");
				while(estado==EstadoEsperaPA2){}
				break;
			case EstadoPA2: //Intero 1
				FaseActual='2';
				NombreArchivo[7]=FaseActual;
				InicioDeFase(NombreArchivo);
				MESSAGE("Fase 2: \n");
				//enable_timer(0);
				Enviar3('f','a','2');

				while(estado==EstadoPA2)
					AccionFaseIntero();
				MESSAGE("\nTERMINE FASE 2\n");
				//disable_timer(0);


				ADS1292_Finalizar_Adquisicion();
				file_fclose(&filew);
				break;
			case EstadoPAEG2:
				break;
			case EstadoGPQPA2:
				cantDatosQRS[1]=cantQRS;
				cantDatosPulsador[1]=cantPulsador;
				GuardarPulsador(FaseActual);
				GuardarQRS(FaseActual);
				estado=EstadoEsperaPA3;
				//no break
			case EstadoEsperaPA3:
				Enviar3('f','f','g');
				MESSAGE("Espero confirmacion fase 3\n");
				while(estado==EstadoEsperaPA3){}
				break;
			case EstadoPA3: //Retorno auditivo
				FaseActual='3';
				NombreArchivo[7]=FaseActual;  //Primer archivo es archivo2
				InicioDeFase(NombreArchivo);
				MESSAGE("Fase 3: \n");
				//enable_timer(0);
				Enviar3('f','a','3');


				while(estado==EstadoPA3)
					AccionFaseIntero();
				MESSAGE("\nTERMINE FASE 3\n");
				//disable_timer(0);

				ADS1292_Finalizar_Adquisicion();
				file_fclose(&filew);
				break;
			case EstadoPAEG3:
				break;
			case EstadoGPQPA3:
				cantDatosQRS[2]=cantQRS;
				cantDatosPulsador[2]=cantPulsador;
				GuardarPulsador(FaseActual);
				GuardarQRS(FaseActual);
				estado=EstadoEsperaPA4;
				//no break
			case EstadoEsperaPA4:
				Enviar3('f','f','g');
				MESSAGE("Espero confirmacion fase 4\n");
				while(estado==EstadoEsperaPA4){}
				break;
			case EstadoPA4: //Intero 2
				FaseActual='4';
				NombreArchivo[7]=FaseActual;
				InicioDeFase(NombreArchivo);
				MESSAGE("Fase 4: \n");
				//enable_timer(0);
				Enviar3('f','a','4');

				while(estado==EstadoPA4)
					AccionFaseIntero();
				MESSAGE("\nTERMINE FASE 4\n");
				//disable_timer(0);



				ADS1292_Finalizar_Adquisicion();
				file_fclose(&filew);
				break;
			case EstadoPAEG4:
				break;
			case EstadoGPQPA4:
				cantDatosQRS[3]=cantQRS;
				cantDatosPulsador[3]=cantPulsador;
				GuardarPulsador(FaseActual);
				GuardarQRS(FaseActual);
				estado=EstadoEnviarTodo;
				break;
			case EstadoEsperaPB5: //Fase roja pre para ver si esta conectado bien
				ADS1292_Finalizar_Adquisicion();
				//disable_timer(0);
				CantFases=2;
				MESSAGE("Espera confirmacion para fase 5\n");
				while(estado==EstadoEsperaPB5);
				break;
			case EstadoPB5: //Comienza fase motora
				FaseActual='5';
				NombreArchivo[7]=FaseActual;  //Primer archivo es archivo2
				InicioDeFase(NombreArchivo);
				MESSAGE("Fase 5: \n");
				Enviar3('f','b','5');
				AccionFaseMotora();
				ADS1292_Finalizar_Adquisicion();
				file_fclose(&filew);
				break;
			case EstadoPBEG5:
				MESSAGE("Esperanto.\n");
				while(estado==EstadoPBEG5){}
				break;
			case EstadoGPQPB5:
				cantDatosQRS[0]=cantQRS; //Globito todo
				cantDatosPulsador[0]=cantPulsador;
				GuardarPulsador(FaseActual);
				GuardarQRS(FaseActual);
				estado=EstadoEsperaPB6;
				//no break
			case EstadoEsperaPB6:
				Enviar3('f','f','g');
				MESSAGE("Espero confirmacion fase 2\n");
				while(estado==EstadoEsperaPB6){}
				break;
			case EstadoPB6: //Fase volumen variable
				FaseActual='6';
				NombreArchivo[7]=FaseActual;
				InicioDeFase(NombreArchivo);
				MESSAGE("Fase 6: \n");
				//enable_timer(0);
				Enviar3('f','b','6');
				while(estado==EstadoPB6)
					AccionFaseIntero();
				MESSAGE("\nTERMINE FASE 6\n");

				//disable_timer(0);
				ADS1292_Finalizar_Adquisicion();
				file_fclose(&filew);
				break;
			case EstadoPBEG6:
				break;
			case EstadoGPQPB6:
				cantDatosQRS[1]=cantQRS;
				cantDatosPulsador[1]=cantPulsador;
				GuardarPulsador(FaseActual);
				GuardarQRS(FaseActual);
				estado=EstadoEnviarTodo;
				break;
			case EstadoPAB1:
				//enable_timer(0);
				//MESSAGE("Elimino archivo %s \n",NombreArchivo);no elimino xq no esta!
				//rmfile(&efs.myFs,NombreArchivo);
				estado=EstadoEsperaPA1;
				MESSAGE("Borrado fase %c (1) nuevamente\n", FaseActual);
				break;
			case EstadoPAB2:
				ADS1292_Finalizar_Adquisicion();
				MESSAGE("Elimino archivo %s",NombreArchivo);
				rmfile(&efs.myFs,NombreArchivo);

				//-------------ABRIR ARCHIVO ECG GRABADO-------------//
				//if((error=file_fopen(&fileECG,&efs.myFs,"UnCanal.txt",'r')))
				//		Error_file_fopen(error,"UnCanal.txt");	//ERROR!
				estado=EstadoEsperaPA2;
				MESSAGE("Borrado fase %c (2) nuevamente\n", FaseActual);
				break;
			case EstadoPAB3:
				ADS1292_Finalizar_Adquisicion();
				MESSAGE("Elimino archivo %s",NombreArchivo);
				rmfile(&efs.myFs,NombreArchivo);
				estado=EstadoEsperaPA3;
				MESSAGE("Borrado fase %c (3) nuevamente\n", FaseActual);
				break;
			case EstadoPAB4:
				ADS1292_Finalizar_Adquisicion();
				MESSAGE("Elimino archivo %s",NombreArchivo);
				rmfile(&efs.myFs,NombreArchivo);
				estado=EstadoEsperaPA4;
				MESSAGE("Borrado fase %c (4) nuevamente\n", FaseActual);
				break;
			case EstadoPBB5:
				ADS1292_Finalizar_Adquisicion();
				MESSAGE("Elimino archivo %s",NombreArchivo);
				rmfile(&efs.myFs,NombreArchivo);
				estado=EstadoEsperaPB5;
				MESSAGE("Borrado fase 5 nuevamente\n"); //GLOBITO
				break;
			case EstadoPBB6:
				ADS1292_Finalizar_Adquisicion();
				MESSAGE("Elimino archivo %s",NombreArchivo);
				rmfile(&efs.myFs,NombreArchivo);
				estado=EstadoEsperaPB6;
				MESSAGE("Borrado fase 6 nuevamente\n");
				break;

			case EstadoCancel1:
							//file_fclose(&filew); Como no abre no borra
							estado= EstadoPAB1; //voy a borrar
							MESSAGE("Fase 1 cancelada\n");
							break;
			case EstadoCancel2:
							disable_timer(0);
							if(file_fclose(&filew))
								MESSAGE("Error cierre archivo");
							estado= EstadoPAB2; //voy a borrar
							MESSAGE("Fase 2 cancelada\n");
							break;
			case EstadoCancel3:
							disable_timer(0);
							file_fclose(&filew);
							estado= EstadoPAB3; //voy a borrar
							MESSAGE("Fase 3 cancelada\n");
							break;
			case EstadoCancel4:
							disable_timer(0);
							file_fclose(&filew);
							estado= EstadoPAB4; //voy a borrar
							MESSAGE("Fase 4 cancelada\n");
							break;
			case EstadoCancel5:
							disable_timer(0);
							file_fclose(&filew);
							estado= EstadoPBB5; //voy a borrar
							MESSAGE("Fase 5 cancelada\n");
							break;
			case EstadoCancel6:
							disable_timer(0);
							file_fclose(&filew);
							estado= EstadoPBB6; //voy a borrar
							MESSAGE("Fase 6 cancelada\n");
							break;
			case EstadoEnviarTodo:
				file_fclose(&filew);

				delayms(100);
				Enviar3('f','g','t');
				//MESSAGE("\n Index %d Bloque %d\n",index,bloque);
				//cantw=file_write(&filew,index-bloque*512,buf4+bloque*512);
				//MESSAGE("CantW cachito , %d \n",cantw);
				//MESSAGE("Cachito es: %d",index-bloque*512);

				//Envio archivos de cada fase

				if(CantFases==4){
					NombreArchivo[7]='1';
					ArchivoPulsador[5]='1';
					ArchivoQRS[3]='1';
				}
				if(CantFases==2){
					NombreArchivo[7]='5';
					ArchivoPulsador[5]='5';
					ArchivoQRS[3]='5';
				}
				if(CantFases==0)
					MESSAGE("TE odioooooooooooo\n");


				int n=0;
				for(n=0;n<CantFases;n++){
					CantEnviados=0;
					if((error=file_fopen(&filer,&efs.myFs,NombreArchivo,'r'))){	//abrir archivo
							Error_file_fopen(error,NombreArchivo);
					}
					do{
						cantr = file_read(&filer,BUFFER_R_SIZE,buf);
						for(i=0;i<cantr;i++){
							if((i)%4==0){
								while( (LPC_UART0->LSR & LSR_THRE) == 0 );	// Block until tx empty
								LPC_UART0->THR = 'd';
								LPC_UART0->THR = buf[i+1];
								LPC_UART0->THR = buf[i+2];
								LPC_UART0->THR = buf[i+3];
								LPC_UART0->THR = 'd'^buf[i+1]^buf[i+2]^buf[i+3];
								CantEnviados++;
							}
						}
					}while(cantr!=0);
					file_fclose(&filer);


					if((error=file_fopen(&filer,&efs.myFs,ArchivoPulsador,'r'))){	//abrir archivo
												Error_file_fopen(error,ArchivoPulsador);
										}
					cantr = file_read(&filer,sizeof(int)*cantDatosPulsador[n],vectorPulsador);

					for(i=0;i<cantDatosPulsador[n];i++){
						Enviar3('p',(vectorPulsador[i] >> 8) & 0xFF,(vectorPulsador[i]& 0xFF)); //pXX ->pc
					}
					file_fclose(&filer);

					delayms(50);


					if((error=file_fopen(&filer,&efs.myFs,ArchivoQRS,'r'))){	//abrir archivo
												Error_file_fopen(error,ArchivoQRS);
										}
					cantr = file_read(&filer,sizeof(int)*cantDatosQRS[n],vectorQRS);

					for(i=0;i<cantDatosQRS[n];i++){
						Enviar3('q',(vectorQRS[i] >> 8) & 0xFF,(vectorQRS[i]& 0xFF)); //qXX ->pc alta baja
					}

					Enviar3('f','f','a'); //indicador de fin de archivo
					NombreArchivo[7]++;
					ArchivoPulsador[5]++;
					ArchivoQRS[3]++;
				}

				//GLOBITO VER TEMA CACHITO
				//fs_umount(&efs.myFs);

				delayms(100);
				//aviso a la PC que termine de enviar todos los datos del archivo
				Enviar3('f','f','n');
				MESSAGE("Cachito es: %d\n",index-bloque*SIZE_BLOQUE_GUARDADO);
				MESSAGE("CantdDatosGenerados: %d\n",CantDatosGenerados);
				MESSAGE("Cant Pulsaciones: %d\n",cantPulsador);
				MESSAGE("Cant QRS: %d\n",cantQRS);

				estado=EstadoSalirTodo;
				break;
			case EstadoSalirTodo:
				file_fclose(&filew);
				/*IMPRESION DE ARCHIVOS/CARPETAS DE LA SD*/
				MESSAGE("Directory of 'root':\n");
				ls_openDir( &list, &(efs.myFs) , "/");

				//int nro_archivo=0;

				rmfile(&efs.myFs,"Pulsa1.txt");
				rmfile(&efs.myFs,"Pulsa2.txt");
				rmfile(&efs.myFs,"Pulsa3.txt");
				rmfile(&efs.myFs,"Pulsa4.txt");
				rmfile(&efs.myFs,"QRS1.txt");
				rmfile(&efs.myFs,"QRS2.txt");
				rmfile(&efs.myFs,"QRS3.txt");
				rmfile(&efs.myFs,"QRS4.txt");
				rmfile(&efs.myFs,"archivo1.txt");
				rmfile(&efs.myFs,"archivo2.txt");
				rmfile(&efs.myFs,"archivo3.txt");
				rmfile(&efs.myFs,"archivo4.txt");

				rmfile(&efs.myFs,"Pulsa5.txt");
				rmfile(&efs.myFs,"Pulsa6.txt");
				rmfile(&efs.myFs,"QRS5.txt");
				rmfile(&efs.myFs,"QRS6.txt");
				rmfile(&efs.myFs,"archivo5.txt");
				rmfile(&efs.myFs,"archivo6.txt");

				/*IMPRESION DE ARCHIVOS/CARPETAS DE LA SD*/
				while ( ls_getNext( &list ) == 0 ) {
					list.currentEntry.FileName[LIST_MAXLENFILENAME-1] = '\0';
					MESSAGE( "%s (%li bytes)\n" ,
					list.currentEntry.FileName,
					list.currentEntry.FileSize ) ;
				}
				MESSAGE("Directory of 'root':\n");
				ls_openDir( &list, &(efs.myFs) , "/");
				/*
				while ( ls_getNext( &list ) == 0 ) {
					list.currentEntry.FileName[LIST_MAXLENFILENAME-1] = '\0';
					MESSAGE( "%s (%li bytes)\n" ,
						list.currentEntry.FileName,
						list.currentEntry.FileSize ) ;
				}
				*/
				fs_umount(&efs.myFs);
				estado=1000;
				MESSAGE("Salir todo\n Fin del programa. . .\n");
				break;
			case 1000://sali bien
				break;
			default:
				Enviar3('e','e','i');
				MESSAGE("Estoy en cualquiera estado= %d\n",estado);
				MESSAGE("cantDatGen: %d, index: %d, bloque %d \n",CantDatosGenerados,index,bloque);
				MESSAGE("Cant QRS: %d\n",cantQRS);
				estado=EstadoSalirTodo;

				break;
			}
	}
	//No debe llegar aca
	fs_umount(&efs.myFs);
	return 0;
}
 //---------------FIN MAIN------------------//


int IniciarSD(void){
    /*-------------INICIALIZACION SD-------------*/
	int8_t res;
	MESSAGE("CARD init...\n");
	if ( ( res = efs_init( &efs, 0 ) ) != 0 ) {
		MESSAGE("Failed with %i\n",res);
		 if(res==-1)MESSAGE(" the interface could not be initialized.\n");
		 if(res==-2)MESSAGE("the filesystem could not be initialized.\n");
	return res;

	}
	else {
		MESSAGE("Ok\n");
		/*IMPRESION DE ARCHIVOS/CARPETAS DE LA SD*/
		MESSAGE("Directory of 'root':\n");
		ls_openDir( &list, &(efs.myFs) , "/");
		while ( ls_getNext( &list ) == 0 ) {
			list.currentEntry.FileName[LIST_MAXLENFILENAME-1] = '\0';
			MESSAGE( "%s (%li bytes)\n" ,
				list.currentEntry.FileName,
				list.currentEntry.FileSize ) ;
		}
	}
	return 0;
}

__inline int AbrirWAV(){

	int cantread, error_f;

	if((error_f=file_fopen(&fileWAV,&efs.myFs,"EM5.wav",'r'))){	//abrir archivo para leer
				MESSAGE("No abrio el archivo de sonido.\n");
				MESSAGE("Codigo de error: %d\n",error_f);
				return error_f;
	}
	cantread = file_read(&fileWAV, sizeof(wav_file)-sizeof(void*), &wav_file );
	WavSize = wav_file.Subchunk2Size;
    MESSAGE("\nTamaño WAV: %d\n",WavSize);

    int i;
    for(i=0;i<2401;i++)
    	cantread=file_read(&fileWAV,sizeof(char),&Sonido[i]);

    file_fclose(&fileWAV);   //GLOBITO APOCALIPSIS PROBABLE
    return 0; //Todo ok
}
void Error_file_fopen(int err, eint8* fileName)
{
	MESSAGE("\n ERROR! Opening the file  %s.\n",fileName);

	switch(err){
		case -1: MESSAGE("The file you are trying to open for reading could not be found.\n");
			Enviar3('e','a','r');
		break;
		case -2: MESSAGE("The file you are trying to open for writing already exists.\n");
			rmfile(&efs.myFs,fileName);
			MESSAGE("File deleted OK.\n");
			if((err=file_fopen(&filew,&efs.myFs,fileName,'w'))){	//abrir archivo y posicionarse al final
					Error_file_fopen(err,fileName); //si ya existe lo borra!
			}
			else
				MESSAGE("%s file opened OK\n",fileName);
		break;
		case -3: MESSAGE("No free spot could be found for writing or appending.\n");
		Enviar3('e','a','r');
		break;
		case -4: MESSAGE("Mode is not correct (if it is not 'r', 'w' or 'a').\n");
			Enviar3('e','a','r');
		break;

	}

}

void UART0_IRQHandler(void){
	int i=0;
    MESSAGE("\nComando Recibido: ");
	while(LPC_UART0->LSR & (1<<0)){
		bufr[i++]=LPC_UART0->RBR & (0xFF);
		MESSAGE("%c",bufr[i-1]);
		if(i==4)
			break;
	}
	MESSAGE("\n");


	//if(bufr[0]!='b')
		//tirar error
	//	return;


	if((bufr[0])^(bufr[1])^(bufr[2])==bufr[3]) //Mensaje recibido ok, de vuelta a la pc
		Enviar3('c',bufr[1],bufr[2]);
	else
		MESSAGE("Error checksum");


	if(Es('i','n')){
		estado=EstadoInicio;
		return;
	}

	if(Es('e','m')){
		//enable_timer( 0 );
		return;
	}
	if(Es('f','n')){
		disable_timer(0);
		return;
	}
	if(Es('o','n')){
		led2_on();
		return;
	}
	if(Es('o','f')){
		led2_off();
		return;
	}
	if(Es('p','a')){
		estado=EstadoEsperaPA1;
		return;
	}
	if(Es('p','b')){
		estado=EstadoEsperaPB5;
		return;
	}
	if(Es('a','1')){
		disable_timer(0);
		estado=EstadoPA1;
		CantDatosGenerados=0;
		return;
	}
	if(Es('a','2')){
		estado=EstadoPA2;
		CantDatosGenerados=0;
		return;
	}
	if(Es('a','3')){
		estado=EstadoPA3;
		CantDatosGenerados=0;
		return;
	}
	if(Es('a','4')){
		estado=EstadoPA4;
		CantDatosGenerados=0;
		return;
	}
	if(Es('b','5')){
		estado=EstadoPB5;
		CantDatosGenerados=0;
		return;
	}
	if(Es('b','6')){
		estado=EstadoPB6;
		CantDatosGenerados=0;
		return;
	}
	if(Es('g','1')){
			estado=EstadoGPQPA1;
			//estado=EstadoEsperaPA2;
			return;
		}
	if(Es('g','2')){
		estado=EstadoGPQPA2;
		//estado=EstadoEsperaPA3;
		return;
		}
	if(Es('g','3')){
		estado=EstadoGPQPA3;
		//	estado=EstadoEsperaPA4;
			return;
		}
	if(Es('g','4')){
		estado=EstadoGPQPA4;
		//	estado=EstadoEnviarTodo;
			return;
		}
	if(Es('g','5')){
			estado=EstadoGPQPB5;
			return;
		}

	if(Es('g','6')){
		estado=EstadoGPQPB6;
		return;
	}

	if(Es('g','t')){ //GLOBITO OBSOLETO
		estado=EstadoEnviarTodo;
		return;
	}
	if(Es('f','1')){
		estado=EstadoPAEG1;
		return;
	}
	if(Es('f','2')){
		disable_timer(0);
		estado=EstadoPAEG2;
		return;
	}
	if(Es('f','3')){
		disable_timer(0);
		estado=EstadoPAEG3;
		return;
	}
	if(Es('f','4')){
		disable_timer(0);
		estado=EstadoPAEG4;
		return;
	}
	if(Es('f','5')){
		disable_timer(0);
		estado=EstadoPBEG5;
		return;
	}
	if(Es('f','6')){
		disable_timer(0);
		estado=EstadoPBEG6;
		return;
	}
	if(Es('e','1')){
			estado=EstadoPAB1;
			return;
		}
	if(Es('c','1')){
			disable_timer(0);
			estado=EstadoCancel1;
			return;
		}
	if(Es('e','2')){
			estado=EstadoPAB2;
			return;
		}
	if(Es('c','2')){
			disable_timer(0);
			estado=EstadoCancel2;
			return;
		}
	if(Es('e','3')){
			estado=EstadoPAB3;
			return;
		}
	if(Es('c','3')){
			disable_timer(0);
			estado=EstadoCancel3;
			return;
		}
	if(Es('e','4')){
			estado=EstadoPAB4;
			return;
		}
	if(Es('c','4')){
			disable_timer(0);
			estado=EstadoCancel4;
			return;
		}
	if(Es('e','5')){
			estado=EstadoPBB5;
			return;
		}
	if(Es('c','5')){
			disable_timer(0);
			estado=EstadoCancel5;
			return;
		}
	if(Es('e','6')){
			estado=EstadoPBB6;
			return;
		}
	if(Es('c','6')){
			disable_timer(0);
			estado=EstadoCancel6;
			return;
		}
	if(Es('e','x')){
			//Cerrar todx los archivos si no se cerraron y eliminarlos
			estado=EstadoSalirTodo;
			return;
		}
	if(Es('c','t')){ //No cerro aplicacion pero quiere arrancar de nuevo
		//Cerrar todx los archivos si no se cerraron y eliminarlos
			estado=EstadoSalirTodo;
			return;
		}
	if(Es('v','+')){
		//Incrementar volumen
			cambiarVol=1;
			return;
		}
	if(Es('v','-')){
		//Decrementar volumen
			cambiarVol=-1;
			return;
		}
	// Enviar error por mensaje no valido
	Enviar3('e','m','i');
	estado=-1; //al inicio de los tiempos
	return;


}

void GuardarPulsador(char fase){
	int cant_wr,error_wr;
	ArchivoPulsador[5]=fase;
	if((error_wr=file_fopen(&fileM,&efs.myFs,ArchivoPulsador,'w'))){	//abrir archivo y posicionarse al final
				Error_file_fopen(error_wr,ArchivoPulsador); //si ya existe lo borra!
					}
	cant_wr=file_write(&fileM,sizeof(int)*cantPulsador,vectorPulsador);
	MESSAGE("Pulsaciones= %d, Se grabaron %d pulsaciones.\n",cantPulsador,(cant_wr/4));
	file_fclose(&fileM);
}

void GuardarQRS(char fase){
	int cant_wr,error_wr;
	ArchivoQRS[3]=fase;
	if((error_wr=file_fopen(&fileM,&efs.myFs,ArchivoQRS,'w'))){	//abrir archivo y posicionarse al final
				Error_file_fopen(error_wr,ArchivoQRS); //si ya existe lo borra!
					}

	cant_wr=file_write(&fileM,sizeof(int)*cantQRS,vectorQRS);
	MESSAGE("Picos= %d, Se grabaron %d Picos.\n",cantQRS,cant_wr/4);
	file_fclose(&fileM);
}
void Inicializar_Pin_Buzzer(void)
{

	LPC_PINCON->PINSEL4 &= ~(0x03UL << 26);							// Entrada de DRDY
	LPC_GPIO2->FIODIR |= (0x01 << 13);								// 13 seteado como salida
	LPC_GPIO2->FIOCLR |= (0x01 << 13);
	return;
}



void TIMER1_IRQHandler (void){
  LPC_TIM1->IR = 1;			/* clear interrupt flag */
 // led2_invert();
 /* int cantrWAV;
  if(WavSize)
		while (WavSize--){
			cantrWAV =file_read(&fileWAV,sizeof(WavData),&WavData);

			DACR = ((0x3FC) & (WavData)*1) << 6;
			//if(!WavSize) file_fclose(&fileWAV); lo paso afuera
		}*/
  if(WavSize>1){
			DACR = ((0x3FC) & (Sonido[2401-WavSize])*1) << 6;
			WavSize--;
  }// else desabilito timer y wavsize vuelve a valer maximo
  else{
	  disable_timer(1);
	  WavSize=2401;
  }

  return;
}
void TIMER2_IRQHandler (void){
  LPC_TIM2->IR = 1;			/* clear interrupt flag */
  CantDatosGenerados++; //paso 1ms
  return;
}
void TIMER3_IRQHandler (void){
  LPC_TIM3->IR = 1;			/* clear interrupt flag */
  return;
}

 void InicioDeFase(char* nombreArch){
	int Error;
	cantPulsador=0;
	cantQRS=0;
	CantDatosGenerados=0;
	AnteriorQRS=0;

	if((Error=file_fopen(&filew,&efs.myFs,nombreArch,'w'))){	//abrir archivo y posicionarse al final
		Error_file_fopen(Error,nombreArch); //si ya existe lo borra!
	}

	bloque=0;
	index=0;
	max=-2147483648;
	nactual= umbral=nignorar=promedio= suma=inicializo=0;
	ADS1292_Comenzar_Adquisicion();
	//QRS_stat= QRSDetector(0,1); //Reinicio detector


}

