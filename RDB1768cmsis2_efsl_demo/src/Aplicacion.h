/*
 * Aplicacion.h
 *
 *  Created on: 14/06/2013
 *      Author: Marcelo
 */

#ifndef APLICACION_H_
#define APLICACION_H_

#include "RegsLPC1769.h"
#include "infotronic.h"

#include <stdint.h>
#include <stdbool.h>
//#include "monitor.h"
//#include "diskio.h"
//#include "ff.h"
#include "dac.h"
#include <stdlib.h>


#define REAL_TIME				0
#define SD_CARD					1

#define 	FILENAME	"TPO.txt"

#define 	MENSAJE_INICIO			0
#define 	MENSAJE_FRECUENCIA		1
#define 	MENSAJE_EDAD			2
#define 	MENSAJE_PESO			3
#define 	MENSAJE_TELEFONO		4
#define 	MENSAJE_CALCULANDO		5
#define 	MENSAJE_FIN				6
#define 	MENSAJE_ERROR			7
#define 	MENSAJE_MODO			8

#define DIGITO_CERO		0
#define DIGITO_UNO		1
#define DIGITO_DOS		2
#define DIGITO_TRES		3
#define DIGITO_CUATRO	4
#define DIGITO_CINCO	5
#define DIGITO_SEIS		6
#define DIGITO_SIETE	7

#define 	MAX_TIMERS  10
 #define NO_KEY       0xFF
 #define REBOTES   5
#define 	UNA_DECIMA 	40	//!< con ticks de 2,5 mS
#define 	UN_SEGUNDO 	10
typedef  unsigned char 	TIEMPOS ;
typedef  unsigned char  EVENTOS ;

#define 	ENTER			0
#define		BACK			1
#define 	UP				2
#define 	DOWN			3
void Maquina (void);
void Display_lcd_char(const char msg , char renglon , char posicion );
void Borrar_lcd (void);
unsigned char ME_Uno (void);
void ME_Frecuencia(void);
void ME_3digitos (uint16_t* dato);
unsigned char ME_Tel (void);
void Presionado(unsigned char);
void Init_Systick (void);
unsigned char DriverTecladoHW( void );
void DriverTecladoSW ( unsigned char);
void DriverTeclado(void);
unsigned char Teclado (void);

void TimerStart ( uint8_t ev , uint8_t t );
void TimerStop ( uint8_t ev );
void TimerClose ( void );
void TimerEvent (void);
void AnalizarTimers( TIEMPOS * , EVENTOS *  , uint8_t  );
void BasesDeTiempo( void );

//FRESULT Montar_SD (void);
//FRESULT Write_SD (int32_t dato);
#endif /* APLICACION_H_ */
