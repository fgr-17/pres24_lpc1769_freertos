#ifndef EFSL_DEMO_H_
#define EFSL_DEMO_H_


// -- Includes  -- //
#include "LPC17xx.h"
#include "string.h"
#include "uart0.h"
#include "leds.h"
#include "timer.h"
#include <cr_section_macros.h>
#include <NXP/crp.h>
#include <stdio.h>
#include <stdlib.h>
#include "efs.h"
#include "ls.h"
#include "mkfs.h"
#include "wav.h"
#include "dac.h"
//#include <ADS1292/ADS1292.h>
#include <ADS1292.h>
#include "estados.h"	//Asignacion de nros a cada estado
#include "fases.h"
#include "pulsador.h"

// -- Defines -- //
#define LSR_THRE	0x20
#define MESSAGE printf
#define BAUDRATE 115200
#define LONG_ENVIO 4
#define BUFFER_R_SIZE  512   //buffer size (in byte) for R/W operations

#define fm  500 //En Hz
#define fmDAC 11025 //En Hz



// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
__CRP const unsigned int CRP_WORD = CRP_NO_CRP;

EmbeddedFileSystem efs;
EmbeddedFile filer, filew, fileECG;
EmbeddedFile fileM;
DirList list;

// -- Variables -- //
char B[LONG_ENVIO];
unsigned short bloque;
char nuevodato;
int index=0;
unsigned char buf[BUFFER_R_SIZE];
unsigned char buf4[L_BUF] = {"hola"};
unsigned char bufr[8];
unsigned char MensEnv[4];

volatile uint32_t msTicks; // counter for 1ms SysTicks
unsigned short e;
int estado;

EmbeddedFile fileWAV;
wav wav_file;
uint16_t WavSize;
uint8_t WavData;
//int8_t wav_data[10000];
char Sonido[2401];

char dato[9];
int resul;
char FaseActual;
int CantFases;
int CantDatosGenerados=0;
int IGraf=1; //Cada cuantos generados se mandan a graficar
int CantEnviados=0;

int vectorQRS[1600];
int vectorPulsador[10];//GLOBITO VOLVER A + CANT
int cantQRS=0; //Este es transitorio, de cada fase, para guardar
int cantPulsador=0;

int cantDatosQRS[4]; //este es para leer
int cantDatosPulsador[4];

int AnteriorQRS=0;




unsigned int datoECG2;  //CAmbie, era sin unsigned, problemas con detector para valores negativos
char datoECG2char[9];
int posicTag=0;


//Detector Nuestro
int nactual;
int umbral;
int nignorar;
int max;
int promedio;
long long suma;
int inicializo;

int cambiarVol;

char ArchivoPulsador[10]="PulsaX.txt";
char ArchivoQRS[8]="QRSX.txt";


int IniciarSD(void);
void Error_file_fopen(int,eint8*);
void GuardarPulsador(char fase);
void GuardarQRS(char fase);
void TIMER1_IRQHandler (void);
void TIMER2_IRQHandler(void);
void TIMER3_IRQHandler(void);

void UART0_IRQHandler(void);

void InicioDeFase(char* nombreArch);



__inline int AbrirWAV();


#endif /* EFSL_DEMO_H_ */
