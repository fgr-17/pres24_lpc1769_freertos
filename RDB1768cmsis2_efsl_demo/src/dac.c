#include "dac.h"

void Dac_Init (void){

	SetPINSEL(AOUT, FUNCION_2);
	SetDIR ( AOUT , SALIDA);
	PCLKSEL0 |= 3 << 22; // CLCK/8
	DACCTRL |= 0; // Double buffering disabled, sin DMA, sin Time Out Counter
}
