﻿/**
 \file ads1292.c
 \brief Driver y Handlers del integrado de adquisición de señales ADS1292
 \author Lautaro Petrauskas
 \date 2013.11.15
**/


/*******************************************************************************
 * 		INCLUSIÓN DE ARCHIVOS :
 *******************************************************************************/

#include <LPC17xx.h>
#include <stdint.h>

#include <ADS1292/ADS1292.h>
#include <SYSTICK/systick.h>
#include <SSP/ssp.h>
#include "pulsador.h"



/*******************************************************************************
 * 		DECLARACIÓN DE CONSTANTES :
 *******************************************************************************/

#define BYTES_POR_PALABRA			3						// 24 bits de dato
#define PALABRAS_POR_ADQ			3						// Status + CH1 + Ch2
#define BYTES_POR_ADQ				(PALABRAS_POR_ADQ * BYTES_POR_PALABRA)

#define FREQ_TEST_SGN				1						// Frecuencia de la señal de prueba
#define	FS							500						// 125 SPS


/*******************************************************************************
 * 		PROTOTIPOS DE FUNCIONES :
 *******************************************************************************/

uint32_t ADS1292_SSP1_Recibir_Palabra_24bits (t_u32* buf_ch1, t_u32*buf_ch2, uint32_t ind);
void ADS1292_Inic(void);
void ADS1292_HW_Inic (void);
uint8_t SSP1_Recibir_Byte (void);

void ADS1292_Activar_Int_DRDY ();
void ADS1292_Desactivar_Int_DRDY ();

void ADS1292_Comenzar_Adquisicion (void);
void ADS1292_Finalizar_Adquisicion (void);

/*******************************************************************************
 * 		VARIABLES GLOBALES :
 *******************************************************************************/

t_u32 ch1_buf [L_BUF];															// Muestras del canal 1
t_u32 ch2_buf [L_BUF];															// Muestras del canal 2
uint32_t ind = 0;																// indice de muestras para el canal 1
uint32_t cuenta_buffer = 0;
/*******************************************************************************
 * 		IMPLEMENTACIÓN DE FUNCIONES :
 *******************************************************************************/

/************************************************************************
 *
 *	\function	void ADS1292_Inic(void)
 *
 * 	\brief 		Inicializacion del ADS1292. Ver SBAS502B pág. 63
 *
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *
 ************************************************************************/

void ADS1292_Inic(void)
{
	static uint8_t byte_ID;

	systick_delay(32);											// Espero 2^12 pulsos tMOD
	ADS_SPI_CS_INACTIVO();
	systick_delay(1000);										// Espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On
	ADS_SPI_CS_ACTIVO();
	systick_delay(500);											// Espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On

	SSP1_Enviar_Byte(ADS_SDATAC);								// El ADS se inicia en modo RDATAC, se pasa a modo SDATAC para escribir regs.
	systick_delay(1);											// Se espera 4 tCLK

	SSP1_Enviar_Byte(ADS_RESET);								// Reseteo el ADS
	systick_delay(1000);										// Espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On

/*
	SSP1_Enviar_Byte (ADS_RREG | ADS_ID);
	systick_delay(1);
	SSP1_Enviar_Byte(0x00);										// Desde esa dirección, solo escribo 1 registro
	systick_delay(1);
	byte_ID = SSP1_Recibir_Byte();

	if(byte_ID != (ADS_ID_REV_ID_A_ADS1x9x | ADS_ID_REV_ID_B_ADS1292x))
		// todo señalar error a la terminal?
		return; // -1											// Salgo porque no recibi el ID que corresponde
*/
	SSP1_Enviar_Byte(ADS_WREG | ADS_CONFIG2);							// Voy a escribir el registro CONFIG2
	SSP1_Enviar_Byte(0x00);												// Desde esa dirección, solo escribo 1 registro
	SSP1_Enviar_Byte(ADS_CONFIG2_PDB_REFBUF);							// PDB_REFBUF = 1
	systick_delay(10);													// Espero a que se inicialice la referencia interna

	SSP1_Enviar_Byte(ADS_WREG | ADS_CONFIG1);					// Voy a escribir el registro CONFIG1
	SSP1_Enviar_Byte(0x00);										// Desde esa dirección, solo escribo 1 registro
	SSP1_Enviar_Byte(ADS_CONFIG_1_OS_RATIO_500SPS);				// 125SPS + continuous mode
	systick_delay(1);

	SSP1_Enviar_Byte(ADS_WREG | ADS_CONFIG2);							// Voy a escribir el registro CONFIG2
	SSP1_Enviar_Byte(0x00);												// Desde esa dirección, solo escribo 1 registro
	SSP1_Enviar_Byte(ADS_CONFIG2_PDB_REFBUF);							// PDB_REFBUF = 1
	systick_delay(10);


	SSP1_Enviar_Byte(ADS_WREG | ADS_CH1SET);									// Voy a escribir el registro CONFIG2
	SSP1_Enviar_Byte(0x00);														// Desde esa dirección, solo escribo 1 registro
	SSP1_Enviar_Byte(ADS_CH1SET_GAIN_1 | ADS_CH1SET_MUX_NORMAL);			// Bit 7 en 1 obligatorio, bit 3 conecto CLK al pin de salida
	systick_delay(1);

	SSP1_Enviar_Byte(ADS_WREG | ADS_CH2SET);									// Voy a escribir el registro CONFIG2
	SSP1_Enviar_Byte(0x00);														// Desde esa dirección, solo escribo 1 registro
//	SSP1_Enviar_Byte(ADS_CH2SET_GAIN_1 | ADS_CH1SET_MUX_TEST_SIGNAL);			// Bit 7 en 1 obligatorio, bit 3 conecto CLK al pin de salida
	SSP1_Enviar_Byte(ADS_CH2SET_GAIN_1 | ADS_CH2SET_MUX_NORMAL);
	systick_delay(1);

	ADS_SPI_CS_INACTIVO();

/*

	SSP1_Enviar_Byte(ADS_START);								// Reseteo el ADS
	systick_delay(1000);										// Espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On

	SSP1_Enviar_Byte(ADS_RDATAC);								// El ADS se inicia en modo RDATAC, se pasa a modo SDATAC para escribir regs.
	systick_delay(1);											// Se espera 4 tCLK

	ADS_SPI_CS_INACTIVO();
*/
	return;
}


void ADS1292_Comenzar_Adquisicion (void)
{

	ADS_SPI_CS_ACTIVO();

	SSP1_Enviar_Byte(ADS_SDATAC);								// El ADS se inicia en modo RDATAC, se pasa a modo SDATAC para escribir regs.
	systick_delay(1);											// Se espera 4 tCLK

	SSP1_Enviar_Byte(ADS_START);								// Reseteo el ADS
	systick_delay(1000);										// Espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On

	SSP1_Enviar_Byte(ADS_RDATAC);								// El ADS se inicia en modo RDATAC, se pasa a modo SDATAC para escribir regs.
	systick_delay(1);											// Se espera 4 tCLK

	ADS1292_Activar_Int_DRDY();

	ADS_SPI_CS_INACTIVO();

	return;
}

void ADS1292_Finalizar_Adquisicion (void)
{

	ADS_SPI_CS_ACTIVO();

	SSP1_Enviar_Byte(ADS_SDATAC);								// El ADS se inicia en modo RDATAC, se pasa a modo SDATAC para escribir regs.
	systick_delay(1);											// Se espera 4 tCLK

	SSP1_Enviar_Byte(ADS_STOP);								// Reseteo el ADS
	systick_delay(100);										// Espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On

	ADS1292_Desactivar_Int_DRDY();

	ADS_SPI_CS_INACTIVO();

	return;
}

/************************************************************************
 *
 *	\function	void ADS1292_HW_Inic (void)
 *
 * 	\brief 		Inicializacion de pines del ADS1292
 *
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *
 ************************************************************************/

void ADS1292_HW_Inic (void)
{

	LPC_PINCON->PINSEL0 &= ~(0x03UL << 0);						// Entrada de DRDY
	LPC_GPIO0->FIODIR0 |= (0x00 << 0);

	// LPC_GPIOINT->IO0IntEnF |= GPIOINT_IONINTENF_P00EF;
	// NVIC_EnableIRQ(EINT3_IRQn);

	LPC_PINCON->PINSEL0 &= ~(0x03UL << 12);						// Salida CS
	LPC_GPIO0->FIODIR0 |= (0x01 << 6);


	return;
}


void ADS1292_Desactivar_Int_DRDY ()
{
	NVIC_DisableIRQ(EINT3_IRQn);
	LPC_GPIOINT->IO0IntEnF &= ~GPIOINT_IONINTENF_P00EF;
	NVIC_EnableIRQ(EINT3_IRQn);
	return;
}


void ADS1292_Activar_Int_DRDY ()
{
	NVIC_DisableIRQ(EINT3_IRQn);
	LPC_GPIOINT->IO0IntEnF |= GPIOINT_IONINTENF_P00EF;
	NVIC_EnableIRQ(EINT3_IRQn);
	return;
}
/************************************************************************
 *
 *	\function	uint8_t SSP1_Recibir_Byte (void)
 *
 * 	\brief 		Recibo un byte por SSP
 *
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *
 ************************************************************************/

uint8_t SSP1_Recibir_Byte (void)
{
	uint8_t byte_leido;
	ADS_SPI_CS_ACTIVO();
	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	byte_leido = LPC_SSP1->DR;
	ADS_SPI_CS_INACTIVO();

	return byte_leido;
}

/************************************************************************
 *
 *	\function	void SSP1_Recibir_Palabra_24bits (uint8_t*buf)
 *
 * 	\brief 		Recibo una palabra de 3 bytes
 *
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *
 ************************************************************************/

__inline uint32_t ADS1292_SSP1_Recibir_Palabra_24bits (t_u32* buf_ch1, t_u32*buf_ch2, uint32_t ind)
{
	static t_u32 status;

	ADS_SPI_CS_ACTIVO();

	/*
	 * Guardo los 24 bits de status (recibo 3 bytes por separado)
	 */

	status.bytes.b0 = 0;
	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	status.bytes.b3 = LPC_SSP1->DR;

	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	status.bytes.b2 = LPC_SSP1->DR;

	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	status.bytes.b1 = LPC_SSP1->DR;

/*	if(status.qword != 0xC0000000)
	{
		status.qword = 0; //GLOBITO
		// ERROR

		return 0;
	}
*/
	/*
	 * Guardo los 24 bits de CH1 (recibo 3 bytes por separado)
	 */
	buf_ch1[ind].bytes.b3 = 0;
	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	buf_ch1[ind].bytes.b2 = LPC_SSP1->DR;
	// channel1 = (LPC_SSP1->DR <<16);

	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	buf_ch1[ind].bytes.b1 = LPC_SSP1->DR;
	// channel1 |= (LPC_SSP1->DR<<8) ;

	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	buf_ch1[ind].bytes.b0 = LPC_SSP1->DR;


	/*
	 * Guardo los 24 bits de CH2 (recibo 3 bytes por separado)
	 */
	buf_ch2[ind].bytes.b3 = 0;
	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	buf_ch2[ind].bytes.b2 = LPC_SSP1->DR;

	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	buf_ch2[ind].bytes.b1 = LPC_SSP1->DR;

	LPC_SSP1->DR = DR_LECTURA;
	while ( (LPC_SSP1->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	buf_ch2[ind].bytes.b0 = LPC_SSP1->DR;


	ADS_SPI_CS_INACTIVO();
	return ++ind;
}

/************************************************************************
 *
 *	\function	void EINT3_IRQHandler (void)
 *
 * 	\brief 		Inicializacion de pines del ADS1292
 *
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *
 ************************************************************************/

void EINT3_IRQHandler (void)
{	int i=0;
	uint8_t reg_status;
	reg_status = LPC_GPIOINT->IntStatus;									// Leo este reg. para saber si la int. es de P0 o P2
	NVIC_DisableIRQ(EINT3_IRQn);
							// Leo este reg. para saber si la int. es de P0 o P2
	if(reg_status & GPIOINT_INTSTAT_P0INT)
	{
		// Interrupción en pines de P0
		reg_status = LPC_GPIOINT->IO0IntStatF;
		if(reg_status & GPIOINT_INTSTAT_P00FEI)
		{
			LPC_GPIOINT->IO0IntClr |= GPIOINT_INTCLR_P00CI;					// Limpio la interrupcion pendiente
			ind = ADS1292_SSP1_Recibir_Palabra_24bits (ch1_buf, ch2_buf, ind);

			B[3] = ch1_buf[ind-1].bytes.b0; // baja
			//B[2] = ch1_buf[ind-1].bytes.b1; // media
			//B[1] = ch1_buf[ind-1].bytes.b2; // alta

			B[2] = ch1_buf[ind-1].bytes.b1; // media
			B[1] = ch1_buf[ind-1].bytes.b2; // alta

			B[0] = 0;

			if(ind >= L_BUF)
				ind = 0;


			/*if(ch1_buf[ind-1].bytes.b3==0){
				buf4[index++] = B[0];
				buf4[index++] = B[1];
				buf4[index++] = B[2];
				buf4[index++] = B[3];


							buf4[index++] = B[0];

							 borrar[0]++;
							 //borrar[1]++; //un poco mas de nasta
							if(borrar[0]==0){
								borrar[1]++;
								  if(borrar[1]==0){
									  borrar[2]++;
								   }
							}
							buf4[index++] = borrar[2];
							buf4[index++] = borrar[1];
							buf4[index++] = borrar[0];


				if(index >= L_BUF)
					index=0;
				nuevodato='s';
				CantDatosGenerados++;
			}*/

/*

			for(i=0;i<4;i++)
			{
			  buf4[index] = B[i];
			  index++;
			}
*/


		//	if(CantDatosGenerados)//UMBRAL

			if(ind > L_BUF)
			{
				ind = 0;
			}
		}
		// ERROR
	}
	else if (reg_status & GPIOINT_INTSTAT_P2INT)
	{
				// Interrupción en pines de P2
				// reg_status = LPC_GPIOINT->IO2IntStatF;
				if(LPC_GPIOINT->IO2IntStatR & GPIOINT_INTSTAT_P211REI)
				{
					LPC_GPIOINT->IO2IntClr |= GPIOINT_INTCLR_P211CI;				// Limpio la interrupcion pendiente
					// LPC_GPIOINT->IO2IntEnR &= ~GPIOINT_IO2INTENR_P211;				// Desactivo temporalmente la interrupcion
					antirreb_estado = ANTIRREB_INI;									// Muevo un lugar la máquina de estados
					ms_contador_antirreb = 0;
					antirreb_activo = 1;
				}



	}
	NVIC_EnableIRQ(EINT3_IRQn);
	return;
}
