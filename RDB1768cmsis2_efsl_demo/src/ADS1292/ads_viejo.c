/*
 * ads_viejo.c
 *
 *  Created on: 20/11/2014
 *      Author: froux
 */




//uint32_t demora = 500;


void ADS_Prueba(void);
/**
    \fn void ADS_Init (uint8_t channels, uint8_t sampling)
    \brief Configura el modo de funcionamiento e inicialización del ADS1292
    \returns void
    \author Lautaro Petrauskas
    \date 2013.11.15
*/

/*
void ADS_Init (uint8_t channels, uint8_t sampling){
	ADS_SPI_Init();
	ADS_Command(ADS_RESET);								// Reseteo el ADS
    ADS_Command(ADS_SDATAC);							// STOP DATA

    ADS_Config(ADS_CONFIG1,ADS_WRITE,sampling);			// 125 SPS
    ADS_Config(ADS_CONFIG2,ADS_WRITE,0xA0);  		  	//  Reference Buffer ON
    ADS_Config(ADS_RLD_SENS,ADS_WRITE,0x20);    		// RLD Buffer ON

    if (channels == 1){									// Turn Off Channel 2
       ADS_Config(ADS_CH2SET,ADS_WRITE,0x05);			// Input Short Channel 2
       ADS_Config(ADS_CH2SET,ADS_WRITE,0x81);			// Channel 2 Power Down
    }


  //  ADS_Config(ADS_CH1SET,ADS_WRITE,0x05);			// Signal Measuring
    //flag = ADS_Config(ADS_CONFIG2,ADS_READ,0x00);

//  flag = ADS_Config(ADS_CH1SET,ADS_READ,0x00);
	//ADS_Config(ADS_CONFIG2,ADS_WRITE,0xA3);			// Test Signal ON con cuadrada a 1Hz
    //ADS_Config(ADS_CH2SET,ADS_WRITE,0x00);			//  Channel 2 Input a Test Signal

	//ADS_Config(ADS_CH2SET,ADS_WRITE,0x05);			// Channel 2 Input a Test Signal
    ADS_Command(ADS_RDATAC);
    ADS_Command(ADS_START);
}
*/
/**
    \fn uint8_t ADS_Config(uint8_t reg, uint8_t rw, uint8_t data )
    \brief Función que permite escribir o leer los registros del ADS12xx
    \returns dato a leer de ser requerido
    \author Lautaro Petrauskas
    \date 2013.11.15
*/
/*
void ADS_Prueba(void)
{

	ADS_Command(ADS_RESET);								// Reseteo el ADS
	ADS_Command(ADS_SDATAC);							// STOP DATA

	ADS_Config(ADS_CONFIG2,ADS_WREG,0x88);  		  	//  Reference Buffer ON

	return;
}

uint8_t ADS_Config(uint8_t reg, uint8_t rw, uint8_t data )
{
	uint8_t opcode;
	extern uint32_t demora;
	while(demora--);
	// uint8_t aux;

	ADS_SPI_CS_Low();

	// SPI_SendByte(rw|reg);
	//opcode = rw|reg;
	// SSPSend( 1, &opcode, 1 );
	SSP1_Enviar_Byte(rw | reg);

	demora = 50;while(demora--);

	// SPI_SendByte(0x00);
	// opcode = 0x00; SSPSend( 1, &opcode, 1);
	SSP1_Enviar_Byte(0x00);

	demora = 50; while(demora--);

	if(rw==ADS_WREG)
	{
		//SPI_SendByte(data);
		// opcode = data;
		// SSPSend(1, &opcode, 1);
		SSP1_Enviar_Byte(data);
	}
	else
		opcode = 0;
		//aux = SPI_RecvByte();

	// demora = 50;
	// while(demora--);
	ADS_SPI_CS_High();
	demora = 500;
	return 0;
}


void ADS_Command(uint8_t command )
{
	extern uint32_t demora;
	ADS_SPI_CS_Low();
	// SSPSend( 1, &command, 1 );
	// SPI_SendByte(command);
	SSP1_Enviar_Byte(command);
	demora = 50;
	while(demora--);
	demora = 50;
	ADS_SPI_CS_High();
	return;
}
*/
