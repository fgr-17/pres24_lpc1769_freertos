/*
 * systick.h
 *
 *  Created on: 12/11/2014
 *      Author: froux
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

#include <LPC17xx.h>
#include <stdint.h>

extern volatile uint32_t msTicks; // counter for 1ms SysTicks
// ****************
// systick_delay - creates a delay of the appropriate number of Systicks (happens every 1 ms)
extern void systick_delay (uint32_t delayTicks);

#endif /* SYSTICK_H_ */
