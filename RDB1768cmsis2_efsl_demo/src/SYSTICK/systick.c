/*
 * systick.c
 *
 *  Created on: 12/11/2014
 *      Author: froux
 */

#include <stdint.h>

volatile uint32_t msTicks; // counter for 1ms SysTicks

#include <SYSTICK/systick.h>

void systick_delay (uint32_t delayTicks);
// Nuestro. no bardear
extern volatile uint32_t msTicks;


void systick_delay (uint32_t delayTicks)
{
  uint32_t currentTicks;
  currentTicks = msTicks;	// read current tick counter
  // Now loop until required number of ticks passes.
  while ((msTicks - currentTicks) < delayTicks);
}

