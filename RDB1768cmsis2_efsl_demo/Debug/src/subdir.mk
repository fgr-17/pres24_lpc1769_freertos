################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Integrador_FW_GPIO.c \
../src/QRS_Det.c \
../src/cr_startup_lpc176x.c \
../src/dac.c \
../src/efsl_demo.c \
../src/fases.c \
../src/leds.c \
../src/pulsador.c \
../src/timer.c \
../src/uart0.c \
../src/wav.c 

OBJS += \
./src/Integrador_FW_GPIO.o \
./src/QRS_Det.o \
./src/cr_startup_lpc176x.o \
./src/dac.o \
./src/efsl_demo.o \
./src/fases.o \
./src/leds.o \
./src/pulsador.o \
./src/timer.o \
./src/uart0.o \
./src/wav.o 

C_DEPS += \
./src/Integrador_FW_GPIO.d \
./src/QRS_Det.d \
./src/cr_startup_lpc176x.d \
./src/dac.d \
./src/efsl_demo.d \
./src/fases.d \
./src/leds.d \
./src/pulsador.d \
./src/timer.d \
./src/uart0.d \
./src/wav.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__USE_CMSIS=CMSISv2p00_LPC17xx -DCR_INTEGER_PRINTF -DDEBUG -D__CODE_RED -D__REDLIB__ -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/CMSISv2p00_LPC17xx/inc" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src/ADS1292" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src/SSP" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_lib/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/cr_startup_lpc176x.o: ../src/cr_startup_lpc176x.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__USE_CMSIS=CMSISv2p00_LPC17xx -DCR_INTEGER_PRINTF -DDEBUG -D__CODE_RED -D__REDLIB__ -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/CMSISv2p00_LPC17xx/inc" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src/ADS1292" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src/SSP" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_lib/inc" -O0 -Os -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"src/cr_startup_lpc176x.d" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


