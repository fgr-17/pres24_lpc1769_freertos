################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/SYSTICK/systick.c 

OBJS += \
./src/SYSTICK/systick.o 

C_DEPS += \
./src/SYSTICK/systick.d 


# Each subdirectory must supply rules for building sources it contributes
src/SYSTICK/%.o: ../src/SYSTICK/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__USE_CMSIS=CMSISv2p00_LPC17xx -DCR_INTEGER_PRINTF -DDEBUG -D__CODE_RED -D__REDLIB__ -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/CMSISv2p00_LPC17xx/inc" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src/ADS1292" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_demo/src/SSP" -I"/home/froux/MEGA/LPC1769_Repo/LPC_Open_Free_RTOS/RDB1768cmsis2_efsl_lib/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


