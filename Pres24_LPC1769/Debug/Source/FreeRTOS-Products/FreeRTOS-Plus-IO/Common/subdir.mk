################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_DriverInterface.c \
C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_IOUtils.c \
C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CharQueueTxAndRx.c \
C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CircularBufferRx.c \
C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_ZeroCopyTx.c 

OBJS += \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_DriverInterface.o \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_IOUtils.o \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CharQueueTxAndRx.o \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CircularBufferRx.o \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_ZeroCopyTx.o 

C_DEPS += \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_DriverInterface.d \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_IOUtils.d \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CharQueueTxAndRx.d \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CircularBufferRx.d \
./Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_ZeroCopyTx.d 


# Each subdirectory must supply rules for building sources it contributes
Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_DriverInterface.o: C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_DriverInterface.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DPROTOCOLOBT -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Device\LPC17xx\SupportedBoards" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\portable\GCC\ARM_CM3" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-CLI" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\CMSISv2p00_LPC17xx\inc" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\FatFS" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include\ipv4" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\netif\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\lwIP_Apps" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\Examples\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\lpc17xx.cmsis.driver.library\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -Wextra -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_IOUtils.o: C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/FreeRTOS_IOUtils.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DPROTOCOLOBT -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Device\LPC17xx\SupportedBoards" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\portable\GCC\ARM_CM3" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-CLI" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\CMSISv2p00_LPC17xx\inc" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\FatFS" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include\ipv4" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\netif\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\lwIP_Apps" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\Examples\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\lpc17xx.cmsis.driver.library\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -Wextra -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CharQueueTxAndRx.o: C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CharQueueTxAndRx.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DPROTOCOLOBT -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Device\LPC17xx\SupportedBoards" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\portable\GCC\ARM_CM3" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-CLI" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\CMSISv2p00_LPC17xx\inc" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\FatFS" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include\ipv4" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\netif\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\lwIP_Apps" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\Examples\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\lpc17xx.cmsis.driver.library\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -Wextra -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CircularBufferRx.o: C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_CircularBufferRx.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DPROTOCOLOBT -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Device\LPC17xx\SupportedBoards" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\portable\GCC\ARM_CM3" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-CLI" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\CMSISv2p00_LPC17xx\inc" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\FatFS" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include\ipv4" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\netif\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\lwIP_Apps" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\Examples\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\lpc17xx.cmsis.driver.library\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -Wextra -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_ZeroCopyTx.o: C:/Pres24_Firmware_Repo/pres24_lpc1769_freertos/FreeRTOS-Products/FreeRTOS-Plus-IO/Common/IOUtils_ZeroCopyTx.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DPROTOCOLOBT -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-IO\Device\LPC17xx\SupportedBoards" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS\portable\GCC\ARM_CM3" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\FreeRTOS-Products\FreeRTOS-Plus-CLI" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\CMSISv2p00_LPC17xx\inc" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\FatFS" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include\ipv4" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\netif\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\lwIP_Apps" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\lwIP\include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source\Examples\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\lpc17xx.cmsis.driver.library\Include" -I"C:\Pres24_Firmware_Repo\pres24_lpc1769_freertos\Pres24_LPC1769\Source" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -Wextra -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


