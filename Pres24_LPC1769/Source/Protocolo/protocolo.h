/*
 * protocolo.h
 *
 *  Created on: 05/03/2016
 *      Author: i7
 */

#ifndef PROTOCOLO_PROTOCOLO_H_
#define PROTOCOLO_PROTOCOLO_H_

/*======================================================================================================
 * 		DEFINICION DE CONSTANTES :
 *======================================================================================================*/

#ifdef PROTOCOLOBT

#define LISTA_COMANDOS_CANT											7								// 7 comandos : version ampliada con adquisición controlada y señal de prueba
#define COMANDO_LARGO												1

#define CMD_ARR														0
#define CMD_S														1
#define CMD_P	 													2
#define CMD_E														3
#define CMD_A														4
#define CMD_B														5
#define CMD_T														6

#endif

#define PRT_RESPUESTA_L												6								// Longitud de todas las respuestas
#define PRT_SINCRONISMO_REP											10								// Repeticiones del paquete de sincronismo

#define PRT_BUFFER_ONLINE_L											CANAL_BUFFER_L

#define FYH_ASCII_L													24								// 21 caracteres mas 4 ceros para terminar

#define FYH_ASCII_DDD												0								// Caracter decenas de dia
#define FYH_ASCII_DDU												1								// Caracter unidades de dia
#define FYH_ASCII_MMD												3								// Caracter decenas de mes
#define FYH_ASCII_MMU												4								// Caracter unidades de mes
#define FYH_ASCII_AAD												8								// Caracter decenas de año
#define FYH_ASCII_AAU												9								// Caracter unidades de año
#define FYH_ASCII_HORD												12 	 							// Caracter decenas de hora
#define FYH_ASCII_HORU												13								// Caracter unidades de hora
#define FYH_ASCII_MIND												15								// Caracter decenas de minuto
#define FYH_ASCII_MINU												16								// Caracter unidades de minuto
#define FYH_ASCII_SEGD												18								// Caracter decenas de segundo
#define FYH_ASCII_SEGU												19								// Caracter unidades de segundo

#define FECHA_Y_HORA_L												12

#define FECHA_Y_HORA_HOR 											0
#define FECHA_Y_HORA_MIN 											1
#define FECHA_Y_HORA_SEG 											2
#define FECHA_Y_HORA_DD  											3
#define FECHA_Y_HORA_MM  											4
#define FECHA_Y_HORA_AA  											5

/*======================================================================================================
 * 		DECLARACION DE TIPOS :
 *======================================================================================================*/

typedef enum {	ESPERO_SINC, ESPERO_START, ETAPA_ONLINE, ETAPA_ADQUIRIR, SENAL_PRUEBA, ETAPA_VER_ADQUIRIR,
				ESPERO_HOR, ESPERO_MIN, ESPERO_SEG, ESPERO_DD, ESPERO_MM, ESPERO_AA
} T_Estado_Protocolo;

/*======================================================================================================
 * 		FUNCIONES EXTERNAS :
 *======================================================================================================*/

extern void Instalar_Tarea_ProtocoloBT(void);

/*======================================================================================================
 * 		VARIABLES GLOBALES :
 *======================================================================================================*/

extern uint8_t autonum_paquete;																		// Cuenta del numero de paquete de datos enviado
extern T_Estado_Protocolo estado_protocolo;															// Variable de estado

extern uint8_t prt_paq_online [PRT_RESPUESTA_L];

extern int8_t fechayhora [FECHA_Y_HORA_L];
extern int8_t fyh_ascii [FYH_ASCII_L];
#endif /* PROTOCOLO_PROTOCOLO_H_ */
