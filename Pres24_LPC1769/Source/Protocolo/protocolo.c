/*
 * protocolo.c
 *
 *  Created on: 05/03/2016
 *      Author: i7
 */

/*======================================================================================================
 * 		INCLUSIÓN DE ARCHIVOS :
 *======================================================================================================*/

/* Standard includes. */
#include "string.h"
#include <stdbool.h>
/* Library includes. */
#include "lpc17xx_gpio.h"


/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+IO includes. */
#include "FreeRTOS_IO.h"

#include "ff.h"

/* Includes propios */
#include <Menu/tipos.h>
#include <Menu/menu.h>
#include <UART/uart.h>
#include <ADS1292/ads1292.h>
#include <Protocolo/protocolo.h>
#include "SPI-interface-to-SD-card.h"
// #include <HC-05/hc05.h>

/*======================================================================================================
 * 		DECLARACIONES DE CONSTANTES DE TAREAS - STACK Y PRIORIDAD :
 *======================================================================================================*/

#define PROTOCOLO_STACK							(configMINIMAL_STACK_SIZE * (1UL))
#define PROTOCOLO_PRIORIDAD						3

#define ENVIAR_ONLINE_STACK						(configMINIMAL_STACK_SIZE * (1UL))
#define ENVIAR_ONLINE_PRIORIDAD					1

#define SENAL_PRUEBA_STACK						(configMINIMAL_STACK_SIZE * (1UL))
#define SENAL_PRUEBA_PRIORIDAD					1


/*======================================================================================================
 * 		PROTOTIPO DE FUNCIONES :
 *======================================================================================================*/

void Instalar_Tarea_ProtocoloBT(void);
void Inicializar_Tarea_ProtocoloBT (void);
void Tarea_ProtocoloBT (void *pvParameters);


void Instalar_Tarea_Enviar_Online (void);
void Desinstalar_Tarea_Enviar_Online (void);
void Tarea_Enviar_Online (void *pvParameters);


/*======================================================================================================
 * 		VARIABLES GLOBALES :
 *======================================================================================================*/

uint8_t autonum_paquete = 0;																		// Cuenta del numero de paquete de datos enviado

T_Estado_Protocolo estado_protocolo;																// Variable de estado

uint8_t prt_paq_sincronismo [PRT_RESPUESTA_L] = {'U', 0, 0, 0, 0, 0};
uint8_t prt_paq_online [PRT_RESPUESTA_L] = {'U', 0, 0, 0, 0, 0};
uint8_t prt_paq_detener [PRT_RESPUESTA_L] = {'P', '@', '@', '@', '@', '@'};

uint8_t prt_paq_adquirir [PRT_RESPUESTA_L] = {'A', '@', '@', '@', '@', '@'};
uint8_t prt_paq_finadquirir [PRT_RESPUESTA_L] = {'B', '@', '@', '@', '@', '@'};

uint8_t prt_paq_fyhrecibido [PRT_RESPUESTA_L] = {'F', '@', '@', '@', '@', '@'};

#ifdef PROTOCOLOBT
int8_t  lista_cmd [LISTA_COMANDOS_CANT] = { '@', 'S', 'P', 'E', 'A', 'B', 'T' };
#endif

int16_t bufprueba [CANAL_BUFFER_L] = {
 #include <SEN/sen16bit.h>
};

int32_t bufprueba_ind = 0;

int8_t fechayhora [FECHA_Y_HORA_L] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
int8_t fyh_ascii [FYH_ASCII_L] = {0, 0, '-', 0, 0, '-', '2', '0', 0, 0, 10, 13, 0, 0, ':', 0, 0, ':', 0, 0, 0, 0, 0, 0};

/*======================================================================================================
 * 		IMPLEMENTACION DE FUNCIONES :
 *======================================================================================================*/

#define PCENET_BIT				(1 << 30)
#define PCI2C2_BIT				(1 << 26)
#define PCI2C1_BIT				(1 << 19)
#define PCI2C0_BIT				(1 << 7)
#define PCPWM1_BIT				(1 << 6)
#define PCUART1_BIT				(1 << 4)
#define PCTIM0_BIT				(1 << 1)
#define PCTIM1_BIT				(1 << 2)


void Apagar_Perifericos_Inactivos(void)
{
	LPC_SC->PCONP &= ~(PCENET_BIT | PCI2C2_BIT | PCI2C1_BIT | PCI2C0_BIT | PCPWM1_BIT | PCUART1_BIT | PCTIM0_BIT | PCTIM1_BIT);
}


/*======================================================================================================
 *	\function	void Instalar_Tarea_ProtocoloBT(void)
 * 	\brief 		Instalo la tarea que administra el protocolo BT
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Instalar_Tarea_ProtocoloBT(void)
{
	Inicializar_Tarea_ProtocoloBT();
	xTaskCreate( Tarea_ProtocoloBT,
				( const int8_t * const ) "Protocolo", 												/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
				PROTOCOLO_STACK,																	/* The size of the stack allocated to the task. */
				NULL,																				/* The parameter is not used, so NULL is passed. */
				PROTOCOLO_PRIORIDAD,																/* The priority allocated to the task. */
				NULL );
	return;
}

/*======================================================================================================
 *	\function	void Inicializar_Tarea_ProtocoloBT(void)
 * 	\brief 		Inicializo variables de la tarea que administra el protocolo BT
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Inicializar_Tarea_ProtocoloBT (void)
{
	estado_protocolo = ESPERO_SINC;
}

/*======================================================================================================
 *	\function	void tarea_ProtocoloBT (void *pvParameters)
 * 	\brief 		Tarea que administra el protocolo BT
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Tarea_ProtocoloBT (void *pvParameters)
{
	int32_t comando_recibido;

	PINDEBUG_CONFIGURAR_PINES();
	PINDEBUG_EN_DESACTIVAR();
	PINDEBUG_EN_ACTIVAR();
	PINDEBUG_EN_DESACTIVAR();


	Apagar_Perifericos_Inactivos();

	if(UART_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	if(ADS1292_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	if(SD_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	// UART_Enviar_Mensaje((const int8_t * const)"Pres 24 : Consola de usuario \n\n\r");




	Instalar_Adquirir();
	Instalar_Tarea_Enviar_Online();
	while(1)
	{
		comando_recibido = UART_Recibir_Comando (lista_cmd, LISTA_COMANDOS_CANT);

		switch(estado_protocolo)
		{
		case ESPERO_SINC:

			if(comando_recibido == CMD_ARR)
			{
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);

				estado_protocolo = ESPERO_START;

			}
			break;

		case ESPERO_START:

			if(comando_recibido == CMD_ARR)
			{
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_sincronismo, PRT_RESPUESTA_L);

				estado_protocolo = ESPERO_START;
			}
			else if (comando_recibido == CMD_S)
			{
				// Instalar_Tarea_Enviar_Online();
				autonum_paquete = 0;
				estado_protocolo = ETAPA_ONLINE;
			}
			else if (comando_recibido == CMD_A)
			{
				UART_Enviar_Buffer((int8_t*)prt_paq_adquirir, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_adquirir, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_adquirir, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_adquirir, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_adquirir, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_adquirir, PRT_RESPUESTA_L);

				// Crear_Sets_Archivos();
				// ADS1292_adquiriendo = 1;											// habilito la adquisicion en la tarea
				estado_protocolo = ESPERO_HOR;
			}

			else if (comando_recibido == CMD_T)
			{
				// asigno puntero pEnv de los dos canales a señal de prueba.
				bufprueba_ind = 0;
				autonum_paquete = 0;
				estado_protocolo = SENAL_PRUEBA;
			}

			break;

		case ETAPA_ONLINE:

			if(comando_recibido == CMD_P)
			{

				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);

				estado_protocolo = ESPERO_SINC;
				// Desinstalar_Tarea_Enviar_Online();

			}
			else if (comando_recibido == CMD_E)
			{

				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);

				estado_protocolo = ESPERO_SINC;
				// Desinstalar_Tarea_Enviar_Online();
			}

			break;

		case ESPERO_HOR :

			fechayhora[FECHA_Y_HORA_HOR] = (int8_t)comando_recibido;
			fyh_ascii [FYH_ASCII_HORD] = (int8_t) (fechayhora[FECHA_Y_HORA_HOR] / 10) + '0';
			fyh_ascii [FYH_ASCII_HORU] = (int8_t) (fechayhora[FECHA_Y_HORA_HOR] % 10) + '0';
			UART_Enviar_Buffer((int8_t*)prt_paq_fyhrecibido, PRT_RESPUESTA_L);
			estado_protocolo = ESPERO_MIN;
			break;

		case ESPERO_MIN :
			fechayhora[FECHA_Y_HORA_MIN] = (int8_t)comando_recibido;
			fyh_ascii [FYH_ASCII_MIND] = (int8_t) (fechayhora[FECHA_Y_HORA_MIN] / 10) + '0';
			fyh_ascii [FYH_ASCII_MINU] = (int8_t) (fechayhora[FECHA_Y_HORA_MIN] % 10) + '0';
			UART_Enviar_Buffer((int8_t*)prt_paq_fyhrecibido, PRT_RESPUESTA_L);
			estado_protocolo = ESPERO_SEG;
			break;

		case ESPERO_SEG :
			fechayhora[FECHA_Y_HORA_SEG] = (int8_t)comando_recibido;
			fyh_ascii [FYH_ASCII_SEGD] = (int8_t) (fechayhora[FECHA_Y_HORA_SEG] / 10) + '0';
			fyh_ascii [FYH_ASCII_SEGU] = (int8_t) (fechayhora[FECHA_Y_HORA_SEG] % 10) + '0';
			UART_Enviar_Buffer((int8_t*)prt_paq_fyhrecibido, PRT_RESPUESTA_L);
			estado_protocolo = ESPERO_DD;
			break;

		case ESPERO_DD :
			fechayhora[FECHA_Y_HORA_DD] =(int8_t) comando_recibido;
			fyh_ascii [FYH_ASCII_DDD] = (int8_t) (fechayhora[FECHA_Y_HORA_DD] / 10) + '0';
			fyh_ascii [FYH_ASCII_DDU] = (int8_t) (fechayhora[FECHA_Y_HORA_DD] % 10) + '0';
			UART_Enviar_Buffer((int8_t*)prt_paq_fyhrecibido, PRT_RESPUESTA_L);
			estado_protocolo = ESPERO_MM;
			break;

		case ESPERO_MM :
			fechayhora[FECHA_Y_HORA_MM] = (int8_t)comando_recibido;
			fyh_ascii [FYH_ASCII_MMD] = (int8_t) (fechayhora[FECHA_Y_HORA_MM] / 10) + '0';
			fyh_ascii [FYH_ASCII_MMU] = (int8_t) (fechayhora[FECHA_Y_HORA_MM] % 10) + '0';
			UART_Enviar_Buffer((int8_t*)prt_paq_fyhrecibido, PRT_RESPUESTA_L);
			estado_protocolo = ESPERO_AA;
			break;

		case ESPERO_AA :
			fechayhora[FECHA_Y_HORA_AA] = (int8_t)comando_recibido;
			fyh_ascii [FYH_ASCII_AAD] = (int8_t) (fechayhora[FECHA_Y_HORA_AA] / 10) + '0';
			fyh_ascii [FYH_ASCII_AAU] = (int8_t) (fechayhora[FECHA_Y_HORA_AA] % 10) + '0';
			UART_Enviar_Buffer((int8_t*)prt_paq_fyhrecibido, PRT_RESPUESTA_L);
			estado_adquisicion = ADQUISICION_CREAR_ARCHIVO;
			estado_protocolo = ETAPA_ADQUIRIR;
			break;

		case ETAPA_ADQUIRIR :

			if (comando_recibido == CMD_B)
			{
				UART_Enviar_Buffer((int8_t*)prt_paq_finadquirir, PRT_RESPUESTA_L);
				estado_adquisicion = ADQUISICION_CERRAR_ARCHIVO;												// Finalizo adquisicion
				estado_protocolo = ESPERO_SINC;
			}
			else if (comando_recibido == CMD_S)
			{
				autonum_paquete = 0;
				estado_protocolo = ETAPA_VER_ADQUIRIR;
			}

			break;

		case ETAPA_VER_ADQUIRIR :

			if( comando_recibido == CMD_P)
			{
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				estado_protocolo = ETAPA_ADQUIRIR;
			}
			else if (comando_recibido == CMD_E)
			{
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				estado_protocolo = ETAPA_ADQUIRIR;
			}
			break;

		case SENAL_PRUEBA :

			if(comando_recibido == CMD_P)
			{

				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);
				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);

				estado_protocolo = ESPERO_SINC;
				// Desinstalar_Tarea_Enviar_Online();

			}
			else if (comando_recibido == CMD_E)
			{

				UART_Enviar_Buffer((int8_t*)prt_paq_detener, PRT_RESPUESTA_L);

				estado_protocolo = ESPERO_SINC;
				// Desinstalar_Tarea_Enviar_Online();
			}



		default:
			Inicializar_Tarea_ProtocoloBT();

		}
	}
	return;

}

/*======================================================================================================
 *	\function	void Instalar_Tarea_Enviar_Online (void *pvParameters)
 * 	\brief 		Instalador de la tarea
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Instalar_Tarea_Enviar_Online (void)
{
	xTaskCreate( Tarea_Enviar_Online,
				( const int8_t * const ) "EnviarOnline", 											/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
				ENVIAR_ONLINE_STACK,																	/* The size of the stack allocated to the task. */
				NULL,																				/* The parameter is not used, so NULL is passed. */
				ENVIAR_ONLINE_PRIORIDAD,																/* The priority allocated to the task. */
				NULL );
	return;
}

/*======================================================================================================
 *	\function	void Tarea_Enviar_Online (void *pvParameters)
 * 	\brief 		Desinstalador de la tarea
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Desinstalar_Tarea_Enviar_Online (void)
{
	vTaskDelete(Tarea_Enviar_Online);
	return;
}

/*======================================================================================================
 *	\function	void Tarea_Enviar_Online (void *pvParameters)
 * 	\brief 		Tarea que envía los datos en forma online
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Tarea_Enviar_Online (void *pvParameters)
{
	while(1)
	{

		if((estado_protocolo == ETAPA_ONLINE) || (estado_protocolo == ETAPA_VER_ADQUIRIR)||(estado_protocolo == SENAL_PRUEBA))
		{
			if((canal0.estado_buffer == PENDIENTE)) //|| (canal1.estado_buffer == PENDIENTE))
			{
				canal0.ind_envio = 0;
				canal1.ind_envio = 0;

				canal0.estado_buffer = PROCESANDO;
				// canal1.estado_buffer = PROCESANDO;
			}

			if((canal0.estado_buffer == PROCESANDO)) // || (canal1.estado_buffer == PROCESANDO))
			{

				// canal1.ind_envio ++;
				if(estado_protocolo == SENAL_PRUEBA)
				{
					prt_paq_online[2] = (uint8_t)(bufprueba[bufprueba_ind]) >> 8;
					prt_paq_online[3] = (uint8_t)(bufprueba[bufprueba_ind]) >> 0;

					// El canal dos lo divido por la mitad
					prt_paq_online[4] = (uint8_t)(bufprueba[bufprueba_ind]) >> 9;
					prt_paq_online[5] = (uint8_t)(bufprueba[bufprueba_ind]) >> 1;
					bufprueba_ind++;

					if(bufprueba_ind >= PRT_BUFFER_ONLINE_L)
					{
						bufprueba_ind = 0;
					}


				}
				else
				{
					prt_paq_online[2] = (uint8_t)((canal0.pEnv[canal0.ind_envio] & 0xFFFF0000) >> 16);
					prt_paq_online[3] = (uint8_t)((canal0.pEnv[canal0.ind_envio] & 0x0000FFFF) >> 0);
					prt_paq_online[4] = (uint8_t)((canal1.pEnv[canal1.ind_envio] & 0xFFFF0000) >> 16);
					prt_paq_online[5] = (uint8_t)((canal1.pEnv[canal1.ind_envio] & 0x0000FFFF) >> 0);

					canal0.ind_envio++;
					canal1.ind_envio++;

					if(canal0.ind_envio >= PRT_BUFFER_ONLINE_L)
					{
						canal0.estado_buffer = FINALIZADO;
						canal0.ind_envio = 0;
						canal1.ind_envio = 0;
					}


				}
				prt_paq_online[1] = autonum_paquete;
				autonum_paquete++;
				UART_Enviar_Buffer((int8_t*)prt_paq_online, PRT_RESPUESTA_L);

			}
		}
		else
		{
			vTaskDelay(100);
			// Desinstalar_Tarea_Enviar_Online();
		}
	}
}

