/*
 * menu.c
 *
 *  Created on: 19/7/2015
 *      Author: froux
 */


/*======================================================================================================
 * 		INCLUSIÓN DE ARCHIVOS :
 *======================================================================================================*/

/* Standard includes. */
#include "string.h"
#include <stdbool.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "ff.h"
/* FreeRTOS+IO includes. */
#include "FreeRTOS_IO.h"

/* Includes propios */
#include <Menu/tipos.h>
#include <Menu/menu.h>

#include <UART/uart.h>
#include <ADS1292/ADS1292.h>
#include "SPI-interface-to-SD-card.h"
#include <HC-05/hc05.h>
#include <Protocolo/protocolo.h>

/* Includes grales : */
#include <lpc17xx_gpio.h>


uint8_t empezo_read_ssp_SD = 0;


/*======================================================================================================
 * 		DEFINICIONES DE CONSTANTES DE TAREAS - STACK Y PRIORIDAD :
 *======================================================================================================*/

#define MENU_INICIAL_STACK									(configMINIMAL_STACK_SIZE * (1UL))
#define MENU_INICIAL_PRIORIDAD								1

#define ADQUIRIR_STACK										(configMINIMAL_STACK_SIZE * (8UL))
#define ADQUIRIR_PRIORIDAD									3

/*======================================================================================================
 * 		IMPLEMENTACIÓN DE FUNCIONES Y TAREAS :
 *======================================================================================================*/

void Instalar_Menu_Inicial (void);
void tarea_Menu_Inicial ( void *pvParameters );

void Desinstalar_Adquirir (void);
void Instalar_Adquirir (void);
void tarea_Adquirir (void*pvParameters);

void Crear_Sets_Archivos (void);

/*======================================================================================================
 * 		DEFINICION DE VARIABLES :
 *======================================================================================================*/

/* Mensaje de Bienvenida : */
const int8_t * const mensaje_bienvenida = ( int8_t * ) "PRES24 Consola";

/* Lista de comandos : */

#ifndef PROTOCOLOBT

#define LISTA_COMANDOS_CANT								4
#define COMANDO_LARGO									1

#define CMD_ADQ				0
#define CMD_VER				1
#define CMD_FIN 			2
#define CMD_INI				3


int8_t  lista_cmd [LISTA_COMANDOS_CANT] = { 'a', 'v', 'f', 'i' };

#endif

char nombre_archivo [ARCHIVO_NOMBRE_LARGO];
char nombre_archivo_c0 [ARCHIVO_NOMBRE_LARGO];
char nombre_archivo_c1 [ARCHIVO_NOMBRE_LARGO];

FIL pArchivoDatos;
FIL ArchivoCanal0;
FIL ArchivoCanal1;

volatile enum {ESPERANDO, ABRO_ARCHIVO, ADQUIRIENDO, TRASMITIENDO} estado_adquirir;
t_estado_adquisicion estado_adquisicion = ADQUISICION_INACTIVO;
/*======================================================================================================
 * 		IMPLEMENTACIÓN DE FUNCIONES Y TAREAS :
 *======================================================================================================*/

/*======================================================================================================
 *	\function	void Instalar_Menu_Inicial (void)
 * 	\brief 		Instalo la tarea que inicializa la UART que lee la terminal
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Instalar_Menu_Inicial (void)
{
	xTaskCreate( tarea_Menu_Inicial,
				( const int8_t * const ) "MenuInicial", 											/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
				MENU_INICIAL_STACK,																	/* The size of the stack allocated to the task. */
				NULL,																				/* The parameter is not used, so NULL is passed. */
				MENU_INICIAL_PRIORIDAD,																/* The priority allocated to the task. */
				NULL );

}

/*======================================================================================================
 *	\function	void tarea_Menu_Inicial ( void *pvParameters )
 * 	\brief 		Manejo del menu inicial
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void tarea_Menu_Inicial ( void *pvParameters )
{
	int32_t comando_recibido = -1;
	int32_t linea_comandos = 1;
// #define TERMINAL
#ifdef TERMINAL
	if(UART_Inicializar() == PERIF_INICIALIZADO)
	{
		UART_Enviar_Mensaje((const int8_t * const)"Pres 24 : Consola de usuario \n\n\r");
		UART_Enviar_Mensaje((const int8_t * const)"Inicializacion del Sistema : \n\n\r");
		UART_Enviar_Mensaje((const int8_t * const)"\t> Inicializando Comunicacion...");
		UART_Enviar_Mensaje((const int8_t * const)"\t\tComunicacion Inicializada\n\r");
	}



	// HC05_Inicializar();


	UART_Enviar_Mensaje((const int8_t * const) "\t> Inicializando Conversor A/D...");
	if(ADS1292_Inicializar() == PERIF_INICIALIZADO)
	{
		UART_Enviar_Mensaje((const int8_t * const)"\tConversor A/D Inicializado\n\r");
	}
	else
	{
		UART_Enviar_Mensaje((const int8_t * const)"\tConversor A/D ERROR\n\r");
	}


	UART_Enviar_Mensaje((const int8_t * const) "\t> Inicializando Almacenamiento...");
	if(SD_Inicializar() == PERIF_INICIALIZADO)
	{
		UART_Enviar_Mensaje((const int8_t * const)"\tMemoria SD inicializada\n\r");
	}
	else
	{
		UART_Enviar_Mensaje((const int8_t * const)"\tMemoria SD error\n\r");
	}


	UART_Enviar_Mensaje((const int8_t * const)"\n\n\rSistema inicializado. \n\r");
	UART_Enviar_Mensaje((const int8_t * const)"\n\n\rIngrese un comando: \n\r");




	/*******************************************
	 * INGRESO A LA LÍNEA DE COMANDOS :
	 ******************************************/

	while(linea_comandos)
	{
		UART_Enviar_Mensaje((const int8_t * const)"\n\r>");

		comando_recibido = UART_Recibir_Comando (lista_cmd, LISTA_COMANDOS_CANT);

		switch(comando_recibido)
		{
		case CMD_ADQ:

			/* Hago eco del comando */
			// UART_Enviar_Mensaje((const int8_t * const)"\ncomando adq");
			Instalar_Adquirir();
			// estado_adquirir = ABRO_ARCHIVO;
			break;

		case CMD_VER:
			// UART_Enviar_Mensaje((const int8_t * const)"\n\rcomando ver");
			Instalar_Ver();
			break;

		case CMD_FIN:
			UART_Enviar_Mensaje((const int8_t * const)"\n\rcomando fin");
			Cerrar_Archivo(&ArchivoCanal0);
			Cerrar_Archivo(&ArchivoCanal1);
			Desinstalar_Adquirir();

			estado_adquirir = ESPERANDO;
			break;

		default :
			UART_Enviar_Mensaje((const int8_t * const)"\ncomando no instalado");

		}

	}
#endif

// #define VISUAL
#ifdef VISUAL

	if(UART_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	if(ADS1292_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	if(SD_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	//UART_Enviar_Mensaje((const int8_t * const)"\n\rcomando finInicio");
	while(linea_comandos)
	{
		comando_recibido = UART_Recibir_Comando (lista_cmd, LISTA_COMANDOS_CANT);

		switch(comando_recibido)
		{
		case CMD_ADQ:

			/* Hago eco del comando */
			// UART_Enviar_Mensaje((const int8_t * const)"\ncomando adq");
			Instalar_Adquirir();
			UART_Enviar_Mensaje((const int8_t * const)"a");
			// estado_adquirir = ABRO_ARCHIVO;
			break;

		case CMD_VER:
			// UART_Enviar_Mensaje((const int8_t * const)"\n\rcomando ver");
			Instalar_Ver();
			UART_Enviar_Mensaje((const int8_t * const)"v");
			break;

		case CMD_FIN:

			//UART_Enviar_Mensaje((const int8_t * const)"\n\rcomando fin");
			/* Eco del comando */
			UART_Enviar_Mensaje((const int8_t * const)"f");
			/* Cierro archivos */
			Cerrar_Archivo(&ArchivoCanal0);
			Cerrar_Archivo(&ArchivoCanal1);

			/* Desinstalo tareas : (agregar flag para ver cual está activa) : */
			Desinstalar_Adquirir();
			Desinstalar_Ver();

			/* Vuelvo al estado :*/
			estado_adquirir = ESPERANDO;
			break;

		case CMD_INI:

			UART_Enviar_Mensaje((const int8_t * const)"i");
			break;

		default:
			UART_Enviar_Mensaje((const int8_t * const)"X");
		}
	}



#endif
#define PROTOCOLO_PRUEBA
#ifdef PROTOCOLO_PRUEBA


	PINDEBUG_CONFIGURAR_PINES();
	PINDEBUG_EN_DESACTIVAR();
	PINDEBUG_EN_ACTIVAR();
	PINDEBUG_EN_DESACTIVAR();

	if(UART_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	if(ADS1292_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	if(SD_Inicializar() != PERIF_INICIALIZADO)
		configASSERT(0);

	// UART_Enviar_Mensaje((const int8_t * const)"Pres 24 : Consola de usuario \n\n\r");

	Instalar_Tarea_ProtocoloBT();
	Instalar_Adquirir();

#endif


	while(1)
	{
		vTaskDelay(1000);
	}


}


/*======================================================================================================
 *	\function	void Desinstalar_Adquirir (void)
 * 	\brief 		Desinstalador de la tarea Adquirir
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Desinstalar_Adquirir (void)
{
	vTaskDelete(tarea_Adquirir);
	return;
}

/*======================================================================================================
 *	\function	void Instalar_Adquirir (void)
 * 	\brief 		Instalador de la tarea Adquirir
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Instalar_Adquirir (void)
{

	estado_adquirir = ESPERANDO;
	xTaskCreate (tarea_Adquirir,
				 (const int8_t* const) "Adquirir",
				 ADQUIRIR_STACK,
				 NULL,
				 ADQUIRIR_PRIORIDAD,
				 NULL);

	return;
}

/*======================================================================================================
 *	\function	void tarea_Adquirir (void*pvParameters)
 * 	\brief 		Adquisicion en buffer y almacenamiento en SD
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void tarea_Adquirir (void*pvParameters)
{

/**/

	if(ADS1292_inicializado) // && !ADS1292_adquiriendo)							// Chequeo que el ADS esté correctamente inicializado y no esté adquiriendo
	{
		ADS1292_Iniciar_Adquisicion();											// Envío comandos de inicializacion
		// ADS1292_adquiriendo = 1;												// Señalizo el flag de adquisición activa
	}
#ifdef TERMINAL
	UART_Enviar_Mensaje((const int8_t * const) "\n\n\rInicio adquisicion : ");
	UART_Enviar_Mensaje((const int8_t * const) nombre_archivo);
	UART_Enviar_Mensaje((const int8_t * const) "\n\r");
#endif

	while(1)
	{

		switch(estado_adquisicion)
		{
		case ADQUISICION_INACTIVO:
			// No hago nada
			vTaskDelay(100);
			break;
		case ADQUISICION_CREAR_ARCHIVO:

			if(Obtener_Nombre_Ultimo_Archivo(nombre_archivo, nombre_archivo_c0, nombre_archivo_c1))
			{
				configASSERT(0);
			}

			Crear_Parte_Archivo(&pArchivoDatos, nombre_archivo);						// Creo el archivo en el que voy a escribir y lo dejo abierto :
			Descargar_Buffer_A_Archivo(&pArchivoDatos, (int32_t*) fyh_ascii);
			Cerrar_Archivo(&pArchivoDatos);
			Crear_Parte_Archivo(&ArchivoCanal0, nombre_archivo_c0);
			Crear_Parte_Archivo(&ArchivoCanal1, nombre_archivo_c1);

			estado_adquisicion = ADQUISICION_ACTIVO;
			break;

		case ADQUISICION_ACTIVO :

			/* Pruebo tomar el semáforo de buffers completos */
			if(xSemaphoreTake( vSemaforoBin_Buffers_completos, portMAX_DELAY) == pdTRUE)
			{
				/* Los buffers de los dos canales se completaron */
				empezo_read_ssp_SD = 1;

				PINDEBUG_EN_ACTIVAR();
				Descargar_Buffer_A_Archivo(&ArchivoCanal0, canal0.pAlm);//(&canal0, nombre_archivo_c0);
				Descargar_Buffer_A_Archivo(&ArchivoCanal1, canal1.pAlm);//(&canal1, nombre_archivo_c1);
				PINDEBUG_EN_DESACTIVAR();

				empezo_read_ssp_SD = 0;

				//Descargar_Buffer_A_Archivo(&ArchivoCanal0, bufprueba);
				//Descargar_Buffer_A_Archivo(&ArchivoCanal1, bufprueba);
				/*
				switch(i)
				{
				case 0:
					UART_Enviar_Mensaje((const int8_t * const)"\rAdquiriendo   ");
					break;
				case 1:
					UART_Enviar_Mensaje((const int8_t * const)"\rAdquiriendo.  ");
					break;
				case 2:
					UART_Enviar_Mensaje((const int8_t * const)"\rAdquiriendo.. ");
					break;
				case 3:
					UART_Enviar_Mensaje((const int8_t * const)"\rAdquiriendo...");
					i = -1;
					break;
				}*/
			}
			else
			{
				// UART_Enviar_Mensaje((const int8_t * const)"\error: tomar semaforo buffer completo");
			}
			break;

		case ADQUISICION_CERRAR_ARCHIVO:

			Cerrar_Archivo(&ArchivoCanal0);
			Cerrar_Archivo(&ArchivoCanal1);
			estado_adquisicion = ADQUISICION_INACTIVO;

			break;
		}
	}


	return;
}

/*======================================================================================================
 *	\function	void Crear_Sets_Archivos (void)
 * 	\brief 		Creo set de archivos de cada canal y el de configuración
 * 	\author		ROUX, Federico G. (rouxfederico@gmail.com)
 *======================================================================================================*/

void Crear_Sets_Archivos (void)
{
	if(Obtener_Nombre_Ultimo_Archivo(nombre_archivo, nombre_archivo_c0, nombre_archivo_c1))
	{
		configASSERT(0);
	}

	Crear_Parte_Archivo(&pArchivoDatos, nombre_archivo);						// Creo el archivo en el que voy a escribir y lo dejo abierto :
	Cerrar_Archivo(&pArchivoDatos);
	Crear_Parte_Archivo(&ArchivoCanal0, nombre_archivo_c0);
	Crear_Parte_Archivo(&ArchivoCanal1, nombre_archivo_c1);

	return;
}
