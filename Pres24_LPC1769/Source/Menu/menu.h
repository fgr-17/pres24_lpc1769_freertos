/*
 * menu.h
 *
 *  Created on: 19/7/2015
 *      Author: froux
 */

#ifndef MENU_MENU_H_
#define MENU_MENU_H_

/*======================================================================================================
 * 		DEFINICION DE CONSTANTES :
 *======================================================================================================*/

#define EST_MENU_INICIAL_NO_INICIALIZADO				0

/*======================================================================================================
 * 		DEFINICION DE PINES :
 *======================================================================================================*/

/* Defino pin para habilitar comandos AT : pin 38 LPCXpresso P0.4*/
#define PINDEBUG_EN_PORT							( 0 )
#define PINDEBUG_EN_PIN								( 0x01 << 22 )
#define PINDEBUG_EN_DIR								boardGPIO_OUTPUT

/* Defino pin de status : */
#define PINDEBUG_STATUS_PORT						( 0 )
#define PINDEBUG_STATUS_PIN							( 0x01 << 5 )
#define PINDEBUG_STATUS_DIR							boardGPIO_INPUT


#define PINDEBUG_CONFIGURAR_PINES()																\
{																							\
	GPIO_SetDir( PINDEBUG_EN_PORT, PINDEBUG_EN_PIN, PINDEBUG_EN_DIR );									\
	GPIO_SetDir( PINDEBUG_STATUS_PORT, PINDEBUG_STATUS_PIN, PINDEBUG_STATUS_DIR );						\
}


#define PINDEBUG_EN_ACTIVAR()						GPIO_SetValue( PINDEBUG_EN_PORT, PINDEBUG_EN_PIN )
#define PINDEBUG_EN_DESACTIVAR()					GPIO_ClearValue( PINDEBUG_EN_PORT, PINDEBUG_EN_PIN )


/*======================================================================================================
 * 		TIPOS DE DATO:
 *======================================================================================================*/

typedef enum  {ADQUISICION_INACTIVO, ADQUISICION_CREAR_ARCHIVO, ADQUISICION_ACTIVO, ADQUISICION_CERRAR_ARCHIVO} t_estado_adquisicion;

/*======================================================================================================
 * 		FUNCIONES EXTERNAS :
 *======================================================================================================*/


extern void Instalar_Menu_Inicial (void);
extern void Instalar_Adquirir (void);
extern void Crear_Sets_Archivos (void);

/*======================================================================================================
 * 		VARIABLES GLOBALES :
 *======================================================================================================*/

extern char nombre_archivo [];
extern t_estado_adquisicion estado_adquisicion;
// extern FIL pArchivoDatos;

#endif /* MENU_MENU_H_ */
