﻿/**
 \file ads1292.c
 \brief Driver y Handlers del integrado de adquisición de señales ADS1292
 \author Lautaro Petrauskas
 \date 2013.11.15
**/


/* ==================================================================================
 * 		INCLUSIÓN DE ARCHIVOS :
 * ================================================================================== */

#include <LPC17xx.h>
#include <stdint.h>
#include <stdbool.h>
#include <integer.h>

#include <FreeRTOS.h>
#include <portmacro.h>
#include <projdefs.h>
#include <lpc17xx_gpio.h>
#include <task.h>

#include <FreeRTOSConfig.h>

#include <FreeRTOS_DriverInterface.h>
#include <LPCXpresso17xx-base-board.h>
#include <semphr.h>


// #include <SSP/type.h>
#include "ff.h"
#include <Menu/tipos.h>
#include <Menu/menu.h>
#include <UART/uart.h>
#include <ADS1292/ADS1292.h>
// #include <SYSTICK/systick.h>
// #include <SSP/ssp.h>
// #include "pulsador.h"

#define DATOS_LISTOS_PRIORIDAD						4
#define DATOS_LISTOS_STACK							(configMINIMAL_STACK_SIZE * (10UL))


uint8_t empezo_read_ssp_ADS = 0;



/* ==================================================================================
 *		PROTOTIPOS DE FUNCIONES :
 * ================================================================================== */

static Peripheral_Descriptor_t ADS1292_Configurar_Canal_SSP( void );
static BYTE ADS1292_EnviarComandoCorto( BYTE cComando);
static BYTE ADS1292_EnviarComandoLargo( BYTE prim_byte, BYTE seg_byte, BYTE ter_byte);
static BYTE ADS1292_EscribirUnRegistro( BYTE registro, BYTE datos);
static BYTE ADS1292_LeerUnRegistro( BYTE registro, BYTE* byte_leido);
void ADS1292_Iniciar_Adquisicion ( void );

t_estado_periferico ADS1292_Inicializar (void);

void tCanal_Inicializar_Estructura (t_canal* canal);
__inline void tCanal_Intercambiar_Punteros (t_canal* canal) __attribute__((always_inline));

/* ==================================================================================
 *		LISTA DE TAREAS Y SU INSTALADOR :
 * ==================================================================================*/

/* Tarea de inicialización del hardware relacionado con el ADS1292 */
void ADS1292_InstalarTareaInicializacion( void );
void tarea_ADS1292_Inicializacion( void *pvParameters );

/* Tarea de handler del pin DRDY del ADS1292 */
void ADS1292_Instalar_Tarea_Datos_Listos ( void );
void tarea_ADS1292_Datos_Listos ( void *pvParameters );

/* Tarea de ejemplo para disparar la adquisición : */
void Instalar_Ejemplo_Inicia_Adquisicion (void);
void tarea_Ejemplo_Inicia_Adquisicion ( void *pvParameters );

/* ==================================================================================
 *		DEFINICION DE VARIABLES GLOBALES :
 * ==================================================================================*/

/* Semáforo binario para manejar la tarea que atiende el pin DR del ADS1292 */
static xSemaphoreHandle vSemaforoBin_ADS1292_DRDY = NULL;
/* Semáforo binario para manejar la taera que atiende el almacenamiento de los buffers de cada canal */
 xSemaphoreHandle vSemaforoBin_Buffers_completos = NULL;
/* Manejo del puerto SSP usado para el ADS1292 - required by FreeRTOS+IO calls. */
static Peripheral_Descriptor_t xADS1292_SSP = NULL;

/* Buffer de entrada de lectura del ADS1292 */
uint8_t ADS1292_Datos_Leidos [ADS1292_RDATAC_CANTBYTESLEIDOS];

/* Estructuras de canales de adquisición : */
t_canal canal0;
t_canal canal1;

/* Flags del ADS1292 */
volatile uint8_t ADS1292_inicializado = 0;
volatile uint8_t ADS1292_adquiriendo = 0;

/*======================================================================================================
 * 									FUNCIONES COMUNES :
 *======================================================================================================*/

/*======================================================================================================
 *	\function	void tCanal_Inicializar_Estructura (t_canal* canal)
 * 	\brief 		Inicializo la estructura de adquisición
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

void tCanal_Inicializar_Estructura (t_canal* canal)
{
	canal->ind = 0;												// Inicializo el índice de cuenta
	canal->pAct = canal->buf1;									// Seteo el puntero a buffer activo
	canal->pAlm = canal->buf2;									// Seteo el puntero a buffer de almacenamiento
	canal->pEnv = canal->buf3;									// Seteo el puntero a buffer de envio
	canal->estado_buffer = INACTIVO;							// Inicializo el estado del buffer a inactivo
	canal->buffer_lleno_pendiente = false;
	return;
}

/*======================================================================================================
 *	\function	void tCanal_Intercambiar_Punteros (t_canal* canal)
 * 	\brief 		Intercambio los punteros de adquisición y almacenamiento
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

__inline void tCanal_Intercambiar_Punteros (t_canal* canal)
{
	int32_t* pAux, pAux2;
/*
	pAux = canal->pAct;
	canal->pAct = canal->pAlm;
	canal->pAlm = pAux;
*/
	pAux = canal->pAlm;
	canal->pAlm = canal->pAct;
	pAux2 = canal->pEnv;
	canal->pEnv = pAux;
	canal->pAct = pAux2;

	return;
}



/*======================================================================================================
 * 									FUNCIONES PARA FREERTOS :
 *======================================================================================================*/

/*======================================================================================================
 *	\function	static Peripheral_Descriptor_t prv_ADS1292_Configurar_Canal_SSP( void )
 * 	\brief 		Tarea de inicializacion del puerto SSP utilizado por el ADS1292
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

static Peripheral_Descriptor_t ADS1292_Configurar_Canal_SSP( void )
{
/* Defino una variable para el puerto SPI inicializada en NULL */
static Peripheral_Descriptor_t xLocalSPIPort = NULL;

	if( xLocalSPIPort == NULL )
	{
		/* Abro el puerto SSP para comunicarme con el ADS1292. El 2do parámetro no lo uso */
		xLocalSPIPort = FreeRTOS_open( ADS1292_SSP_PORT, ( uint32_t ) ( ( void * ) 0 ) );
		configASSERT( xLocalSPIPort );

		/* Configuro el puerto con todos los parámetros especificados en el manual del ADS1292 */
		if( xLocalSPIPort != NULL )
		{
			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_SPI_DATA_BITS, 		( void * ) ADS1292_SSP_DATABIT_8 );
			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_SPI_CLOCK_PHASE, 	( void * ) ADS1292_SPI_SAMPLE_ON_TRAILING_EDGE_CPHA_1 );	/* FINAL */
			// FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_SPI_CLOCK_PHASE, 	( void * ) ADS1292_SPI_SAMPLE_ON_LEADING_EDGE_CPHA_0 ); /* PPIO */

			// FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_SPI_CLOCK_POLARITY, ( void * ) ADS1292_SPI_CLOCK_BASE_VALUE_CPOL_0 );
			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_SPI_CLOCK_POLARITY, ( void * ) ADS1292_SPI_CLOCK_BASE_VALUE_CPOL_1 );
			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_SPI_MODE, 			( void * ) ADS1292_SPI_MASTER_MODE );
			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_SSP_FRAME_FORMAT, 	( void * ) ADS1292_SSP_FRAME_SPI );

			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_SPEED, 	( void * ) ADS1292_SSP_FRECUENCIA);

			FreeRTOS_ioctl( xLocalSPIPort, ioctlUSE_ZERO_COPY_TX, ( ( void * ) 0 ) );

			FreeRTOS_ioctl( xLocalSPIPort, ioctlUSE_CHARACTER_QUEUE_TX, ( void * ) ADS1292_BUFFER_TAM_BYTES );
			FreeRTOS_ioctl( xLocalSPIPort, ioctlUSE_CHARACTER_QUEUE_RX, ( void * ) ADS1292_BUFFER_TAM_BYTES );
			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_RX_TIMEOUT, ( void * ) ADS1292_RX_TIMEOUT );
			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_TX_TIMEOUT, ( void * ) ADS1292_TX_TIMEOUT );
			// NVIC_SetPriority( SSP1_IRQn, (configSPI_INTERRUPT_PRIORITY));
// 			FreeRTOS_ioctl( xLocalSPIPort, ioctlSET_INTERRUPT_PRIORITY, (configSPI_INTERRUPT_PRIORITY - 2) );


		}
	}
	return xLocalSPIPort;
}

/*==========================================================================================================
 *	\function	static BYTE prvADS1292_EnviarComando_1_Byte( BYTE cComando)
 * 	\brief 		Envío un byte al ADS1292 a través del SSP seleccionado
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *==========================================================================================================*/


static BYTE ADS1292_EnviarComandoCorto( BYTE cComando)
{
	if( FreeRTOS_ioctl( xADS1292_SSP, ioctlOBTAIN_WRITE_MUTEX, ADS1292_MUTEX_TOUT ) == pdPASS )
	{
		/* Escribo y chequeo haber escrito todos los bytes */
		if( FreeRTOS_write( xADS1292_SSP, &cComando, ADS1292_LARGO_CMD_CORTO ) == ADS1292_LARGO_CMD_CORTO )
		{
			return 0;
		}
		return 1;
	}
	return 2;
}

/*==========================================================================================================
 *	\function	static BYTE prvADS1292_EnviarComando( BYTE cComando)
 * 	\brief 		Envío un byte al ADS1292 a través del SSP seleccionado
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *==========================================================================================================*/

static BYTE ADS1292_EnviarComandoLargo( BYTE prim_byte, BYTE seg_byte, BYTE ter_byte)
{

	BYTE string_comando [ADS1292_LARGO_CMD_LARGO];

	string_comando[0] = prim_byte;
	string_comando[1] = seg_byte;
	string_comando[2] = ter_byte;

	if( FreeRTOS_ioctl( xADS1292_SSP, ioctlOBTAIN_WRITE_MUTEX, ADS1292_MUTEX_TOUT ) == pdPASS )
	{
		/* Escribo y chequeo haber escrito todos los bytes */
		if( FreeRTOS_write( xADS1292_SSP, string_comando, ADS1292_LARGO_CMD_LARGO ) == ADS1292_LARGO_CMD_LARGO )
		{
			return 0;
		}
		return 1;
	}
	return 2;
}

/*==========================================================================================================
 *	\function	static BYTE prvADS1292_EscribirUnRegistro( BYTE registro, BYTE datos)
 * 	\brief 		Envio el comando para escribir un registro
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *==========================================================================================================*/


static BYTE ADS1292_EscribirUnRegistro( BYTE registro, BYTE datos)
{
	uint8_t i = 0x00;
	BYTE string_comando [ADS1292_ESCRIBIR_REG_LARGO];

	string_comando[0] = (ADS_WREG | registro);
	string_comando[1] = 0x00;
	string_comando[2] = datos;

	if( FreeRTOS_ioctl( xADS1292_SSP, ioctlOBTAIN_WRITE_MUTEX, ADS1292_MUTEX_TOUT ) != pdPASS )
		return 2;

	/* Escribo y chequeo haber escrito todos los bytes */
	if( FreeRTOS_write( xADS1292_SSP, &string_comando[0], 1 ) != 1 )
		return 1;

	for(i = 0xFF; i > 0; i--);

	if( FreeRTOS_write( xADS1292_SSP, &string_comando[1], 1 ) != 1 )
		return 1;

	for(i = 0xFF; i > 0; i--);

	if( FreeRTOS_write( xADS1292_SSP, &string_comando[2], 1 ) != 1 )
		return 1;

	for(i = 0xFF; i > 0; i--);


	return 0;

}



/*==========================================================================================================
 *	\function	static BYTE prvADS1292_LeerUnRegistro( BYTE registro, BYTE* byte_leido)
 * 	\brief 		Envio el comando para escribir un registro
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *==========================================================================================================*/

static BYTE ADS1292_LeerUnRegistro( BYTE registro, BYTE* byte_leido)
{
	uint8_t i = 0x00;
	if(ADS1292_EnviarComandoCorto(ADS_RREG | registro))
		return 1;

	for(i = 0xFF; i > 0; i--);

	if(ADS1292_EnviarComandoCorto(0x00))
		return 1;
	for(i = 0xFF; i > 0; i--);

	if( FreeRTOS_ioctl( xADS1292_SSP, ioctlOBTAIN_WRITE_MUTEX, ADS1292_MUTEX_TOUT ) == pdPASS )
		if(FreeRTOS_read( xADS1292_SSP, byte_leido, 1) == 1)          // <<<<< MANDA MUCHOS CICLOS DE CLOCK
 			return 0;
		else
			return 1;
	else
		return 2;

}


/*===================================================================================
 *	\function	void ADS1292_Iniciar_Adquisicion ( void *pvParameters )
 * 	\brief 		Inicio la adquisición en forma contínua del ADS1292
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void ADS1292_Iniciar_Adquisicion ( void )
{

	/* Instalo la tarea que lee lo enviado por el ADS despues del pulso de DRDY */
	ADS1292_Instalar_Tarea_Datos_Listos();
	/* Inicializo las estructuras de los canales */
	tCanal_Inicializar_Estructura(&canal0);
	tCanal_Inicializar_Estructura(&canal1);


	/* Espero antes de iniciar la interrupcion */
	vTaskDelay(1000);

	/* Activo la comunicación con el ADS mediante SPI */
	ADS1292_ASSERT_CS();
	/* Envío el comando SDATAC : Stop Read Data Continously - Espero 4 *   tCLK */
	ADS1292_EnviarComandoCorto(ADS_SDATAC);						vTaskDelay(ADS1292_DELAY_4tCLK);
	/* Envìo comando START : comienzo las conversiones. Espero 9 * tMOD + 1028*tMOD (SBAS502B pág. 31) */
	ADS1292_EnviarComandoCorto(ADS_START);						vTaskDelay(ADS1292_DELAY_POWERON);
	/* Envío el comando RDATAC : Read Data Continously - Espero 4 *   tCLK */
	ADS1292_EnviarComandoCorto(ADS_RDATAC);						vTaskDelay(ADS1292_DELAY_4tCLK);
	/* Desactivo el chip select del ADS */
	ADS1292_DEASSERT_CS();

	/* Limpio la interrupción pendiente */
	GPIO_ClearInt(ADS1292_DR_PORT, ADS1292_DR_PIN);
	/* Habilito las interrupciones generales */
	/* Enable the interrupt and set its priority to the minimum
	interrupt priority.  A separate command can be issued to raise
	the priority if desired. */
	NVIC_SetPriority( EINT3_IRQn, (configSPI_INTERRUPT_PRIORITY + 1));
	NVIC_EnableIRQ(EINT3_IRQn);
//	NVIC_EnableIRQ(ADS1292_DR_EINTx);
	/* Habilito la interrupción del pin de Data Ready */
	ADS1292_ACTIVAR_IRQ_DRDY();

	return;
}

/*======================================================================================================
 * 		DEFINICION DE TAREAS Y SU INSTALADOR :
 *======================================================================================================*/


/*======================================================================================================
 *	\function	void vADS1292_InstalarTareaInicializacion( void )
 * 	\brief 		Instalo la tarea que inicializa el ADS1292
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

void ADS1292_InstalarTareaInicializacion( void )
{
	xTaskCreate(tarea_ADS1292_Inicializacion,
				( const int8_t * const ) "ADS1292", 							/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
				configFILE_SYSTEM_DEMO_STACK_SIZE,								/* The size of the stack allocated to the task. */
				NULL,																/* The parameter is not used, so NULL is passed. */
				configFILE_SYSTEM_DEMO_TASK_PRIORITY,							/* The priority allocated to the task. */
				NULL );

	return;
}
/*===================================================================================
 *	\function	static void prv_ADS1292_Inicializacion( void *pvParameters )
 * 	\brief 		Tarea de inicializacion del ADS1292. Ver SBAS502B pág. 63
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void tarea_ADS1292_Inicializacion( void *pvParameters )
{
	uint8_t us_byte_ID = 0x00;
	BYTE r =10;

	/* Si es la primera vez que inicializo el puerto, abro el puerto SSP */
	if ( xADS1292_SSP == NULL )
	{
		xADS1292_SSP = ADS1292_Configurar_Canal_SSP();
	}
	/* Configuro el pin del CS como GPIO normal */
	ADS1292_CONFIGURAR_PINES()	;

	/* Espero 2^12 pulsos tMOD */
	vTaskDelay(ADS1292_DELAY_INICIAL);

	/* Pongo en bajo el pin de chip select y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_DEASSERT_CS();									vTaskDelay(ADS1292_DELAY_CS);

	/* Pongo en alto el pin de CS y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_ASSERT_CS();										vTaskDelay(ADS1292_DELAY_CS);

	/* Reseteo el ADS. Espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_EnviarComandoCorto(ADS_RESET);				vTaskDelay(ADS1292_DELAY_POWERON);

	/* El ADS se inicia en modo RDATAC, se pasa a modo SDATAC para escribir regs. Se espera 4 tCLK */
	ADS1292_EnviarComandoCorto(ADS_SDATAC);				vTaskDelay(ADS1292_DELAY_4tCLK);

	/* Leo el ID para controlar que la comunicación esté toda bien : */
	r = ADS1292_LeerUnRegistro(ADS_ID, &us_byte_ID);
	if(us_byte_ID == ADS1292_ID_CORRECTO)
		ADS1292_inicializado = 1;

	/*  Voy a escribir el registro CONFIG2. Desde esa dirección, solo escribo 1 registro : PDB_REFBUF = 1  */
	ADS1292_EscribirUnRegistro( ADS_CONFIG2, ADS_CONFIG2_PDB_REFBUF);  						vTaskDelay(ADS1292_DELAY_REFBUF);

	/*  Voy a escribir el registro CONFIG1. Desde esa dirección, solo escribo 1 registro : 125SPS + continuous mode  */
	ADS1292_EscribirUnRegistro( ADS_CONFIG1, ADS_CONFIG_1_OS_RATIO_1kSPS);  				vTaskDelay(ADS1292_DELAY_CMD);

	/*  Voy a escribir el registro CONFIG2. Desde esa dirección, solo escribo 1 registro : PDB_REFBUF = 1 */
	ADS1292_EscribirUnRegistro( ADS_CONFIG2, ADS_CONFIG2_PDB_REFBUF);  						vTaskDelay(ADS1292_DELAY_CMD);

	/*  Voy a escribir el registro ADS_CH1SET. Desde esa dirección, solo escribo 1 registro : Bit 7 en 1 obligatorio, bit 3 conecto CLK al pin de salida */
	ADS1292_EscribirUnRegistro( ADS_CH1SET, ADS_CH1SET_GAIN_1 | ADS_CH1SET_MUX_NORMAL);  	vTaskDelay(ADS1292_DELAY_CMD);

	/*  Voy a escribir el registro ADS_CH2SET. Desde esa dirección, solo escribo 1 registro : Bit 7 en 1 obligatorio, bit 3 conecto CLK al pin de salida */
	ADS1292_EscribirUnRegistro( ADS_CH2SET, ADS_CH2SET_GAIN_1 | ADS_CH2SET_MUX_NORMAL);  	vTaskDelay(ADS1292_DELAY_CMD);

	/* Pongo en bajo el pin de chip select y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_DEASSERT_CS();								vTaskDelay(ADS1292_DELAY_CS);


	// ADS1292_Instalar_Tarea_Datos_Listos();
	// ADS1292_Iniciar_Adquisicion();


	while(1)
	{
		vTaskDelay (1000);
	}
	return;
}

/*===================================================================================
 *	\function	t_estado_periferico ADS1292_Inicializar (void)
 * 	\brief 		Funcion de inicializacion del ADS1292
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

t_estado_periferico ADS1292_Inicializar (void)
{
	uint8_t us_byte_ID = 0x00;
	BYTE r =10;

	/* Si es la primera vez que inicializo el puerto, abro el puerto SSP */
	if ( xADS1292_SSP == NULL )
	{
		xADS1292_SSP = ADS1292_Configurar_Canal_SSP();
	}

	/* Espero 2^12 pulsos tMOD */
	vTaskDelay(ADS1292_DELAY_INICIAL);

	/* Pongo en bajo el pin de chip select y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_DEASSERT_CS();									vTaskDelay(ADS1292_DELAY_CS);

	/* Pongo en alto el pin de CS y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_ASSERT_CS();										vTaskDelay(ADS1292_DELAY_CS);

	/* Reseteo el ADS. Espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_EnviarComandoCorto(ADS_RESET);				vTaskDelay(ADS1292_DELAY_POWERON);

	/* El ADS se inicia en modo RDATAC, se pasa a modo SDATAC para escribir regs. Se espera 4 tCLK */
	ADS1292_EnviarComandoCorto(ADS_SDATAC);				vTaskDelay(ADS1292_DELAY_4tCLK);

	/* Leo el ID para controlar que la comunicación esté toda bien : */
	r = ADS1292_LeerUnRegistro(ADS_ID, &us_byte_ID);
	if(us_byte_ID == ADS1292_ID_CORRECTO)
		ADS1292_inicializado = 1;

	/*  Voy a escribir el registro CONFIG2. Desde esa dirección, solo escribo 1 registro : PDB_REFBUF = 1  */
	ADS1292_EscribirUnRegistro( ADS_CONFIG2, ADS_CONFIG2_PDB_REFBUF);  						vTaskDelay(ADS1292_DELAY_REFBUF);

	/*  Voy a escribir el registro CONFIG1. Desde esa dirección, solo escribo 1 registro : 125SPS + continuous mode  */
	ADS1292_EscribirUnRegistro( ADS_CONFIG1, ADS_CONFIG_1_OS_RATIO_500SPS);  				vTaskDelay(ADS1292_DELAY_CMD);

	/*  Voy a escribir el registro CONFIG2. Desde esa dirección, solo escribo 1 registro : PDB_REFBUF = 1 */
	ADS1292_EscribirUnRegistro( ADS_CONFIG2, ADS_CONFIG2_PDB_REFBUF);  						vTaskDelay(ADS1292_DELAY_CMD);

	/*  Voy a escribir el registro ADS_CH1SET. Desde esa dirección, solo escribo 1 registro : Bit 7 en 1 obligatorio, bit 3 conecto CLK al pin de salida */
	ADS1292_EscribirUnRegistro( ADS_CH1SET, ADS_CH1SET_GAIN_1 | ADS_CH1SET_MUX_NORMAL);  	vTaskDelay(ADS1292_DELAY_CMD);

	/*  Voy a escribir el registro ADS_CH2SET. Desde esa dirección, solo escribo 1 registro : Bit 7 en 1 obligatorio, bit 3 conecto CLK al pin de salida */
	ADS1292_EscribirUnRegistro( ADS_CH2SET, ADS_CH2SET_GAIN_1 | ADS_CH2SET_MUX_NORMAL);  	vTaskDelay(ADS1292_DELAY_CMD);

	/* Pongo en bajo el pin de chip select y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_DEASSERT_CS();								vTaskDelay(ADS1292_DELAY_CS);
	return PERIF_INICIALIZADO;
}

/*===================================================================================
 *	\function	static void ADS1292_Inicializar_Tarea_Datos_Listos (void)
 * 	\brief 		Función de inicialización de la tarea que atiende el pin DRDY
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void ADS1292_Instalar_Tarea_Datos_Listos (void)
{
	/* Inicializo la estructura del semáforo para sincronizar buffers completos. Lo tomo para dejarlo listo para usar */
	vSemaphoreCreateBinary(vSemaforoBin_Buffers_completos);
	configASSERT(vSemaforoBin_Buffers_completos);
	configASSERT(xSemaphoreTake(vSemaforoBin_Buffers_completos, portMAX_DELAY))

	/* Inicializo la estructura del semáforo para sincronizar data ready*/
	vSemaphoreCreateBinary(vSemaforoBin_ADS1292_DRDY);
	configASSERT(vSemaforoBin_ADS1292_DRDY);
	configASSERT(xSemaphoreTake(vSemaforoBin_ADS1292_DRDY, portMAX_DELAY));
	/*Creo tarea de datos listos*/
	xTaskCreate (tarea_ADS1292_Datos_Listos,
					 (const int8_t* const) "Datos Listos",
					 DATOS_LISTOS_STACK,
					 NULL,
					 DATOS_LISTOS_PRIORIDAD,
					 NULL);
	return;
}

/*===================================================================================
 *	\function	static void prv_ADS1292_Inicializacion( void *pvParameters )
 * 	\brief 		Tarea de inicializacion del ADS1292. Ver SBAS502B pág. 63
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void tarea_ADS1292_Datos_Listos ( void *pvParameters )
{
	t_u32 dato_canal;
	int a = 0;
	while(1)
	{
		/* Clavo la tarea esperando a que el pin DRDY se active y la IRQ me de el semáforo */
		if(xSemaphoreTake( vSemaforoBin_ADS1292_DRDY, portMAX_DELAY) == pdTRUE)
		{
			/* Bajo el pin de CS para activar el SSP : */
			ADS1292_ASSERT_CS();
			empezo_read_ssp_ADS = 1;

			/* Leo todos los bits y chequeo haber leído esa misma cantidad */
			if(FreeRTOS_read( xADS1292_SSP, ADS1292_Datos_Leidos, ADS1292_RDATAC_CANTBYTESLEIDOS) == ADS1292_RDATAC_CANTBYTESLEIDOS)
			{
				/* Levanto el pin de CS para desactivar el SSP : */
				ADS1292_DEASSERT_CS();

				if( ((ADS1292_Datos_Leidos [ADS1292_DATOS_LEIDOS_STATUS_B2] & 0xF0 ) == ADS1292_STATUS_B2) &&
					((ADS1292_Datos_Leidos [ADS1292_DATOS_LEIDOS_STATUS_B1] & 0x1F ) == ADS1292_STATUS_B1) &&
					(ADS1292_Datos_Leidos [ADS1292_DATOS_LEIDOS_STATUS_B0] == ADS1292_STATUS_B0) )
				{
					/* Armo el dato de 24 bits recibido para el canal 0 */
					dato_canal.bytes.b0 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO0_B0];
					dato_canal.bytes.b1 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO0_B1];
					dato_canal.bytes.b2 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO0_B2];
					dato_canal.bytes.b3 = 0x00;
					/* Asigno el dato armado al buffer del canal 0 */
					canal0.pAct[canal0.ind++] = dato_canal.qword;
					/* Pregunto si llegue al final del buffer : */
					if(canal0.ind >= CANAL_BUFFER_L)
					{
						canal0.estado_buffer = PENDIENTE;
						canal0.buffer_lleno_pendiente = true;
						canal0.ind = 0;
						tCanal_Intercambiar_Punteros(&canal0);
					}


					/* Armo el dato de 24 bits recibido para el canal 1 */
					dato_canal.bytes.b0 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO1_B0];
					dato_canal.bytes.b1 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO1_B1];
					dato_canal.bytes.b2 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO1_B2];
					dato_canal.bytes.b3 = 0x00;
					/* Asigno el dato armado al buffer del canal 1 */
					canal1.pAct[canal1.ind++] = dato_canal.qword;
					/* Pregunto si llegue al final del buffer : */
					if(canal1.ind >= CANAL_BUFFER_L)
					{
						canal1.buffer_lleno_pendiente = true;
						canal1.estado_buffer = PENDIENTE;
						canal1.ind = 0;
						tCanal_Intercambiar_Punteros(&canal1);
					}

					/* Pregunto si se llenaron los dos buffers */
					if(canal0.buffer_lleno_pendiente && canal1.buffer_lleno_pendiente)
					{
						/* Bajo los flags de buffer pendiente */
						canal0.buffer_lleno_pendiente = false;
						canal1.buffer_lleno_pendiente = false;



						/* Doy el semáforo para que el menú lo levante */
						if(xSemaphoreGive(vSemaforoBin_Buffers_completos) != pdTRUE)
						{
							// UART_Enviar_Mensaje((const int8_t * const)"\n\rerror : dar semaforo buffer completo");
							// configASSERT(0);
						}

					}

				}
				else
				{
					// UART_Enviar_Mensaje((const int8_t * const)"\n\rerror : Status del ADS");
				}

			}
			else
			{
				// UART_Enviar_Mensaje((const int8_t * const)"\n\rerror : cantidad bytes leidos ADS");
				// configASSERT(0);
			}
		}
		else
		{
			// UART_Enviar_Mensaje("\n\rerror : tomar semaforo drdy");
			// configASSERT(0);
		}


	}

	while(1)
	{
		vTaskDelay (1000);
	}
	return;
}

/*===================================================================================
 *	\function	void Instalar_Ejemplo_Inicia_Adquisicion (void)
 * 	\brief 		Instalo tarea de ejemplo
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void Instalar_Ejemplo_Inicia_Adquisicion (void)
{
	xTaskCreate (tarea_Ejemplo_Inicia_Adquisicion,
				 (const int8_t* const) "Datos Listos",
				 1000,
				 NULL,
				 4,
				 NULL);
	return ;
}

/*===================================================================================
 *	\function		void tarea_ADS1292_Ejemplo_Inicia_Adquisicion ( void *pvParameters )
 * 	\brief 		Tarea de ejemplo para iniciar la adquisición
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void tarea_Ejemplo_Inicia_Adquisicion ( void *pvParameters )
{

	while(1)
	{
		if(ADS1292_inicializado && !ADS1292_adquiriendo)
		{
			ADS1292_Iniciar_Adquisicion();
			ADS1292_adquiriendo = 1;
		}
		else
			vTaskDelay (9999);
	}
	return;
}


/*======================================================================================================
 * 		RUTINA DE ATENCIÓN DE INTERRUPCIÓN :
 *======================================================================================================*/

/*======================================================================================================
 *	\function	void EINT3_IRQHandler (void)
 * 	\brief 		Inicializacion de pines del ADS1292
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *======================================================================================================*/

void EINT3_IRQHandler (void)
{

	uint8_t reg_status;
	static portBASE_TYPE xHigherPriorityTaskWoken;

	static uint8_t datos_perdidos = 0;
	static uint32_t datos_ok = 0;

	/* Empiezo inhabilitando esta interrupción */
	NVIC_DisableIRQ(ADS1292_DR_EINTx);

	/* Leo este reg. para saber si la int. es de P0 o P2 */
	reg_status = LPC_GPIOINT->IntStatus;

	/* Analizo si la interrupción es por un pin de P0 o de P2 */
	if(reg_status & GPIOINT_INTSTAT_P0INT)
	{
		/* Interrupción en pines de P0, flanco de bajada */
		if(LPC_GPIOINT->IO0IntStatF & ADS1292_DR_PIN)
		{
			/* Limpio la interrupcion pendiente */
			// LPC_GPIOINT->IO0IntClr = GPIOINT_INTCLR_P00CI;
			/* Limpio la interrupcion pendiente */
			GPIO_ClearInt(ADS1292_DR_PORT, ADS1292_DR_PIN);


			/* Cedo el semáforo desde la interrupción para atenderlo desde la tarea */
			if(xSemaphoreGiveFromISR(vSemaforoBin_ADS1292_DRDY, &xHigherPriorityTaskWoken) != pdTRUE)
			{
				// UART_Enviar_Mensaje("\n\rerror : dar semaforo drdy");
				datos_perdidos++;
			}
			else
				datos_ok++;


		}
	}
	else if (reg_status & GPIOINT_INTSTAT_P2INT)
	{
		/* Interrupción en pines de P2, flanco de subida */
		//if(LPC_GPIOINT->IO2IntStatF & ADS1292_DR_PIN)
	//	{
			/* Limpio la interrupcion pendiente */
			// GPIO_ClearInt(ADS1292_DR_PORT, ADS1292_DR_PIN);
			/* Cedo el semáforo desde la interrupción para atenderlo desde la tarea */
			// xSemaphoreGiveFromISR(vSemaforoBin_ADS1292_DRDY, &xHigherPriorityTaskWoken);

// 		}
	}
	NVIC_EnableIRQ(ADS1292_DR_EINTx);
	return;
}

