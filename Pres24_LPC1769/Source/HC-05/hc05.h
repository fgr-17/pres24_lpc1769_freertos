/*
 * hc05.h
 *
 *  Created on: 7/11/2015
 *      Author: froux
 */

#ifndef HC_05_HC05_H_
#define HC_05_HC05_H_

/*======================================================================================================
 * 		DEFINICION DE CONSTANTES :
 *======================================================================================================*/

/* Baud rate para el modo AT : */
#define HC05_BAUD_RATE_AT						9600// 38400

/* Defino pin para habilitar comandos AT : pin 38 LPCXpresso P0.4*/
#define HC05_EN_PORT							( 0 )
#define HC05_EN_PIN								( 0x01 << 4 )
#define HC05_EN_DIR								boardGPIO_OUTPUT

/* Defino pin de status : */
#define HC05_STATUS_PORT						( 0 )
#define HC05_STATUS_PIN							( 0x01 << 5 )
#define HC05_STATUS_DIR							boardGPIO_INPUT




#define HC05_CONFIGURAR_PINES()																\
{																							\
	GPIO_SetDir( HC05_EN_PORT, HC05_EN_PIN, HC05_EN_DIR );									\
	GPIO_SetDir( HC05_STATUS_PORT, HC05_STATUS_PIN, HC05_STATUS_DIR );						\
}


#define HC05_EN_ACTIVAR()						GPIO_SetValue( HC05_EN_PORT, HC05_EN_PIN )
#define HC05_EN_DESACTIVAR()					GPIO_ClearValue( HC05_EN_PORT, HC05_EN_PIN )

/*======================================================================================================
 * 		DEFINICION DE COMANDOS AT :
 *======================================================================================================*/

#define ATCMD_AT							((const char* )"AT\r\n")
#define ATCMD_RESET							"AT+RESET\r\n"
#define ATCMD_VERSION						"AT+VERSION?\r\n"



/*======================================================================================================
 * 		VARIABLES GLOBALES :
 *======================================================================================================*/

extern t_estado_periferico HC05_Inicializar ( void );


#endif /* HC_05_HC05_H_ */
