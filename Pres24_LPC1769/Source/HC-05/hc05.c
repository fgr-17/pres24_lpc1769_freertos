/*
 * hc05.c
 *
 *  Created on: 7/11/2015
 *      Author: froux
 */


/* Standard includes. */
#include "string.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+IO includes. */
#include "FreeRTOS_IO.h"

/* Includes grales : */
#include <lpc17xx_gpio.h>

/* Includes propios : */
#include <Menu/tipos.h>
#include <UART/uart.h>
#include <HC-05/hc05.h>

/*======================================================================================================
 * 		PROTOTIPOS DE FUNCIONES  :
 *======================================================================================================*/

t_estado_periferico HC05_Inicializar ( void );

/*======================================================================================================
 * 		VARIABLES GLOBALES :
 *======================================================================================================*/



/*======================================================================================================
 * 		IMPLEMENTACION DE FUNCIONES :
 *======================================================================================================*/

/*======================================================================================================
 *	\function	t_estado_periferico HC05_Inicializar ( void *pvParameters )
 * 	\brief 		Tarea que inicializa el BT y lo setea a 115200bps. DEBE ESTAR INICIALIZADA LA UART
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

t_estado_periferico HC05_Inicializar ( void )
{
	/* Defino pin para entrar en modo AT. Supongo que la UART ya está funcionando */
	HC05_CONFIGURAR_PINES();

	/* Activo el pin de EN para entrar en modo AT */
	HC05_EN_ACTIVAR();
	HC05_EN_DESACTIVAR();
	HC05_EN_ACTIVAR();

	UART_Enviar_Comando_AT(ATCMD_AT, "OK");

	/* Desactivo el pin de EN para salir en modo AT */

	// ioctlSET_SPEED



return 0;
}
