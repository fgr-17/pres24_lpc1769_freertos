/*
 * uart.c
 *
 *  Created on: 16/6/2015
 *      Author: froux
 */

/*======================================================================================================
 * 		INCLUSIÓN DE ARCHIVOS :
 *======================================================================================================*/

/* Standard includes. */
#include "string.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+IO includes. */
#include "FreeRTOS_IO.h"

#include "ff.h"

/* Includes propios */
#include <Menu/tipos.h>
#include <Menu/menu.h>
#include <UART/uart.h>
#include <HC-05/hc05.h>
#include <Protocolo/protocolo.h>

/*======================================================================================================
 * 		PROTOCOLOS DE TAREAS :
 *======================================================================================================*/

void UART_Terminal_Instalar_Tarea_Inicializacion ( void );
void tarea_Inicializar_UART_Terminal ( void *pvParameters );

t_estado_periferico UART_Inicializar ( void );
void UART_Enviar_Mensaje (const int8_t * const  mensaje);
void UART_Enviar_Buffer (const int8_t * const buffer, int32_t largo);
int8_t UART_Recibir_Comando (int8_t * lista_comandos, int32_t lista_comandos_largo);
/*======================================================================================================
 * 		VARIABLES GLOBALES :
 *======================================================================================================*/

/* Defino el buffer de salida de la uart usada como terminal */
int8_t UART_Terminal_Buffer_Salida [UART_TERMINAL_BUFFER_SALIDA_LON];

/* Descriptor de periférico usado para la UART Terminal  */
static Peripheral_Descriptor_t UART_Terminal = NULL;

/* Defino variable de estado del periférico y su mutex : */
t_estado_periferico uart0_estado = PERIF_APAGADO;

/*======================================================================================================
 * 		IMPLEMENTACIÓN DE FUNCIONES Y TAREAS :
 *======================================================================================================*/

/*======================================================================================================
 *	\function	void UART_Terminal_Instalar_Tarea_Inicializacion( void )
 * 	\brief 		Instalo la tarea que inicializa la UART que lee la terminal
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

void UART_Terminal_Instalar_Tarea_Inicializacion (void)
{
	xTaskCreate( tarea_Inicializar_UART_Terminal,
				( const int8_t * const ) "UART_Inic", 							/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
				configFILE_SYSTEM_DEMO_STACK_SIZE,								/* The size of the stack allocated to the task. */
				NULL,															/* The parameter is not used, so NULL is passed. */
				configFILE_SYSTEM_DEMO_TASK_PRIORITY,							/* The priority allocated to the task. */
				NULL );

	return;
}


/*======================================================================================================
 *	\function	static void Tarea_Inicializar_UART_Terminal ( void *pvParameters )
 * 	\brief 		Tarea que inicializa la UART
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

void tarea_Inicializar_UART_Terminal ( void *pvParameters )
{
	portBASE_TYPE xReturned;

	/* Abro el puerto seleccionado con los parámetros default, después lo modifico */
	UART_Terminal = FreeRTOS_open (UART_TERMINAL, (uint32_t) cmdPARAMTER_NOT_USED);
	configASSERT ( UART_Terminal );

	/* Cambio al modo Zero Copy para no guardar datos en buffer, escribo directamente sobre el registro de salida */
	xReturned = FreeRTOS_ioctl( UART_Terminal, ioctlUSE_ZERO_COPY_TX, cmdPARAMTER_NOT_USED );
	configASSERT( xReturned );

	/* Uso una queue para recibir los mensajes */
	xReturned = FreeRTOS_ioctl( UART_Terminal, ioctlUSE_CHARACTER_QUEUE_RX, ( void * ) cmdMAX_INPUT_SIZE );
	configASSERT( xReturned );

	/* Seteo la interrupción de RX como la de mín. prioridad */
	xReturned = FreeRTOS_ioctl( UART_Terminal, ioctlSET_INTERRUPT_PRIORITY, ( void * ) ( configMIN_LIBRARY_INTERRUPT_PRIORITY - 1 ) );
	configASSERT( xReturned );

	/* Señalo el periférico como inicializado  */
	uart0_estado = PERIF_INICIALIZADO;



	while(1)
	{

		/*
		if ( FreeRTOS_read( UART_Terminal,&caracter_leido, 1) == 1)
		{
		if (caracter_leido == 'a')
			caracter_leido = 0x01;
		else
			vTaskDelay(10);

		}
*/
		/* Me quedo esperando a que se elimine la tarea */
		vTaskDelay(1000);


	}

}


/*======================================================================================================
 *	\function	t_estado_periferico UART_Inicializar ( void )
 * 	\brief 		Tarea que inicializa la UART
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/


t_estado_periferico UART_Inicializar ( void )
{

	portBASE_TYPE xReturned;

	/* Abro el puerto seleccionado con los parámetros default, después lo modifico */
	UART_Terminal = FreeRTOS_open (UART_TERMINAL, (uint32_t) cmdPARAMTER_NOT_USED);
	configASSERT ( UART_Terminal );

	/* Cambio al modo Zero Copy para no guardar datos en buffer, escribo directamente sobre el registro de salida */
	xReturned = FreeRTOS_ioctl( UART_Terminal, ioctlUSE_ZERO_COPY_TX, cmdPARAMTER_NOT_USED );
	configASSERT( xReturned );

	/* Uso una queue para recibir los mensajes */
	xReturned = FreeRTOS_ioctl( UART_Terminal, ioctlUSE_CHARACTER_QUEUE_RX, ( void * ) cmdMAX_INPUT_SIZE );
	configASSERT( xReturned );

	/* Uso una queue para transmitir los mensajes */
	xReturned = FreeRTOS_ioctl( UART_Terminal, ioctlUSE_CHARACTER_QUEUE_TX, ( void * ) TX_QUEUE_LARGO );
	configASSERT( xReturned );


	/* Seteo la interrupción de RX como la de mín. prioridad */
	xReturned = FreeRTOS_ioctl( UART_Terminal, ioctlSET_INTERRUPT_PRIORITY, ( void * ) ( configMIN_LIBRARY_INTERRUPT_PRIORITY ) );
	configASSERT( xReturned );

	/* Señalo el periférico como inicializado  */
	uart0_estado = PERIF_INICIALIZADO;

	return PERIF_INICIALIZADO;


}
/*======================================================================================================
 *	\function	void UART_Enviar_Mensaje (const int8_t * const mensaje)
 * 	\brief 		Envía un string por la UART
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

int UART_Enviar_Comando_AT (const int8_t* comando_at, const int8_t* respuesta_at)
{
	/* buffer para recibir la respuesta a comandos AT */
	int8_t atbuf [32];
	portBASE_TYPE xReturned;

	int bytesleidos = 0;


	/* Paso a la velocidad para configurar el hc05 */
	xReturned = FreeRTOS_ioctl( UART_Terminal, ioctlSET_SPEED, HC05_BAUD_RATE_AT ); //9600);//
	configASSERT( xReturned );

	/* Pregunto si estoy en modo AT */
	if( FreeRTOS_ioctl( UART_Terminal, ioctlOBTAIN_WRITE_MUTEX, cmd50ms ) == pdPASS )
	{
		FreeRTOS_write( UART_Terminal, comando_at, strlen( ( char * ) comando_at ) );
	}


	bytesleidos = FreeRTOS_read(UART_Terminal, atbuf, 2);

	if(!strcmp(atbuf, respuesta_at))
	return 0;
	return 0;
}


/*======================================================================================================
 *	\function	void UART_Enviar_Mensaje (const int8_t * const mensaje)
 * 	\brief 		Envía un string por la UART
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

void UART_Enviar_Mensaje (const int8_t * const mensaje)
{
	if( FreeRTOS_ioctl( UART_Terminal, ioctlOBTAIN_WRITE_MUTEX, cmd50ms ) == pdPASS )
	{
		FreeRTOS_write( UART_Terminal, mensaje, strlen( ( char * ) mensaje ) );
	}
	return;
}


/*======================================================================================================
 *	\function	void UART_Enviar_Buffer (const int8_t * const buffer, int32_t largo)
 * 	\brief 		Envía un buffer de un largo fijo por la UART
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

void UART_Enviar_Buffer (const int8_t * const buffer, int32_t largo)
{
	if( FreeRTOS_ioctl( UART_Terminal, ioctlOBTAIN_WRITE_MUTEX, cmd50ms ) == pdPASS )
	{
		FreeRTOS_write( UART_Terminal, buffer, largo );
	}
	return;
}

/*======================================================================================================
 *	\function	int32_t UART_Recibir_Comando (const int8_t * const lista_comandos)
 * 	\brief 		Recibe un comando por la UART y devuelve el número encontrado, sino devuelve -1
 * 	\author		ROUX, Federico G. (froux@tuta.io)
 *======================================================================================================*/

int8_t UART_Recibir_Comando (int8_t *lista_comandos, int32_t lista_comandos_largo)
{
	int8_t rxbuf[COMANDO_LARGO+1];
	int32_t i;
	while(1)
	{
		if(FreeRTOS_read(UART_Terminal, rxbuf, COMANDO_LARGO) == COMANDO_LARGO)
		{
			switch(estado_protocolo)				// Si estoy en un estado de recepcion de datos, lo paso como me llega.
			{
			case ESPERO_HOR:
			case ESPERO_MIN:
			case ESPERO_SEG:
			case ESPERO_DD:
			case ESPERO_MM:
			case ESPERO_AA:
				return rxbuf[0];
				break;


			default:								// Sino lo convierto a valor de comando
				for (i = 0; i < lista_comandos_largo; i++)
				{
					if(lista_comandos[i] == rxbuf[0])
						return i;
				}
				return -1;
			}
		}

	}
}
