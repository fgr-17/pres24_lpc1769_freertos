/*
 * uart.h
 *
 *  Created on: 16/6/2015
 *      Author: froux
 */

#ifndef UART_UART_H_
#define UART_UART_H_

/*======================================================================================================
 * 		DEFINICION DE CONSTANTES :
 *======================================================================================================*/

/* Definición de constantes de la UART seleccionada como terminal */
#define UART_TERMINAL											(( const int8_t * const ) "/UART0/")
#define UART_TERMINAL_BUFFER_SALIDA_LON							( 8 )
#define cmdPARAMTER_NOT_USED									( ( void * ) 0 )

/* Dimensions the buffer into which input characters are placed. */
#define cmdMAX_INPUT_SIZE										50
#define TX_QUEUE_LARGO											(256*4*1)

/* Block times of 50 and 500milliseconds, specified in ticks. */
#define cmd50ms													( ( void * ) ( 50UL / portTICK_RATE_MS ) )
#define cmd500ms												( ( void * ) ( 500UL / portTICK_RATE_MS ) )

/*======================================================================================================
 * 		PROTOCOLOS DE TAREAS :
 *======================================================================================================*/

extern void UART_Terminal_Instalar_Tarea_Inicializacion (void);
extern void tarea_Inicializar_UART_Terminal ( void *pvParameters );

extern t_estado_periferico UART_Inicializar ( void );
extern void UART_Enviar_Mensaje (const int8_t * const mensaje);
extern void UART_Enviar_Buffer (const int8_t * const buffer, int32_t largo);
int UART_Enviar_Comando_AT (const int8_t* comando_at, const int8_t* respuesta_at);
extern int8_t UART_Recibir_Comando (int8_t *lista_comandos, int32_t lista_comandos_largo);
/*======================================================================================================
 * 		VARIABLES GLOBALES :
 *======================================================================================================*/

/* Defino el buffer de salida de la uart usada como terminal */
extern int8_t UART_Terminal_Buffer_Salida [UART_TERMINAL_BUFFER_SALIDA_LON];
/* Defino variable de estado del periférico : */
extern t_estado_periferico uart0_estado;

#endif /* UART_UART_H_ */
