/*
    FreeRTOS V7.3.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

#ifndef SPI_INTERFACE_TO_SD_CARD_H
#define SPI_INTERFACE_TO_SD_CARD_H

/*======================================================================================================
 * 		DEFINICION DE CONSTANTES :
 *======================================================================================================*/

#define ARCHIVO_PREFIJO							"a"
#define ARCHIVO_EXTENSION						"txt"
#define ARCHIVO_NROCANAL						"-x"

#define ARCHIVO_UNIDAD_LARGO					( 3 )								// Cantidad de caracteres de la unidad "0:/"
#define ARCHIVO_PREFIJO_LARGO					( 1 )								// Cantidad de caracteres del prefijo del archivo "adq"
#define ARCHIVO_EXTENSION_LARGO					( 4 )								// Cantidad de caracteres de la extensión del archivo
#define ARCHIVO_NUMERO_LARGO					( 3 )								// Cantidad de caracteres del nro. de archivo
#define ARCHIVO_NROCANAL_LARGO					( 2 )								// Cantidad de caracteres para indicar el canal

#define ARCHIVO_NUMERO_MIN						( 000 )								// Inicio de la cuenta de archivo
#define ARCHIVO_NUMERO_MAX						( 999 )								// Fin de la cuenta de archivo

#define ARCHIVO_NOMBRE_LARGO					( ARCHIVO_UNIDAD_LARGO +  ARCHIVO_PREFIJO_LARGO + ARCHIVO_NROCANAL_LARGO + ARCHIVO_NUMERO_LARGO + ARCHIVO_EXTENSION_LARGO )

/*======================================================================================================
 * 		FUNCIONES EXTERNAS :
 *======================================================================================================*/

void vStartSPIInterfaceToSDCardTask( void );

/* -- FUNCIONES AGREGADAS :*/
extern t_estado_periferico SD_Inicializar ( void );
extern int8_t Obtener_Nombre_Ultimo_Archivo (char* nombre_devuelto, char*nombre_c0, char*nombre_c1);
// extern void Descargar_Buffer_A_Archivo (t_canal*canal, char*narchivo);
// extern void Descargar_Buffer_A_Archivo (FIL*pArchivo, t_canal*canal); //, char*narchivo)
extern void Descargar_Buffer_A_Archivo (FIL*pArchivo, int32_t*pBuf);
extern void Crear_Parte_Archivo (FIL*pArchivo, char*narchivo);
extern void Cerrar_Archivo (FIL*pArchivo);

#endif /* SPI_INTERFACE_TO_SD_CARD_H */



