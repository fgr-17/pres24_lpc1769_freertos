#	PRES24


> PROYECTO : 	PRES24

> DESCRIPCION :	Adquisición de sensores de presión Königsberg utilizando un ADC de 24 bits de entrada diferencial (ADS1292)
a través de bus SPI del MCU LPC1769. Se adquiere en una memoria SD y se comunica inalámbricamente con una terminal mediante
BT.

> SEGMENTO :	Firmware del LPC1769 utilizando FreeRTOS

> AUTOR :	Roux, Federico G. (froux@tuta.io)

TAREAS A REALIZAR PENDIENTES : 


> Ver como configurar la adquisicion : canales, Fs, etc.

> Manejo de ADC para medir batería.

> guardar configuracion en el archivo ppal, fs, tiempo de funcionamiento, cantidad de muestras, etc.

> Mostrar tamaño del archivo 

HISTORIAL DE MODIFICACIONES :

- 24.04.2015 : Inicialización del repositorio sobre ejemplo de FreeRTOS y la SD. Se comenzaron las modificaciones para 
agregar SSP0 y el resto de los periféricos.

- 05.05.2015 : Se agregó el manejo del puerto SSP0 con las funciones de freeRTOS_IO (). Estaba hardcodeado en algunas partes para funcionar solamente con el SSP1.

- 15.06.2015 : Resuelta la interrupción externa del pin DRDY del ADS1292 y su atención en una tarea a través de un semáforo binario. Falta iniciar correctamente el semaforo binario para que no salte al principio sin que ocurra ninguna interrupción. Falta instalar semihosting para debuggear más rápido.

- 18.06.2015 : Agregadas definiciones de todas las UART y del otro bus SPI en el archivo "LPCXpresso17xx-base-board.h". > HACER : Queda por separar todo lo que sean periféricos propios del micro y las definiciones que están asociadas al hardware de la placa base usada.

- 30.06.2015 : La comunicación con el ADS1292 está bien configurada. Responde el ID de manera correcta, y la frecuencia está bien seteada.

- 18.07.2015 : 
	> ADS1292 configurado correctamente. Inicia al mandarle el START. 
	> Se agregaron funciones a la librería lpc17xx_gpio.c para desactivar una interrupción asociada a un pin.
	> Se modificó la función GPIO_IntCmd() porque al activar un pin como interrupción, desactivaba el resto. No hacía la función OR con el valor que ya tenía el puerto.
	> Se modificó el archivo FreeRTOSIOConfig.h para cambiar las constantes de habilitación de la UART.
	> Se corrigieron errores de inicialización de la UART0. Funciona correctamente la Tx
	> Se agregó el handler de la UART usada en el archivo FreeRTOS_lpc17xx.c para poder usar el periférico con la queue. Funciona correctamente la RX usando queues

- 19.07.2015 :
	> Se crearon los archivos "menu.c" y "menu.h" en la carpeta "/Menu" para el manejo del programa ppal. 
	> Se creo el tipo de dato t_estado_periferico para crear una variable asociada a cada periférico que necesita inicializacion. De esta manera puedo saber leyendo la variable, si el periférico está inicializado o no.
	

- 26.09.2015 :
	> Comienzo del menú de inicialización y CLI
	> Definido <tipos.h> para el manejo de tipos comunes a varios archivos
	> Resuelta la inicializacion del sistema con funciones y la pantalla principal
	> Resuelta funcionalidad del menu

- xx.10.2015 :
	> Se creó la estructura t_canal con los campos de buffer, punteros e índices.-
	> Se crearon las funciones de inicializacion de t_canal, y del intercambio de buffers.
	> Pin DRDY funcionando en la placa nueva .
	> Se agregó semáforo para sincronizar buffers de adquisición completos
	> Se modificaron las prioridades de las tareas y sus heaps
	> La tarea Adquisicion() se modifico para no ser máq. de estados porque trae problemas sincronizar la variable de estado y modificarla desde diferentes lugares
	> RESEULTO -- Guardar archivos : ver de qué tamaño es mejor guardar el buffer, y si conviene separar en varios archivos o seguir escribiendo siempre en el mismo. --> 256 muestras de 4byte cada una a una Fs máx. de 500SPS 2 canales 24 bits. Se resolvió teniendo en cuenta las prioridades de las tareas y las IRQ.
	> RESEULTO -- Primer toma de semáforos trae problemas. --> Se hace un take al crear el semáforo
	> RESEULTO -- VISTO...Prioridad de tareas y sus tamaños de HEAP. --> se puede ajustar mejor?
	> RESEULTO -- Cerrar tareas finalizadas ?
	> RESEULTO -- Hacer una tarea con la maq. de estados ppal. --> Se resolvió en forma de tarea
	> RESEULTO --  Es necesario usar máquinas de estado??? --> Se usaron tareas sin estado por no poder sincronizar la variable de estado
	> RESUELTO -- HECHO CON F_SYNC PARCIALMENTE...trabajar con las funciones por debajo de f_open, f_write, f_close, para no abrir y cerrar archivos, y guardar todo en un mismo archivo.
	> RESEULTO -- Mostrar nombre del archivo al empezar a guardar




- 07.11.2015 : 
	> 17.10.2015 : RESUELTO : Hacer comunicación por BT : Conecta a 9600bps.
	> 07.11.2015 : Funciones para pasar el BT a 115200bps mediante comandos AT. Falta terminar. Problemas de hardware en la placa. (RESUELTO)

- 05.03.2016
	> BT funcionando a 115200bps. (se modificó el HW del HC-05 para poder enviar comandos AT)
	> Menu separado para consola y aplicación de Visual Studio (al final no se usa, conecto con SW de Damian)
	> Planteado toda la máquina de estados de protocolo de Damián
	> LPCXpresso 7.9.2 (en el 8.1 no funciona el debugger)
	> Máquina de estados testeada sin estado ONLINE 

- 09.03.2016 :
	> Fs llevada a 1kHz con buffer de 1024 muestras funcionando OK (no se tilda por lo menos).
	> Visualizando el tiempo de guardado de la SD en LabVIEW, demora 20ms sin importar casi el tamaño del buffer a guardar, y siempre se ven cada tanto algunas demoras de hasta 700ms, con lo que el tiempo de llenado del buffer conviene que sea mayor que esto. (se cambio el tamaño del buffer a 1024)
	> Se creo un tipo de dato para el estado del buffer en función del envío. Falta hacer el envío desde una tarea extra, porque la tarea de protocolo solo se activa cada vez que se recibe un dato por UART, y la otra activa, que es la del ADS, solo se activa al llenarse el buffer (el resto del tiempo espera un semáforo).

- 13.03.2016 :
	> Creada una tarea "Tarea_Enviar_Online()" que envia los paquetes de datos cuando la máquina de estados de protocolo se encuentra en ETAPA_ONLINE. Se creo un campo nuevo de la estructura t_canal, T_Estado_Buffer estado_buffer, con 4 estados : INACTIVO, PENDIENTE, PROCESANDO, FINALIZADO, para señalizar el envío del buffer.
	> Al enviar el buffer a la vez que se almacena en la SD, el programa se tilda, por lo que se creará en la próxima etapa un tercer buffer y puntero para que cada tarea acceda a diferentes lugares de memoria. Otra opción es copiar el buffer de almacenamiento de 32bits a uno de 16 casteando cada valor, una vez que termino el guardado en la SD.

- 13.03.2016 :

	> Muestreo fijado en 1kHz (revisar que no se tilde al enviar online)
	> Envío online aparentemente resuelto. Falta chequear con el programa de Damián.
	> Falta soldar Cap de 1uF del ADS.
	> Revisar tiempos en muestreo de 1kHz.
	> Revisar consumo incluyendo el HC-05

- 22.03.2016 :

	> Soldado cap de 10uF
	> Hacer titilar mientras el programa está funcionando	
	> Corregido pines desoldados de IN1P y IN1N. Muestrea bien en los dos canales

- 28.03.2016 :

	> Modificados los archivos de la carpeta ETH para inicializar el controlador de ethernet y ponerlo en modo power down. Consume en total 64mA contra 190 de antes. Hay picos de 100mA cuando guarda en la SD.
	> Se subió la prioridad de la tarea Adquirir a 4, para evitar que se demore mucho en guardar en la SD. El ranking de tareas queda : 
		- Datos_Listos (procesa el buffer una vez completo) : 4
		- Adquirir (guarda en la SD) : 4
		- Menú Inicial (inicializa las demas tareas ) : 2
		- Enviar_Online (envía los datos por puerto serie ): 2


- 29.03.2016 :

	> Creada documentación de tareas en Visio. Archivo agregado al repo.	
	> Agregado led P0[22] encendido muentras guarda en la SD.
	> Se eliminó la tarea Menu_Inicial() y se paso la inicialización a la tarea Protocolo().
	> Orden de prioridades :
		- Datos_Listos (procesa el buffer una vez completo) : 4
		- Adquirir (guarda en la SD) : 3
		- Protocolo (interfaz usuario) : 3
		- Enviar_Online (envía los datos por puerto serie ): 2

- 15.04.2016 :

	> Maquina de estados actualizada : agregada opción de adquirir, adquirir y ver, y señal de prueba. (ver documentacion)

- 16.04.2016 :

	> corregida inicializacion de autonum. cada vez que se muestra señal
	> corregida tarea adquirir : salia al no ser verdadera la condicion del while
	> agregada tarea de enviar señal de prueba. envia señal entera por canal 1 y la mitad por canal 2
	> señal de 16 bit y 32 bit creada
	> Maquina de estados funciona OK en todos los estados

	a revisar : cuando esta en alguna etapa online y finalizo, puede quedar la respuesta de la finalización mezclada con los datos o una parte de ellos.

- 20.04.2016 :

	> tarea_Adquirir() modificada para funcionar como máquina de estados, y crear archivos cada vez que se inicia una adquisición. FALTA DOCUMENTAR ESTE ARCHIVO

- 23.04.2016

	> UART_Recibir_Comando() modificado. Devuelve 8 bits siempre, y si está en la fase de recibir hora, no analiza si es un comando o no, devuelve el valor como llega. ERROR de capas!! pero funciona...
	> La tarea Adquirir y Enviar Online van a vTaskDelay(100) si no hacen nada. Sino Adquirir (prioridad 3) no deja ejecutar las de menor prioridad al quedar sin hacer nada. 
	> Agregado recibir fecha y hora en 6 bytes separados, cada uno respondiendo "F@@@@@"
	> Manejo de comandos hecho en 8 bits
	> Señal de prueba se envía sincronizada con la adquisición

- 07.05.2016

	> Modificaciones para reducir el consumo: se apagaron los sgtes dispositivos
#define PCENET_BIT				(1 << 30)
#define PCI2C2_BIT				(1 << 26)
#define PCI2C1_BIT				(1 << 19)
#define PCI2C0_BIT				(1 << 7)
#define PCPWM1_BIT				(1 << 6)
#define PCUART1_BIT				(1 << 4)
#define PCTIM0_BIT				(1 << 1)
#define PCTIM0_BIT				(1 << 2)			
