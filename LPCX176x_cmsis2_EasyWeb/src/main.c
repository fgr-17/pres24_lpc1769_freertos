/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : EMAC OFF
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

//LED
#define LED 		(1 << 22)
#define LED_ON 		LPC_GPIO0->FIOSET=LED
#define LED_OFF		LPC_GPIO0->FIOCLR=LED
#define LED_TOG		LPC_GPIO0->FIOPIN^=LED

//LED2
#define LED_2 		(1 << 12)
#define LED2_ON 	LPC_GPIO2->FIOSET  = LED_2
#define LED2_OFF	LPC_GPIO2->FIOCLR  = LED_2
#define LED2_TOG	LPC_GPIO2->FIOPIN ^= LED_2

#include "ethmac.h"

int main(void)
{
 LPC_GPIO0->FIODIR |= LED;		//LED output
 LPC_GPIO2->FIODIR |= LED_2;	//LED2 output

 Init_EthMAC();					//init and power it down
//switch off a few things
 LPC_SC->PCONP &= ~( (1<<30) | (1<<26) | (1<<21)| (1<<19)| (1<<10)| (1<<9)| (1<<8)| (1<<7)| (1<<6)| (1<<4)| (1<<3)| (1<<2)| (1<<1));
 volatile static int i = 0 ;
 while(1)
 {
  i++ ;
  if(i>500000)
  {
   LED_TOG;						//let's see something
   LED2_TOG;					//for my money
   i=0;
  }
 }
 return 0 ;
}
